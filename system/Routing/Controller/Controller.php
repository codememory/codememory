<?php

namespace System\Routing\Controller;

use Kernel\Service\Exceptions\{
    ExpanderNotImplementedException,
    RegularServiceException,
    ServiceNotCreatedException,
    ServiceNotImplementedException
};
use Kernel\Service\Providers\{
    ConfigProvider,
    CookieProvider,
    DateProvider,
    EnvProvider,
    FindProvider,
    HeaderProvider,
    RequestProvider,
    ResponseProvider,
    TimeProvider
};
use Kernel\Service\ServiceProvider;
use System\FileSystem\Different\Services\{
    FileEditorProvider,
    FileInformationProvider,
    FileIsProvider,
    FileProvider
};
use System\Routing\Exceptions\ControllerException;
use System\Validation\Exceptions\InvalidRuleNameException;
use System\Validation\Exceptions\NoValidationException;
use System\Validation\Manager as ValidateManager;
use System\Validation\Validator;
use System\View\Utils;
use System\View\View;

/**
 * @package System\Routing\Controller
 *
 * @property FileProvider            $file
 * @property ResponseProvider        $response
 * @property HeaderProvider          $header
 * @property FileInformationProvider $fileInfo
 * @property FindProvider            $find
 * @property FileEditorProvider      $fileEditor
 * @property FileIsProvider          $fileIs
 * @property RequestProvider         $request
 * @property EnvProvider             $env
 * @property ConfigProvider          $config
 * @property CookieProvider          $cookie
 * @property DateProvider            $date
 * @property TimeProvider            $time
 *
 * Class Controller
 * @author  Codememory
 */
class Controller
{

    /**
     * @var ServiceProvider
     */
    private ServiceProvider $services;

    /**
     * @var ValidateManager
     */
    private ValidateManager $validateManager;

    /**
     * @var array
     */
    private array $binds;

    /**
     * @var View
     */
    protected View $view;

    /**
     * Controller constructor.
     *
     * @param ServiceProvider $services
     * @param ValidateManager $validateManager
     *
     * @throws ExpanderNotImplementedException
     * @throws ServiceNotCreatedException
     * @throws \ReflectionException
     */
    public function __construct(ServiceProvider $services, ValidateManager $validateManager)
    {

        $this->services = $services;
        $this->services->collect();
        $this->validateManager = $validateManager;
        $this->binds = $this->config->binds();

        $this->view = new View(
            $this->config->open('configs.binds')->all()['default.templateEngine'],
            new Utils()
        );

    }

    public function getAvails()
    {

        // TODO:

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get a list of binds that include both the framework binds and
     * & the dynamic ones, you can also specify the $ key argument to
     * & pass the name of the bind you want to receive
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|null $key
     *
     * @return mixed
     */
    protected function getBind(?string $key = null): mixed
    {

        if(null === $key) {
            return $this->binds;
        }

        return $this->binds[$key] ?? null;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Add dynamically new bind to all binds
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $key
     * @param mixed  $value
     *
     * @return $this
     */
    protected function setBind(string $key, mixed $value): Controller
    {

        $this->binds[$key] = $value;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Validator manager, through this method you can call the validation
     * & class that implements the System\Validation\BuildInterface interface
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array  $data
     * @param string $validate
     * @param mixed  ...$arguments
     *
     * @return Validator
     * @throws \ReflectionException
     * @throws InvalidRuleNameException
     * @throws NoValidationException
     */
    protected function validateManager(array $data, string $validate, ...$arguments): Validator
    {

        return $this->validateManager->track($data, $validate, ...$arguments);

    }

    /**
     * @param $property
     *
     * @return object
     * @throws ControllerException
     * @throws RegularServiceException
     * @throws ServiceNotImplementedException
     */
    public function __get($property): object
    {

        if (!array_key_exists($property, $this->services->getAllServices())) {
            throw new ControllerException(sprintf(
                'Property | service provider <b>%s</b> not found',
                $property
            ));
        }

        return $this->services->getService($property);

    }

    /**
     * @param string $method
     * @param array  $arguments
     *
     * @throws ControllerException
     */
    public function __call(string $method, array $arguments = []): void
    {

        throw new ControllerException(
            sprintf('<b>%s::%s()</b> method does not exist.',
                get_class($this), $method)
        );

    }

}