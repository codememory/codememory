<?php

namespace System\Routing\Console;

use File;
use FileEditor;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class MakeController
 * @package System\Routing\Console
 *
 * @author  Codememory
 */
class MakeControllerCommand extends Command
{

    protected function configure()
    {

        $this
            ->setName('make:controller')
            ->setDescription('Создание контроллера')
            ->addArgument(
                'name',
                InputArgument::REQUIRED,
                'Имя контроллера'
            )
            ->addOption(
                'recreate',
                null,
                InputOption::VALUE_OPTIONAL,
                'Пересоздать контроллер, если он существует',
                false
            )
            ->setDescription('Создание контроллера');

    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $style = new SymfonyStyle($input, $output);

        $optionRecreate = (bool)$input->getOption('recreate');
        $namePath = $input->getArgument('name');
        $nameExplode = explode('/', $namePath);
        $controllerName = sprintf('%s%s', array_pop($nameExplode), 'Controller');

        if (count($nameExplode) >= 1) {
            $namespace = sprintf('App\\Controllers\\%s', implode('\\', $nameExplode));
        } else {
            $namespace = sprintf('App\\Controllers');
        }

        $path = sprintf('app/Controllers/%sController.php', $namePath);
        $stubController = File::read('system/Routing/Console/Stubs/ControllerStub.stub');

        if (File::exists($path) && $optionRecreate === false) {
            $style->error(
                [
                    sprintf('Контроллер %s\\%s уже существует', $namespace, $controllerName),
                    'Добавте опцию --recreate=true если хотите пересоздать'
                ]
            );

            return Command::FAILURE;
        } else {
            $this->createController($style, $controllerName, $namespace, $path, $stubController);

            $style->success(sprintf('Контроллер %s\\%s успешно создан', $namespace, $controllerName));
        }

        return Command::SUCCESS;

    }

    /**
     * @param SymfonyStyle $style
     * @param string       $controllerName
     * @param string       $namespace
     * @param string       $path
     * @param string       $stubController
     */
    private function createController(SymfonyStyle $style, string $controllerName, string $namespace, string $path, string $stubController)
    {

        $author = $style->ask('Укажите автора', 'Codememory');
        $replaced = str_replace(
            $this->replaceable(),
            [
                $controllerName,
                $author,
                $namespace
            ],
            $stubController
        );

        File::editor()->put($path, $replaced);

    }

    /**
     * @return string[]
     */
    private function replaceable(): array
    {

        return [
            '{controller}',
            '{author}',
            '{namespace}'
        ];

    }

}