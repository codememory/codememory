<?php

namespace System\Routing;

use JetBrains\PhpStorm\Pure;
use ReflectionMethod;
use Request;
use Response;
use stdClass;

/**
 * Class RouteAvail
 * @package System\Routing
 *
 * @author  Codememory
 */
class RouteAvail
{

    private const AVAIL_NAMESPACE = 'Avails\%sAvail';

    /**
     * @var stdClass
     */
    private stdClass $route;

    /**
     * @var array
     */
    private array $avails;

    /**
     * RouteAvail constructor.
     *
     * @param array $avails
     * @param array $route
     */
    public function __construct(array $avails, array $route)
    {

        $this->avails = $avails;
        $this->route = (object) $route;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get full class namespace avail
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $name
     *
     * @return string
     */
    #[Pure] private function getNamespace(string $name): string
    {

        return sprintf(
            self::AVAIL_NAMESPACE,
            $name
        );

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Getting all the arguments to pass to the avail method
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return array
     */
    private function getArguments(): array
    {

        $this->route->parameters = (object) $this->route->parameters;

        return [
            'response' => Response::this(),
            'request'  => Request::this(),
            'route'    => $this->route
        ];

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Calling a specific class avail calls the startup method and passes the arguments
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $name
     * @param string $method
     *
     * @return mixed
     * @throws \ReflectionException
     */
    private function contactAvail(string $name, string $method): mixed
    {

        $namespace = $this->getNamespace($name);
        $reflection = new ReflectionMethod($namespace, $method);

        return $reflection->invokeArgs(new $namespace(), $this->getArguments());

    }

    /**
     * @throws \ReflectionException
     */
    private function disassemblyAvails(): void
    {

        foreach ($this->avails as $avail) {
            $this->contactAvail($avail['avail'], $avail['method']);
        }

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Running.....
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @throws \ReflectionException
     */
    public function request(): void
    {

        $this->disassemblyAvails();

    }

}