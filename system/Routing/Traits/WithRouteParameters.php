<?php

/**
 * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
 * & Ready-made methods for route parameters
 * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
 */

namespace System\Routing\Traits;

use System\Routing\Interfaces\RouteRulesInterface;

/**
 * Trait WithRouteParameters
 * @package System\Routing\Traits
 *
 * @author  Codememory
 */
trait WithRouteParameters
{

    /**
     * =>=>=>=>=>=>=>=>=>
     * & Only numbers
     * <=<=<=<=<=<=<=<=<=
     *
     * @param string $name
     *
     * @return $this
     */
    public function withNumeric(string $name): object
    {

        $this->with($name, RouteRulesInterface::NUMERIC, true);

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>
     * & Dotted numbers
     * <=<=<=<=<=<=<=<=<=<=
     *
     * @param string $name
     *
     * @return $this
     */
    public function withFloat(string $name): object
    {

        $this->with($name, RouteRulesInterface::FLOAT, true);

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Smallcase string only
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $name
     *
     * @return $this
     */
    public function withSmallString(string $name): object
    {

        $this->with($name, RouteRulesInterface::SMALL_STRING, true);

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Capitalized string only
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $name
     *
     * @return $this
     */
    public function withBigString(string $name): object
    {

        $this->with($name, RouteRulesInterface::BIG_STRING, true);

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>
     * & Latin letters only
     * <=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $name
     *
     * @return $this
     */
    public function withAlpha(string $name): object
    {

        $this->with($name, RouteRulesInterface::ALPHA, true);

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Only Latin letters and numbers
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $name
     *
     * @return $this
     */
    public function withAlphaNumeric(string $name): object
    {

        $this->with($name, RouteRulesInterface::ALPHA_NUMERIC, true);

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Only Latin letters and numbers and [ _ -. ]
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $name
     *
     * @return $this
     */
    public function withAlphaDash(string $name): object
    {

        $this->with($name, RouteRulesInterface::ALPHA_DASH, true);

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>
     * & Just one digit
     * <=<=<=<=<=<=<=<=<=<=
     *
     * @param string $name
     *
     * @return $this
     */
    public function withOnlyOneNumeric(string $name): object
    {

        $this->with($name, RouteRulesInterface::ONLY_ONE_NUMERIC, true);

        return $this;

    }

}