<?php

namespace System\Routing\Exceptions;

use JetBrains\PhpStorm\Pure;

/**
 * Class ControllerException
 * @package System\Routing\Exceptions
 *
 * @author  Codememory
 */
class ControllerException extends \ErrorException
{

    /**
     * ControllerException constructor.
     *
     * @param string $message
     */
    #[Pure] public function __construct(string $message)
    {

        parent::__construct($message);

    }

}