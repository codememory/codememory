<?php

namespace System\Routing\Interfaces;

/**
 * Class RouteInterface
 * @package System\Routing\Interfaces
 *
 * @author Codememory
 */
class RouteInterface
{

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & default regex for parameters if they are not processed
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     */
    const DEFAULT_RULES = '([^\/]+)';

}