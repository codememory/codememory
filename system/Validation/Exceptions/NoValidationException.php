<?php

namespace System\Validation\Exceptions;

use ErrorException;
use JetBrains\PhpStorm\Pure;

/**
 * Class NoValidationException
 * @package System\Validation\Exceptions
 *
 * @author  Codememory
 */
class NoValidationException extends ErrorException
{

    /**
     * NoValidationException constructor.
     *
     * @param string $build
     */
    #[Pure] public function __construct(string $build)
    {

        parent::__construct(sprintf(
            'The specified object: <b>%s</b> is not a validator class. This class must implement the BuildInterface interface', $build
        ));

    }

}