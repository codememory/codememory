<?php

namespace System\Validation\Exceptions;

use ErrorException;
use JetBrains\PhpStorm\Pure;

/**
 * Class IncorrectRuleParametersException
 * @package System\Validation\Exceptions
 *
 * @author  Codememory
 */
class IncorrectRuleParametersException extends ErrorException
{

    /**
     * IncorrectRuleParametersException constructor.
     *
     * @param string $message
     */
    #[Pure] public function __construct(string $message)
    {

        parent::__construct($message);

    }

}