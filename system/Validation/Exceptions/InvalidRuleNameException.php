<?php

namespace System\Validation\Exceptions;

use ErrorException;
use JetBrains\PhpStorm\Pure;

/**
 * Class InvalidRuleNameException
 * @package System\Validation\Exceptions
 *
 * @author  Codememory
 */
class InvalidRuleNameException extends ErrorException
{

    /**
     * InvalidRuleNameException constructor.
     *
     * @param string|null $rule
     */
    #[Pure] public function __construct(?string $rule)
    {
        parent::__construct(
            sprintf('The name of the rule is incorrect. No validation <b>%s</b> rules found', $rule)
        );
    }

}