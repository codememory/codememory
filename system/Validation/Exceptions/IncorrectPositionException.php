<?php

namespace System\Validation\Exceptions;

use ErrorException;
use JetBrains\PhpStorm\Pure;

/**
 * Class IncorrectPositionException
 * @package System\Validation\Exceptions
 *
 * @author  Codememory
 */
class IncorrectPositionException extends ErrorException
{

    /**
     * @var int
     */
    private int $position;

    /**
     * IncorrectPositionException constructor.
     *
     * @param int   $position
     * @param array $positions
     */
    #[Pure] public function __construct(int $position, array $positions)
    {

        $this->position = $position;

        parent::__construct(
            sprintf(
                'Position <b>%s</b> for validator not found. List of all positions: <b>%s</b>',
                $position, implode(',', $positions) ?? 'null'
            )
        );

    }

    /**
     * @return int
     */
    public function getPosition(): int
    {

        return $this->position;

    }

}