<?php

namespace System\Validation\Rules;

use System\Validation\Exceptions\IncorrectRuleParametersException;

/**
 * Trait Is
 * @package System\Validation\Rules
 *
 * @author  Codememory
 */
trait Is
{

    /**
     * @param string      $ruleName
     * @param string|null $ruleValues
     *
     * @throws IncorrectRuleParametersException
     */
    private function generalException(string $ruleName, ?string $ruleValues): void
    {

        if(null !== $ruleValues) {
            throw new IncorrectRuleParametersException(
                sprintf('The `%s` rules must not contain any parameters', $ruleName)
            );
        }

    }

    /**
     * @param string|null $ruleValues
     * @param mixed       $inputValue
     * @param string      $inputName
     * @param string|null $message
     *
     * @throws IncorrectRuleParametersException
     */
    private function validateInteger(?string $ruleValues, mixed $inputValue, string $inputName, ?string $message): void
    {

        $this->generalException('integer', $ruleValues);

        if(false === is_integer($inputValue)) {
            $this->errors[] = $message;
        }

    }

    /**
     * @param string|null $ruleValues
     * @param mixed       $inputValue
     * @param string      $inputName
     * @param string|null $message
     *
     * @throws IncorrectRuleParametersException
     */
    private function validateString(?string $ruleValues, mixed $inputValue, string $inputName, ?string $message): void
    {

        $this->generalException('string', $ruleValues);

        if(false === is_string($inputValue)) {
            $this->errors[] = $message;
        }

    }

}