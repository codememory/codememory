<?php

namespace System\Validation;

use System\Validation\Exceptions\IncorrectRuleParametersException;
use System\Validation\Rules\Is;

/**
 * Trait ValidationRules
 * @package System\Validation
 *
 * @author  Codememory
 */
trait ValidationRules
{

    use Is;

    /**
     * Checking for the minimum number of characters
     *
     * @param string|null $ruleValues
     * @param mixed       $inputValue
     * @param string      $inputName
     * @param string|null $message
     */
    private function validateMin(?string $ruleValues, mixed $inputValue, string $inputName, ?string $message): void
    {

        if (iconv_strlen($inputValue) < (int) $ruleValues) {
            $this->errors[] = $message;
        }

    }

    /**
     * Checking for the maximum number of characters
     *
     * @param string|null $ruleValues
     * @param mixed       $inputValue
     * @param string      $inputName
     * @param string|null $message
     */
    private function validateMax(?string $ruleValues, mixed $inputValue, string $inputName, ?string $message): void
    {

        if (iconv_strlen($inputValue) < (int) $ruleValues) {
            $this->errors[] = $message;
        }

    }

    /**
     * Checking the number of characters for an interval from and to
     *
     * @param string|null $ruleValues
     * @param mixed       $inputValue
     * @param string      $inputName
     * @param string|null $message
     *
     * @throws IncorrectRuleParametersException
     */
    private function validateInterval(?string $ruleValues, mixed $inputValue, string $inputName, ?string $message): void
    {

        $severalParams = $this->parser->severalValues($ruleValues)['value'];

        if (count($severalParams) < 2 || count($severalParams) > 2) {
            throw new IncorrectRuleParametersException('Only 2 parameters can be specified in this rule. Example `interval: 1,10`');
        }

        $min = $severalParams[0];
        $max = $severalParams[1];

        if (
            iconv_strlen($inputValue) < (int) $min
            || iconv_strlen($inputValue) > (int) $max
        ) {
            $this->errors[] = $message;
        }

    }

}