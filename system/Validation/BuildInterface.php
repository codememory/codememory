<?php

namespace System\Validation;

/**
 * Interface BuildInterface
 * @package System\Validation
 *
 * @author Codememory
 */
interface BuildInterface
{

    public function build(Validator $validator, ...$arguments): void;

}