<?php

namespace System\Validation;

use JetBrains\PhpStorm\Pure;
use System\Validation\Attributes\Attributes;
use System\Validation\Handlers\Call;

/**
 * Class Validator
 * @package System\Validation
 *
 * @author  Codememory
 */
class Validator
{

    /**
     * @var bool
     */
    private bool $validated = false;

    /**
     * @var array
     */
    private array $errors = [];

    /**
     * @var array
     */
    private array $data = [];

    /**
     * @var array
     */
    private array $validates = [];

    /**
     * @var ValueParser
     */
    private ValueParser $parser;

    /**
     * @var Attributes
     */
    private Attributes $attrs;

    /**
     * Validator constructor.
     *
     * @param ValueParser $parser
     * @param Attributes  $attributes
     */
    public function __construct(ValueParser $parser, Attributes $attributes)
    {

        $this->parser = $parser;
        $this->attrs = $attributes;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Set the data with which the validator should work. An array is passed
     * & as an argument, where the key is the name of validate and the value that
     * & is being processed
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array $data
     *
     * @return $this
     */
    public function setData(array $data): Validator
    {

        $this->data = $data;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Creation of validation for processing. As 1 argument, the name of
     * & the data key is specified for which the validation is created.
     * & If there is no such key, validation will wait until it appears
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string          $inputName
     * @param string|callable $ruleOrCallback
     *
     * @return $this
     */
    public function validate(string $inputName, string|callable $ruleOrCallback): Validator
    {

        $validate = new Validate($inputName, $ruleOrCallback, $this->data, $this->attrs, $this->parser);

        if (is_string($ruleOrCallback)) {
            $validate->setRules($ruleOrCallback);
        }

        $this->validates[$inputName][] = is_string($ruleOrCallback) ? $validate : call_user_func($ruleOrCallback, $validate, $inputName);

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns a boolean value by checking if the data was validated
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool
     */
    public function isValidated(): bool
    {

        return $this->validated;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get an array of errors that occurred during data validation
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return array
     */
    public function getErrors(): array
    {

        return $this->errors;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get the first error. Recommended method
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return int|string|null
     */
    public function getFirstError(): int|string|null
    {

        return current($this->errors) ?: null;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get the average error, if the number of errors is not even, a
     * & running average error will be selected
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return int|string|null
     */
    #[Pure] public function getMiddleError(): int|string|null
    {

        $middleKey = null;

        if ([] !== $this->errors) {
            $count = 0;

            foreach ($this->errors as $key => $error) {
                $count++;

                if ($count === (int) round(count($this->errors) / 2)) {
                    $middleKey = $count;
                }
            }
        }

        if (null !== $middleKey && array_key_exists($middleKey, $this->errors)) {
            return $this->errors[$middleKey > 0 ? ($middleKey - 1) : $middleKey];
        }

        return null;

    }


    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>
     * & Get the last error
     * <=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return int|string|null
     */
    public function getLastError(): int|string|null
    {

        return end($this->errors) ?: null;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Method returns the isValid method and passes the error
     * & message to the argument (link)
     *
     * Example:
     * if(!$validator->isValidatedWithError($errorMessage)) {
     *    echo $errorMessage;
     * }
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param int|string|null $errorMessage
     *
     * @return bool
     */
    public function isValidatedWithError(int|string|null &$errorMessage): bool
    {

        $errorMessage = $this->getFirstError();

        return $this->isValidated();

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>
     * & Make validation run
     * <=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return $this
     * @throws Exceptions\InvalidRuleNameException
     */
    public function make(): Validator
    {

        $call = new Call($this->attrs, $this->parser, $this->validates, $this->data);
        $this->errors = $call->make();

        if ([] === $this->errors) {
            $this->validated = true;
        } else {
            $this->validated = false;
        }

        return $this;

    }

}