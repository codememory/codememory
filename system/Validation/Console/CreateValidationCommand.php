<?php

namespace System\Validation\Console;

use File;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use System\FileSystem\Different\Exceptions\IncorrectPathException;

/**
 * Class CreateValidationCommand
 * @package System\Validation\Console
 *
 * @author  Codememory
 */
class CreateValidationCommand extends Command
{

    private const PREFIX = 'Validate';
    private const PATH_TO_VALIDATIONS = 'app/Validations';

    /**
     * @var InputInterface|null
     */
    private ?InputInterface $input = null;

    /**
     * @var SymfonyStyle|null
     */
    private ?SymfonyStyle $io = null;

    /**
     * @var array
     */
    private array $rules = [];

    protected function configure(): void
    {

        $this
            ->setName('validation:create')
            ->addArgument('name', InputArgument::REQUIRED, 'Имя Валидации(<fg=yellow>классс</>)')
            ->addOption('rules', null, InputOption::VALUE_NONE, 'Добавить правила к валидации')
            ->addOption('re-create', null, InputOption::VALUE_NONE, 'Пересоздать валидацию, если класс с таким именем уже существует')
            ->setDescription('Создать валидацию');

    }

    /**
     * @param string $name
     *
     * @return string
     */
    private function getClassName(string $name): string
    {

        return $name . self::PREFIX;

    }

    /**
     * @param string $name
     * @param bool   $asFile
     *
     * @return string
     */
    #[Pure] private function getPathWithClassName(string $name, bool $asFile = false): string
    {

        $path = sprintf('%s/%s', self::PATH_TO_VALIDATIONS, $this->getClassName($name));

        if ($asFile) {
            $path .= '.php';
        }

        return $path;

    }

    /**
     * @param string $name
     *
     * @return bool
     */
    private function exists(string $name): bool
    {

        return File::exists($this->getPathWithClassName($name, true));

    }

    /**
     * @param string      $author
     * @param string      $className
     * @param string|null $body
     *
     * @return string
     */
    private function getStub(string $author, string $className, ?string $body): string
    {

        $stub = File::read('system/Validation/Console/Stubs/ValidateStub.stub');

        return str_replace(
            ['{author}', '{className}', '{body}'],
            [$author, $className, $body],
            $stub
        );

    }

    /**
     * @param string $keyData
     */
    private function addRule(string $keyData): void
    {

        $rule = $this->io->ask('Укажите название правила <fg=red>[отменить = ENTER]</>');

        if (null !== $rule) {
            $ruleValue = $this->io->ask(sprintf('Значение правила [<fg=white>%s</>]', $rule));

            $message = $this->io->ask(sprintf(
                'Укажите сообщение для правила [<fg=white>%s</>] которое сработает в случае ошибки',
                $rule
            ));

            if (null !== $message) {
                $this->rules[$keyData][] = [
                    'rule'         => $rule,
                    'ruleValue'    => $ruleValue,
                    'errorMessage' => $message
                ];

                $this->addRule($keyData);
            }
        } else {
            $this->addRules();
        }

    }

    private function addRules(): void
    {

        $this->io->ask('Укажите имя(<fg=yellow>ключ</>) данных для которого создать правило <fg=red>[отменить = ENTER]</>', null, function ($value) {
            if (null !== $value) {
                $this->rules[$value] = [];
                $this->addRule($value);
            }
        });

    }

    /**
     * @return string
     */
    private function renderCodeWithRules(): string
    {

        $fullCode = null;

        foreach ($this->rules as $dataKey => $rules) {
            $stringRules = null;
            $message = null;

            foreach ($rules as $index => $ruleInfo) {
                $value = null;

                if (null !== $ruleInfo['ruleValue']) {
                    $value = ':' . $ruleInfo['ruleValue'];
                }

                $stringRules .= sprintf("'%s%s',", $ruleInfo['rule'], $value);
                $message .= <<<MESSAGE
                        \t\t\t->setMessage('{$ruleInfo['errorMessage']}')\n
                MESSAGE;

            }

            $code = <<<VALIDATE
            \n\t\t\t->validate('{dataKey}', function (Validate \$validate) {
                \t\t\t\$validate->setRules({rules})
            {messages}
                    
                \t\t\treturn \$validate;
            \t\t\t})
            VALIDATE;
            $fullCode .= str_replace(
                ['{dataKey}', '{rules}', '{messages}'],
                [$dataKey, substr($stringRules, 0, -1), substr($message, 0, -1) . ';'],
                $code
            );
        }

        return sprintf("\$validator%s;", $fullCode);

    }

    /**
     * @param string      $author
     * @param string|null $body
     *
     * @throws IncorrectPathException
     */
    private function createValidate(string $author, ?string $body): void
    {

        $stub = $this->getStub($author, $this->getClassName($this->input->getArgument('name')), $body);

        File::editor()->put($this->getPathWithClassName($this->input->getArgument('name'), true), $stub);

    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws IncorrectPathException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $this->io = new SymfonyStyle($input, $output);
        $this->input = $input;
        $name = $this->input->getArgument('name');
        $exists = $this->exists($name);

        if ($exists && false === $this->input->getOption('re-create')) {
            $this->io->error([
                sprintf('Валидация с именем %s уже существует.', $name),
                'Укажите опцию --re-create, если хотите пересоздать валидацию.'
            ]);

            return Command::FAILURE;
        }

        $author = $this->io->ask('Укажите имя автора', 'Codememory');

        if (false === $this->input->getOption('rules')) {
            $this->createValidate($author, null);
        } else {
            $this->addRules();

            $this->createValidate($author, $this->renderCodeWithRules());
        }

        $this->io->success([
            sprintf('Валидация %s успешно создана', $this->getClassName($name)),
            sprintf('Путь: %s', $this->getPathWithClassName($name, true))
        ]);

        return Command::SUCCESS;

    }

}