<?php

namespace System\Validation;

/**
 * Class ValueParser
 * @package System\Validation
 *
 * @author  Codememory
 */
class ValueParser
{

    private const RULE_NAME_OPERATOR = ':';
    private const MULTIPLE_VALUE_OPERATOR = ',';
    private const SINGLE_VALUE_OPERATOR = ':';

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Find and get from the rule his name
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $value
     *
     * @return string
     */
    public function getRuleName(string $value): string
    {

        return explode(self::RULE_NAME_OPERATOR, $value)[0];

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get an array of multiple values from a rule
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $value
     *
     * @return array
     */
    public function severalValues(string $value): array
    {

        $values = explode(self::MULTIPLE_VALUE_OPERATOR, $value);

        return [
            'name'  => $this->getRuleName($value),
            'value' => $values
        ];

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get value from rule
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $rule
     *
     * @return array
     */
    public function singleValue(string $rule): array
    {

        $value = null;
        $pattern = sprintf('/[^%1$s]+%1$s(?<value>.*)/', self::SINGLE_VALUE_OPERATOR);

        if (preg_match($pattern, $rule, $match) ) {
            $value = $match['value'];
        }

        return [
            'name'  => $this->getRuleName($rule),
            'value' => $value
        ];

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get all the value "or" from a rule
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $value
     *
     * @return array
     */
    public function explodeOr(string $value): array
    {

        return [
            'name'  => $this->getRuleName($value),
            'value' => explode('|', $value)[1]
        ];

    }

}