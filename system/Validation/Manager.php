<?php

namespace System\Validation;

use JetBrains\PhpStorm\Pure;
use ReflectionClass;
use System\Validation\Attributes\Attributes;
use System\Validation\Exceptions\NoValidationException;

/**
 * Class Manager
 * @package System\Validation
 *
 * @author  Codememory
 */
class Manager
{

    private const NAME_BUILD_METHOD = 'build';
    private const BUILD_INTERFACE = 'System\Validation\BuildInterface';

    /**
     * @var Validator
     */
    private Validator $validator;

    /**
     * Manager constructor.
     */
    #[Pure] public function __construct()
    {

        $this->validator = new Validator(
            new ValueParser(),
            new Attributes()
        );

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The main method in which the namespace of the validation is passed and
     * & the data with which you need to work. This method checks the implementation
     * & of the interface, if the validation class does not implement the interface,
     * & then an exception will be thrown
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array  $data
     * @param string $validate
     * @param mixed  ...$arguments
     *
     * @return Validator
     * @throws Exceptions\InvalidRuleNameException
     * @throws NoValidationException
     * @throws \ReflectionException
     */
    public function track(array $data, string $validate, ...$arguments): Validator
    {

        $reflection = new ReflectionClass($validate);
        $implement = $reflection->implementsInterface(self::BUILD_INTERFACE);

        if (false === $implement) {
            throw new NoValidationException($validate);
        }

        return $this->call($data, $reflection, $arguments);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method calls the main build method from the validation class
     * & and creates the validation
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array           $data
     * @param ReflectionClass $reflection
     * @param                 $arguments
     *
     * @return Validator
     * @throws Exceptions\InvalidRuleNameException
     * @throws \ReflectionException
     */
    private function call(array $data, ReflectionClass $reflection, array $arguments = []): Validator
    {

        $object = $reflection->getName();
        $validator = clone $this->validator;

        $reflection
            ->getMethod(self::NAME_BUILD_METHOD)
            ->invoke(new $object, $validator, ...$arguments);

        return $validator->setData($data)->make();

    }


}