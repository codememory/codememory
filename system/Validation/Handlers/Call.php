<?php

namespace System\Validation\Handlers;

use JetBrains\PhpStorm\Pure;
use System\Validation\Attributes\Attributes;
use System\Validation\Exceptions\InvalidRuleNameException;
use System\Validation\Validate;
use System\Validation\ValidationRules;
use System\Validation\ValueParser;

/**
 * Class Call
 * @package System\Validation\Handlers
 *
 * @author  Codememory
 */
class Call
{

    use ValidationRules;

    /**
     * @var Attributes
     */
    private Attributes $attr;

    /**
     * @var array
     */
    private array $validates;

    /**
     * @var array
     */
    private array $input;

    /**
     * @var ValueParser
     */
    private ValueParser $parser;

    /**
     * @var array
     */
    public array $errors = [];

    /**
     * Call constructor.
     *
     * @param Attributes  $attributes
     * @param ValueParser $parser
     * @param array       $validates
     * @param array       $data
     */
    #[Pure] public function __construct(Attributes $attributes, ValueParser $parser, array $validates, array $data)
    {

        $this->parser = $parser;
        $this->attr = $attributes;
        $this->validates = $validates;
        $this->input = $data;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Method creates and returns the fully qualified method name
     * & of a specific rule
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $rule
     *
     * @return string
     */
    #[Pure] private function getRuleMethodName(string $rule): string
    {

        return 'validate' . ucfirst($rule);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns a boolean value by checking if the rule exists.
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $rule
     *
     * @return bool
     */
    #[Pure] private function ruleExist(string $rule): bool
    {

        return method_exists($this, $this->getRuleMethodName($rule));

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The main method that is called a specific rule for execution.
     * & Having passed arguments to it
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array    $rules
     * @param string   $ruleName
     * @param Validate $validate
     */
    private function callRule(array $rules, string $ruleName, Validate $validate): void
    {

        $message = $rules['message'] ?? [];
        $attributes = $message['attributes'] ?? [];

        call_user_func_array(
            [$this, $this->getRuleMethodName($ruleName)],
            [
                $this->parser->singleValue($rules['rule'])['value'],
                $this->input[$validate->getInputName()] ?? null,
                $validate->getInputName(),
                $this->attr->attributeInit($validate, $rules['rule'], $ruleName)->getMessage($message['text'] ?? null, $attributes)
            ]
        );

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & This method checks for the existence of a rule before executing it.
     * & If the rule does not exist, then an "InvalidRuleNameException" exception
     * & will be thrown; otherwise, the main method for starting the rule is launched
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array    $rules
     * @param Validate $validate
     *
     * @throws InvalidRuleNameException
     */
    private function ruleProcessing(array $rules, Validate $validate): void
    {

        $ruleName = $this->parser->getRuleName($rules['rule']);

        if (false === $this->ruleExist($ruleName)) {
            throw new InvalidRuleNameException($ruleName);
        }

        $this->callRule($rules, $ruleName, $validate);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & This method goes through all the specified rules and performs processing on them
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $name
     * @param array  $validates
     *
     * @throws InvalidRuleNameException
     */
    private function validates(string $name, array $validates): void
    {

        foreach ($validates as $validate) {
            foreach ($validate->rules as $rules) {
                $this->ruleProcessing($rules, $validate);
            }
        }

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & This method starts the rules and returns an array of errors that
     * & occurred during the validation.
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return array
     * @throws InvalidRuleNameException
     */
    public function make(): array
    {

        if ([] !== $this->validates) {
            foreach ($this->validates as $name => $validate) {
                $this->validates($name, $validate);
            }
        }

        return $this->errors;

    }

}