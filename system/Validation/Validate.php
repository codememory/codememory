<?php

namespace System\Validation;

use Closure;
use System\Validation\Attributes\Attributes;
use System\Validation\Exceptions\IncorrectPositionException;

/**
 * Class Validate
 * @package System\Validation
 *
 * @author  Codememory
 */
class Validate
{

    /**
     * @var array
     */
    public array $rules = [];

    /**
     * @var array
     */
    private array $messages = [];

    /**
     * @var string
     */
    public string $inputName;

    /**
     * @var array
     */
    public array $data = [];

    /**
     * @var Closure|string
     */
    private string|Closure $ruleOrCallback;

    /**
     * @var ValueParser
     */
    public ValueParser $parser;

    /**
     * @var Attributes
     */
    private Attributes $attrs;

    /**
     * Validate constructor.
     *
     * @param string          $inputName
     * @param string|callable $ruleOrCallback
     * @param array           $data
     * @param Attributes      $attributes
     * @param ValueParser     $parser
     */
    public function __construct(string $inputName, string|callable $ruleOrCallback, array $data, Attributes $attributes, ValueParser $parser)
    {

        $this->inputName = $inputName;
        $this->ruleOrCallback = $ruleOrCallback;
        $this->data = $data;
        $this->parser = $parser;
        $this->attrs = $attributes;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Set several rules for one processed value
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string ...$rules
     *
     * @return $this
     */
    public function setRules(string ...$rules): Validate
    {

        array_map(function (string $rule) {
            $this->rules[]['rule'] = $rule;
        }, $rules);

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Set a message for the rule that will be written to the list of
     * & errors in case the value did not pass the rule. Messages can be
     * & listed out of order. But in this case, as the 2nd argument, you
     * & need to pass the validation position for which the message is created
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param int|string $message
     * @param int|null   $position
     *
     * @return Validate
     * @throws IncorrectPositionException
     */
    public function setMessage(int|string $message, ?int $position = null): Validate
    {

        if (null !== $position) {
            $this->messages[$position] = $message;
        } else {
            $this->messages[] = $message;
        }

        return $this->addMessageHandler();

    }

    /**
     * @param int   $position
     * @param mixed $message
     */
    private function handlerAddRuleInfo(int $position, mixed $message): void
    {

        $this->rules[$position]['message'] = [
            'text'       => $message,
            'attributes' => $this->attrs->searchAttrs($message)
        ];

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The main processing method that will write a message for a specific rule
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return Validate
     * @throws IncorrectPositionException
     */
    private function addMessageHandler(): Validate
    {

        if ([] !== $this->messages) {
            foreach ($this->messages as $position => $message) {
                if (false === array_key_exists($position, $this->rules)) {
                    throw new IncorrectPositionException($position, array_keys($this->rules));
                } else {
                    $this->handlerAddRuleInfo($position, $message);
                }
            }
        }

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get the name of the created field for which the validation is being created
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return string
     */
    public function getInputName(): string
    {

        return $this->inputName;

    }

}