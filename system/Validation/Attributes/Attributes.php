<?php

namespace System\Validation\Attributes;

use System\Validation\Validate;

/**
 * Class Attributes
 * @package System\Validation\Attributes
 *
 * @author  Codememory
 */
class Attributes
{

    private const RULE_ATTRIBUTE = '%(?<attributes>[^\%]+)%';

    /**
     * @var array
     */
    private array $acceptedAttributes = [];

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Finding all attributes in a validation error message
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $message
     *
     * @return array
     */
    public function searchAttrs(string $message): array
    {

        preg_match_all(sprintf('/%s/', self::RULE_ATTRIBUTE), $message, $match);

        return $match['attributes'];

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Initializing all available attributes
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param Validate $validate
     * @param string   $rule
     * @param string   $ruleName
     *
     * @return $this
     */
    public function attributeInit(Validate $validate, string $rule, string $ruleName): Attributes
    {

        $this->acceptedAttributes = [
            'in-name'  => $validate->inputName,
            'in-value' => $validate->data[$validate->inputName] ?? null,
            'r-name'   => $ruleName,
            'r-value'  => $validate->parser->singleValue($rule)['value']
        ];

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Getting a ready-made error message. With processed attributes
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|null $message
     * @param array       $attrs
     *
     * @return string|null
     */
    public function getMessage(?string $message = null, array $attrs = []): ?string
    {

        if ([] !== $attrs) {
            foreach ($attrs as $attr) {
                $fullAttr = '%' . $attr . '%';

                if (array_key_exists($attr, $this->acceptedAttributes)) {
                    $message = str_replace($fullAttr, $this->acceptedAttributes[$attr], $message);
                }
            }
        }

        return $message;

    }

}