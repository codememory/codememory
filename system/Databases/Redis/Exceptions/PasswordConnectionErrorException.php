<?php

namespace System\Databases\Redis\Exceptions;

use ErrorException;
use JetBrains\PhpStorm\Pure;

/**
 * Class PasswordConnectionErrorException
 * @package System\Databases\Redis\Exceptions
 *
 * @author  Codememory
 */
class PasswordConnectionErrorException extends ErrorException
{

    /**
     * PasswordConnectionErrorException constructor.
     */
    #[Pure] public function __construct()
    {

        parent::__construct(
            sprintf('Failed to connect to Redis with specified password')
        );

    }

}