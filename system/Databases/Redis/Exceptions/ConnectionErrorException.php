<?php

namespace System\Databases\Redis\Exceptions;

use ErrorException;
use JetBrains\PhpStorm\Pure;

/**
 * Class ConnectionErrorException
 * @package System\Databases\Redis\Exceptions
 *
 * @author  Codememory
 */
class ConnectionErrorException extends ErrorException
{

    /**
     * @var int|string
     */
    private int|string $host;

    /**
     * @var int
     */
    private int $port;

    /**
     * ConnectionErrorException constructor.
     *
     * @param string|int $host
     * @param int        $port
     */
    #[Pure] public function __construct(string|int $host, int $port)
    {

        $this->host = $host;
        $this->port = $port;

        parent::__construct(
            sprintf('Failed to connect to Redis on host <b>%s</b> with port <b>%s</b>', $host, $port)
        );

    }

    /**
     * @return string|int
     */
    public function getHost(): string|int
    {

        return $this->host;

    }

    /**
     * @return int
     */
    public function getPort(): int
    {

        return $this->port;

    }

}