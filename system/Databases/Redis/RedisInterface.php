<?php

namespace System\Databases\Redis;

use Redis;

/**
 * Interface RedisInterface
 * @package System\Databases\RedisManager
 *
 * @author  Codememory
 */
interface RedisInterface
{

    /**
     * @param int $db
     *
     * @return RedisManager|Redis
     */
    public function selectDb(int $db): RedisManager|Redis;

    /**
     * @param string   $name
     * @param bool     $ifMissing
     * @param int|null $life
     *
     * @return RedisManager
     */
    public function createTable(string $name, bool $ifMissing = true, ?int $life = null): RedisManager;

    /**
     * @param string $name
     *
     * @return RedisManager
     */
    public function selectTable(string $name): RedisManager;

    /**
     * @param string $column
     * @param mixed  $value
     *
     * @return RedisManager
     */
    public function addRecord(string $column, mixed $value): RedisManager;

    /**
     * @param string $column
     *
     * @return mixed
     */
    public function getRecord(string $column): mixed;

    /**
     * @return array
     */
    public function getRecords(): array;

    /**
     * @param string $column
     * @param mixed  $value
     *
     * @return bool
     */
    public function updateRecord(string $column, mixed $value): bool;

    /**
     * @param string   $column
     * @param callable $callback
     *
     * @return bool
     */
    public function updateRecordWithAdd(string $column, callable $callback): bool;

    /**
     * @param mixed ...$columns
     *
     * @return bool
     */
    public function removeRecord(...$columns): bool;

}