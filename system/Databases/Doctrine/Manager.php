<?php

namespace System\Databases\Doctrine;

use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Setup;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Console\Application;

/**
 * Class Manager
 * @package System\Databases\Doctrine
 *
 * @author  Codememory
 */
class Manager
{

    /**
     * @var Utils
     */
    private Utils $utils;

    /**
     * @var Application|null
     */
    private ?Application $app;

    /**
     * @var Runner|null
     */
    private ?Runner $runner;

    /**
     * @var EntityManager|null
     */
    public EntityManager|null $entityManager = null;

    /**
     * Manager constructor.
     *
     * @param Utils            $utils
     * @param Application|null $app
     */
    #[Pure] public function __construct(Utils $utils, ?Application $app)
    {

        $this->utils = $utils;
        $this->app = $app;

        if(null !== $app) {
            $this->runner = new Runner($this->utils, $this->app);
        }

    }

    /**
     * @return Configuration
     */
    private function initMetadata(): Configuration
    {

        return Setup::createAnnotationMetadataConfiguration(
            [$this->utils->pathToAllEntity()],
            $this->utils->devMode(),
            $this->utils->proxyDir(),
            empty($this->utils->cache()) ? null : $this->utils->cache(),
            $this->utils->simpleAnnotationReader()
        );

    }

    /**
     * @return $this
     * @throws ORMException
     */
    public function connection(): Manager
    {

        $connection = $this->utils->connection();

        $entity = EntityManager::create(
            array_merge($connection, ['driver' => $this->utils->driver()]),
            $this->initMetadata()
        );

        $this->entityManager = $entity;

        return $this;

    }

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function helperSet(EntityManagerInterface $entityManager): void
    {

        $this->runner
            ->helperSet($entityManager)
            ->addCommands();

    }

}