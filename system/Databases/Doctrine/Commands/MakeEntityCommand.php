<?php

namespace System\Databases\Doctrine\Commands;

use File;
use JetBrains\PhpStorm\Pure;
use RuntimeException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use System\Console\Command;
use System\Databases\Doctrine\Utils;
use System\FileSystem\Different\Exceptions\IncorrectPathException;
use System\Support\Str;

/**
 * Class MakeEntityCommand
 * @package System\Databases\Doctrine\Commands
 *
 * @author  Codememory
 */
class MakeEntityCommand extends Command
{

    /**
     * @var string|null
     */
    protected ?string $command = 'make:entity';

    /**
     * @var string|null
     */
    protected ?string $description = 'Creating an entity class';

    /**
     * @var array
     */
    private array $columns = [];

    /**
     * @var string|null
     */
    private ?string $properties = null;

    /**
     * @var string|null
     */
    private ?string $methods = null;

    /**
     * @return Command
     */
    protected function wrapArgsAndOptions(): Command
    {

        $this
            ->argument('name', InputArgument::REQUIRED, 'Entity name')
            ->option('no-rep', null, InputOption::VALUE_NONE, 'Don\'t create a repository for this entity');

        return $this;

    }

    /**
     * @return string
     */
    private function tableName(): string
    {

        return $this->io->ask('Specify the name of the table', null, function (mixed $value) {
            if (null === $value) {
                throw new RuntimeException('Table name must not be an empty string');
            }

            return $value;
        });

    }

    /**
     * @param mixed $column
     *
     * @return mixed
     */
    private function typeColumn(mixed $column): mixed
    {

        return $this->io->ask(sprintf(
            'Enter the column type [%s]',
            $this->tags->blueText($column)
        ), null, function (mixed $type) {
            if (null === $type) {
                throw new RuntimeException('Column type cannot be nullable');
            }

            return $type;
        });

    }

    /**
     * @return mixed
     */
    private function lengthColumn(): mixed
    {

        return $this->io->ask(sprintf(
            'Maximum line length [%s]',
            $this->tags->whiteText('null')
        ), null, function (mixed $length) {
            if (false === is_numeric($length) && false === empty($length)) {
                throw new RuntimeException('The length of the string must be either empty or a number');
            }

            return $length;
        });

    }

    /**
     * @param mixed $column
     *
     * @return bool|mixed
     */
    private function nullableColumn(mixed $column): bool
    {

        return $this->io->confirm(sprintf(
            'Whether NULL values are allowed for the [%s] column',
            $this->tags->blueText($column)
        ), true);

    }

    private function interview(): void
    {

        $this->io->ask('Enter column name', null, function (mixed $column) {
            if (null === $column) {
                return;
            }

            $this->columns[] = [
                'name'     => $column,
                'type'     => $this->typeColumn($column),
                'length'   => $this->lengthColumn(),
                'nullable' => $this->nullableColumn($column),
                'methods'  => $this->io->choice('Choose which methods to add for these fields', ['set', 'get', 'set&get'], 2)
            ];

            $text = '%s + 1 column %s';
            $this->io->text(sprintf(
                $text,
                $this->tags->greenText('[OK]'),
                $this->tags->yellowText($column)
            ));
            $this->io->newLine(2);

            $this->interview();
        });

    }

    /**
     * @return string
     */
    private function createGetter(): string
    {

        return <<<GETTER
        
            public function get%s(): mixed
            {
            
                return \$this->%s;
                
            }
            
        GETTER;


    }

    /**
     * @return string
     */
    private function createSetter(): string
    {

        return <<<GETTER
        
            public function set%s(mixed \$value): %s
            {
            
                \$this->%s = \$value;
                
                return \$this;
                
            }
            
        GETTER;

    }

    /**
     * @param array $annotations
     *
     * @return string
     */
    #[Pure] private function createDoc(array $annotations): string
    {

        $doc = null;

        foreach ($annotations as $annotation) {
            $doc .= sprintf(" * %s\n", $annotation);
        }

        $doc = substr($doc, 0, -1);

        return sprintf("/**\n%s\n */\n", $doc);

    }

    /**
     * @param string $name
     * @param array  $annotations
     *
     * @return string
     */
    private function createProperty(string $name, array $annotations): string
    {

        $property = <<<PROPERTY
        %s
            protected $%s;\n\n\t
        PROPERTY;

        $doc = explode(PHP_EOL, substr($this->createDoc($annotations), 0, -1));
        foreach ($doc as $key => &$item) {
            if ($key !== 0) {
                $item = "\t" . $item;
            }
        }

        return sprintf($property, implode(PHP_EOL, $doc), Str::camelCase($name));

    }

    /**
     * @param array  $column
     * @param string $entityName
     *
     * @return $this
     */
    private function addSetter(array $column, string $entityName): MakeEntityCommand
    {

        $this->methods .= sprintf(
            $this->createSetter(),
            ucfirst(Str::camelCase($column['name'])),
            $entityName,
            Str::camelCase($column['name'])
        );

        return $this;

    }

    /**
     * @param array $column
     *
     * @return $this
     */
    private function addGetter(array $column): MakeEntityCommand
    {

        $this->methods .= sprintf(
            $this->createGetter(),
            ucfirst(Str::camelCase($column['name'])),
            Str::camelCase($column['name'])
        );

        return $this;

    }

    /**
     * @return string
     */
    private function stub(): string
    {

        return File::read('system/Databases/Doctrine/Commands/Stubs/EntityStub.stub');

    }

    /**
     * @param string $namespace
     * @param string $table
     * @param string $className
     *
     * @return string
     */
    private function replacer(string $namespace, string $table, string $className, array $repository = []): string
    {

        $docRepository = null;

        if ([] !== $repository) {
            $docRepository = sprintf(
                '(repositoryClass="%s")',
                $repository['namespace'] . '\\' . $repository['className']
            );
        }

        $classDoc = $this->createDoc([
            '@ORM\Entity' . $docRepository,
            sprintf('@ORM\Table(name="%s")', $table)
        ]);
        $classDoc = substr($classDoc, 0, -1);

        $stub = $this->stub();

        Str::replace($stub, ['{namespaceEntities}', '{classDoc}', '{className}', '{property}', '{methods}'], [$namespace, $classDoc, $className, $this->properties, $this->methods]);

        return $stub;

    }

    /**
     * @param array  $repositoryInfo
     * @param string $path
     *
     * @return MakeEntityCommand
     * @throws IncorrectPathException
     */
    private function createRepository(array $repositoryInfo, string $path): MakeEntityCommand
    {

        $stub = File::read('system/Databases/Doctrine/Commands/Stubs/RepositoryStub.stub');

        Str::replace($stub, ['{namespace}', '{className}'], [$repositoryInfo['namespace'], $repositoryInfo['className']]);

        File::editor()->put($path . '/' . $repositoryInfo['className'] . '.php', $stub);

        return $this;

    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws IncorrectPathException
     */
    protected function handler(InputInterface $input, OutputInterface $output): int
    {

        $doctrineUtils = new Utils('doctrine');
        $tableName = $this->tableName();
        $entityName = $input->getArgument('name') . $doctrineUtils->prefix();
        $pathRep = $doctrineUtils->repository()['path'];
        $namespaceRep = $doctrineUtils->repository()['namespace'];
        $filenameRepository = $input->getArgument('name') . $doctrineUtils->repository()['prefix'];

        $repositoryInfo = [
            'namespace' => $namespaceRep,
            'className' => $filenameRepository
        ];

        if ($input->getOption('no-rep')) {
            $repositoryInfo = [];
        } else {
            $this->createRepository($repositoryInfo, $pathRep);
        }

        $this->interview();

        foreach ($this->columns as $column) {
            $length = null === $column['length'] ? '' : ' length=%s,';
            $annotations = [
                sprintf(
                    '@ORM\Column(type="%s",%s name="%s", nullable=%s)',
                    $column['type'],
                    sprintf($length, $column['length']),
                    $column['name'],
                    false === $column['nullable'] ? 'false' : 'true'
                )
            ];

            if ($column['name'] === 'id') {
                array_unshift($annotations, '@ORM\Id');
                array_push($annotations, '@ORM\GeneratedValue');
            }

            $this->properties .= $this->createProperty($column['name'], $annotations);

            if ($column['methods'] === 'set') {
                $this->addSetter($column, $entityName);
            } elseif ($column['methods'] === 'get') {
                $this->addGetter($column);
            } else {
                $this
                    ->addSetter($column, $entityName)
                    ->addGetter($column);
            }
        }

        $this->io->success([
            sprintf('The entity has been successfully created. Added columns %d', count($this->columns))
        ]);

        $readyStub = $this->replacer($doctrineUtils->namespace(), $tableName, $entityName, $repositoryInfo);
        $path = $doctrineUtils->pathToAllEntity() . '/' . $entityName . '.php';

        File::editor()->put($path, $readyStub);

        return Command::SUCCESS;

    }

}