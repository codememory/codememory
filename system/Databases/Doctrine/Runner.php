<?php

namespace System\Databases\Doctrine;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Helper\HelperSet;
use Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper;

/**
 * Class Runner
 * @package System\Databases\Doctrine
 *
 * @author  Codememory
 */
class Runner
{

    /**
     * @var Utils
     */
    private Utils $utils;

    /**
     * @var Application
     */
    private Application $app;

    /**
     * @var HelperSet
     */
    private HelperSet $helperSet;

    /**
     * Runner constructor.
     *
     * @param Utils       $utils
     * @param Application $app
     */
    public function __construct(Utils $utils, Application $app)
    {

        $this->utils = $utils;
        $this->app = $app;

    }

    /**
     * @param EntityManagerInterface $entityManager
     *
     * @return $this
     */
    public function helperSet(EntityManagerInterface $entityManager): Runner
    {

        $this->helperSet = new HelperSet([
            'em' => $this->entityManager($entityManager),
            'db' => new ConnectionHelper($entityManager->getConnection())
        ]);

        return $this;

    }

    /**
     * @param EntityManagerInterface $entityManager
     *
     * @return EntityManagerHelper
     */
    #[Pure] private function entityManager(EntityManagerInterface $entityManager): EntityManagerHelper
    {

        return new EntityManagerHelper($entityManager);

    }

    /**
     * @return bool
     */
    public function addCommands(): bool
    {

        $this->app->setCatchExceptions(true);
        $this->app->setHelperSet($this->helperSet);

        ConsoleRunner::addCommands($this->app);

        return true;

    }

}