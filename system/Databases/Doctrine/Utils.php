<?php

namespace System\Databases\Doctrine;

use Config;
use System\Support\Str;

/**
 * Class Utils
 * @package System\Databases\Doctrine
 *
 * @author  Codememory
 */
class Utils
{

    private const DRIVER = 'pdo_sqlite';

    /**
     * @var array|mixed
     */
    private array $config;

    /**
     * Utils constructor.
     *
     * @param string $type
     */
    public function __construct(string $type)
    {

        $config = Config::open('configs.packages')->get($type);
        $config = false === is_array($config) ? [] : $config;

        $this->config = $config;

    }

    /**
     * @return Utils
     */
    public function getDoctrineUtils(): Utils
    {

        return clone new self('doctrine');

    }

    /**
     * @return Utils
     */
    public function getDoctrineMigrationsUtils(): Utils
    {

        return clone new self('doctrineMigrations');

    }

    /**
     * @return false|mixed
     */
    public function devMode(): bool
    {

        return $this->config['devMode'] ?? false;

    }

    /**
     * @return mixed|null
     */
    public function proxyDir(): mixed
    {

        return $this->config['proxyDir'] ?? null;

    }

    /**
     * @return mixed|null
     */
    public function cache(): mixed
    {

        return $this->config['cache'] ?? null;

    }

    /**
     * @return bool|mixed
     */
    public function simpleAnnotationReader(): bool
    {

        return $this->config['simpleAnnotationReader'] ?? true;

    }

    /**
     * @return array
     */
    public function orm(): array
    {

        return $this->getDoctrineUtils()->config['orm'] ?? [];

    }

    /**
     * @return string|null
     */
    public function pathToAllEntity(): ?string
    {

        return Str::asPath($this->orm()['pathToAllEntity']) ?? null;

    }

    /**
     * @return mixed|null
     */
    public function namespace(): ?string
    {

        return $this->orm()['namespace'] ?? null;

    }

    /**
     * @return mixed|null
     */
    public function prefix(): ?string
    {

        return $this->orm()['prefix'] ?? null;

    }

    /**
     * @return array
     */
    public function repository(): array
    {

        $repository = $this->getDoctrineUtils()
                          ->config['orm']['repository'] ?? [];

        return [
            'path'      => Str::asPath($repository['path'] ?? '.'),
            'namespace' => $repository['namespace'] ?? null,
            'prefix'    => $repository['prefix'] ?? null
        ];

    }

    /**
     * @return string|null
     */
    public function connectionDataFrom(): ?string
    {

        return $this->getDoctrineUtils()
                   ->config['connection']['connectionDataFrom'] ?? null;

    }

    /**
     * @param string|null $host
     * @param string|null $user
     * @param string|null $password
     * @param string|null $dbname
     *
     * @return null[]|string[]
     */
    private function dataConnection(?string $host = null, ?string $user = null, ?string $password = null, ?string $dbname = null): array
    {

        return [
            'host'     => $host,
            'user'     => $user,
            'password' => $password,
            'dbname'   => $dbname
        ];

    }

    /**
     * @return array|null[]
     */
    public function connection(): array
    {

        if ('env' === $this->connectionDataFrom()) {
            return $this->dataConnection(
                envi('mysql.host'),
                envi('mysql.user'),
                envi('mysql.password'),
                envi('mysql.dbname')
            );
        }

        $connection = $this->getDoctrineUtils()
                          ->config['connection']['user'] ?? [];

        return $this->dataConnection(
            $connection['host'] ?? null,
            $connection['name'] ?? null,
            $connection['password'] ?? null,
            $connection['dbname'] ?? null
        );

    }

    /**
     * @return string|null
     */
    public function driver(): ?string
    {

        $utils = $this->getDoctrineUtils();

        return $utils->config['connection']['driver'] ?? self::DRIVER;

    }

    /**
     * @return array
     */
    public function migrationsPaths(): array
    {

        $utils = $this->getDoctrineMigrationsUtils();

        return $utils->config['migrationsPaths'] ?? [];

    }

}