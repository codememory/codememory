<?php

namespace System\Support;

/**
 * Class ConvertType
 * @package System\Support
 *
 * @author  Codememory
 */
class ConvertType
{

    /**
     * @param string|null $data
     *
     * @return float|bool|int|string|null
     */
    public function ofString(?string $data): float|bool|int|string|null
    {

        if (preg_match(
            '/^(?<value>[\-0-9]+)$/',
            $data,
            $match
        )) {
            return (int) $match['value'];
        } elseif (preg_match(
            '/^(?<value>[\-0-9]+[,.][0-9]+)$/',
            $data,
            $match
        )) {
            return (double) $match['value'];
        } elseif (preg_match(
            '/^(true|1|yes|enabled|on)$/i',
            $data
        )) {
            return true;
        } elseif (preg_match(
            '/^(false|0|no|disabled|off)$/i',
            $data
        )) {
            return false;
        } else {
            return $data;
        }

    }

}