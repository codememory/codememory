<?php

namespace System\Support;

/**
 * Class Arr
 * @package System\Support
 *
 * @author  Codememory
 */
class Arr
{

    /**
     * @var array
     */
    private static array $data = [];

    /**
     * @param array $data
     *
     * @return Arr
     */
    public static function set(array $data): Arr
    {

        self::$data = $data;

        return new self();

    }

    /**
     * @param array $than
     * @param array ...$withWhom
     *
     * @return bool
     */
    public static function share(array $than, array &...$withWhom): bool
    {

        foreach ($than as $key => $item) {
            foreach ($withWhom as &$arr) {
                $arr[$key] = $item;
            }
        }

        return true;

    }

    /**
     * @param string $key
     *
     * @return array|mixed
     */
    public static function get(string $key): mixed
    {

        $data = self::$data;
        $keys = explode('.', $key);

        foreach ($keys as $key) {
            if (array_key_exists($key, $data)) {
                $data = $data[$key];
            } else {
                $data = null;
            }
        }

        return $data;

    }

    /**
     * @param array  $data
     * @param string $key
     * @param mixed  $value
     *
     * @return bool
     */
    public static function add(array &$data, string $key, mixed $value): bool
    {

        $data[$key] = $value;

        return true;

    }

    /**
     * @param array  $data
     * @param string $key
     *
     * @return bool
     */
    public static function exists(array $data, string $key): bool
    {

        $keys = explode('.', $key);
        $exists = false;

        foreach ($keys as $key) {
            if (array_key_exists($key, $data)) {
                $data = $data[$key];

                $exists = true;
            } else {
                $exists = false;
            }
        }

        return $exists;

    }

    /**
     * @param array ...$arrays
     *
     * @return array
     */
    public static function intoOne(array ...$arrays): array
    {

        $data = [];

        foreach ($arrays as $arr) {
            foreach ($arr as $key => $item) {
                if (self::exists($data, $key)) {
                    $data[] = $item;
                } else {
                    $data[$key] = $item;
                }
            }
        }

        return $data;

    }

    /**
     * @param array  $data
     * @param string $key
     *
     * @return array|mixed
     */
    public static function pull(array &$data, string $key): mixed
    {

        $keys = explode('.', $key);
        $first = $keys[array_key_last($keys)];
        self::set($data);

        $received = self::get($key);

        unset($data[$first]);

        return $received;

    }

    /**
     * @param array $processedArray
     * @param array $renameKeys
     *
     * @return array
     */
    private static function strictRename(array $processedArray, array $renameKeys = []): array
    {

        $recycledArray = [];

        if ([] !== $renameKeys) {
            foreach ($renameKeys as $oldKey => $newKey) {
                foreach ($processedArray as $key => $item) {
                    if ($key === $oldKey) {
                        $recycledArray[$newKey] = $item;
                    } else {
                        $recycledArray[$key] = $item;
                    }
                }
            }
        } else {
            $recycledArray = $processedArray;
        }

        return $recycledArray;

    }

    /**
     * @param array $schema
     * @param array $processed
     * @param bool  $removeElements
     * @param array $renameKeys
     *
     * @return bool
     */
    public static function strictArray(array $schema, array &$processed, bool $removeElements = false, array $renameKeys = []): bool
    {

        $processedArray = [];

        if ($removeElements) {
            foreach ($processed as $key => $value) {
                if (false === self::exists($schema, $key)) {
                    unset($processed[$key]);
                }
            }
        }

        foreach ($schema as $key => $value) {
            if (self::exists($processed, $key)) {
                $processedArray[$key] = empty($processed[$key]) ? $value : $processed[$key];
            } else {
                $processedArray[$key] = $value;
            }
        }

        if (false === $removeElements) {
            foreach ($processed as $key => $value) {
                if (false === self::exists($processedArray, $key)) {
                    $processedArray[$key] = $value;
                }
            }
        }

        $processed = self::strictRename($processedArray, $renameKeys);

        return true;

    }

    /**
     * @param array $array
     * @param array $comparisonArray
     * @param array $values
     *
     * @return array
     */
    public static function arrayDiffKeyAdding(array $array, array $comparisonArray, array $values = []): array
    {

        foreach ($comparisonArray as $keyComp => $valueComp) {
            if (false === self::exists($array, $keyComp)) {
                $array[$keyComp] = $values[$keyComp] ?? null;
            }
        }

        return $array;

    }

    /**
     * @param array       $data
     * @param string|null $prepend
     * @param string      $separator
     *
     * @return array
     */
    private static function handlerDot(array $data, ?string $prepend = null, string $separator = '.'): array
    {

        $results = [];

        foreach ($data as $key => $item) {
            if (is_array($item) && false === empty($item)) {
                $results = array_merge($results, self::handlerDot($item, $prepend.$key.$separator));
            } else {
                $results[$prepend.$key] = $item;
            }
        }

        return $results;

    }

    /**
     * @param array  $data
     * @param string $separator
     *
     * @return array
     */
    public static function dot(array $data, string $separator = '.'): array
    {

        return self::handlerDot($data, null, $separator);

    }


}