<?php

namespace System\Support\Exceptions\JsonParser;

use ErrorException;
use JetBrains\PhpStorm\Pure;

/**
 * Class JsonErrorException
 * @package System\Http\Response\Headers\Exceptions
 *
 * @author  Codememory
 */
class JsonErrorException extends ErrorException
{

    /**
     * JsonErrorException constructor.
     *
     * @param string $message
     */
    #[Pure] public function __construct(string $message)
    {

        parent::__construct(sprintf('<b>JsonParser: </b> %s', $message));

    }

}