<?php

namespace System\Support\Exceptions\JsonParser;

use ErrorException;
use JetBrains\PhpStorm\Pure;

/**
 * Class ArrayIsMissing
 * @package System\Support\Exceptions\JsonParser
 *
 * @author  Codememory
 */
class InvalidDataType extends ErrorException
{

    /**
     * ArrayIsMissing constructor.
     *
     * @param string $message
     */
    #[Pure] public function __construct(string $message)
    {

        parent::__construct(sprintf('<b>JsonParser: </b> %s', $message));

    }

}