<?php


namespace System\View\Engines;

use System\Components\Big\BigSlab;

/**
 * Class AbstractEngine
 * @package System\View\Engines
 *
 * @author  Codememory
 */
abstract class AbstractEngine implements EngineInterface
{

    /**
     * @var BigSlab
     */
    protected BigSlab $bigSlab;

    /**
     * @var array
     */
    protected array $parameters = [];

    /**
     * @var string|null
     */
    protected ?string $view = null;

    /**
     * BigEngine constructor.
     */
    public function __construct()
    {

        $this->bigSlab = new BigSlab();

    }

    /**
     * {@inheritdoc}
     */
    public function setParameters(array $parameters = []): EngineInterface
    {

        $this->parameters = $parameters;

        return $this;

    }

    /**
     * {@inheritdoc}
     */
    public function setView(string $view): EngineInterface
    {

        $this->view = $view;

        return $this;

    }

}