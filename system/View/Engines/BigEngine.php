<?php

namespace System\View\Engines;

use System\Components\Big\Exceptions\TemplateDoesNotExistException;
use System\FileSystem\Different\Exceptions\IncorrectPathException;

/**
 * Class BigEngine
 * @package System\View\Engines
 *
 * @author  Codememory
 */
class BigEngine extends AbstractEngine
{

    /**
     * {@inheritdoc}
     *
     * @throws TemplateDoesNotExistException
     * @throws IncorrectPathException
     */
    public function get(): mixed
    {

        return $this->bigSlab
            ->openTemplate($this->view)
            ->setParameters($this->parameters)
            ->make();

    }

}