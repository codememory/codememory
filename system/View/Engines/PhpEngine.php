<?php

namespace System\View\Engines;

use Config;
use File;
use System\View\Exceptions\InvalidTemplatePathException;

/**
 * Class PhpEngine
 * @package System\View\Engines
 *
 * @author  Codememory
 */
class PhpEngine extends AbstractEngine
{

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Method returns full path to template with php extension
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return string
     */
    private function getPath(): string
    {

        $viewPath = Config::binds('path.templates');

        return str_replace('.', '/', $viewPath) . '/' . $this->view . '.php';

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Checking the existence of a template, if false, an exception
     * & will be thrown
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool
     * @throws InvalidTemplatePathException
     */
    private function exists(): bool
    {

        if (false === File::exists($this->getPath())) {
            throw new InvalidTemplatePathException($this->getPath());
        }

        return true;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The main method that starts buffering and adds a certain
     * & template to it at the end of the method returns this
     * & template from buffering
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return false|string
     */
    private function evaluate(): bool|string
    {
        ob_start();

        File::oneImport($this->getPath(), $this->parameters);

        return ob_get_clean();

    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     * @throws InvalidTemplatePathException
     */
    public function get(): bool
    {

        if ($this->exists()) {
            response()
                ->setContent($this->evaluate())
                ->sendContent();

            return true;
        }

        return false;

    }

}