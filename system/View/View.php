<?php

namespace System\View;

use File;
use JetBrains\PhpStorm\Pure;
use System\View\Engines\BigEngine;
use System\View\Engines\PhpEngine;
use System\View\Exceptions\InvalidViewNameException;

/**
 * Class View
 * @package System\View
 *
 * @author  Codememory
 */
class View
{

    /**
     * @var array
     */
    private array $parameters = [];

    /**
     * @var string
     */
    private string $engine;

    /**
     * @var array|string[]
     */
    private array $engines = [
        'php' => PhpEngine::class,
        'big' => BigEngine::class
    ];

    /**
     * @var Utils
     */
    private Utils $utils;

    /**
     * @var ViewFactory
     */
    private ViewFactory $factory;

    /**
     * View constructor.
     *
     * @param string $engine
     */
    public function __construct(string $engine, Utils $utils)
    {

        $this->engine = $engine;
        $this->utils = $utils;
        $this->factory = new ViewFactory();

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Checking for the existence of a template file
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $path
     *
     * @return bool
     */
    public function exists(string $path): bool
    {

        return File::exists($this->readablePath($path));

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Passing one parameter (variable) to the template.
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $name
     * @param mixed  $value
     *
     * @return $this
     */
    public function with(string $name, mixed $value): View
    {

        $this->parameters[$name] = $value;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Rendering one template of a specific template engine
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $view
     * @param array  $parameters
     *
     * @return mixed
     */
    public function render(string $view, array $parameters = []): mixed
    {

        $this->parameters = [];

        foreach ($parameters as $name => $value) {
            $this->with($name, $value);
        }

        $this->getFactory()
            ->setEngine($this->engines[$this->engine] ?? null)
            ->setView($this->readablePath($view))
            ->setParameters($this->parameters);

        return $this->factory->make();

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Render multiple templates, and pass the same parameters
     * & to each of them
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array $views
     * @param array $parameters
     *
     * @return string|null
     */
    public function renderMultiple(array $views, array $parameters = []): ?string
    {

        $template = null;

        foreach ($views as $view) {
            $template .= $this->render($view, $parameters);
        }

        return $template;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Checking existence of template name in view.yaml configuration
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $name
     *
     * @return bool
     */
    #[Pure] public function existsViewName(string $name): bool
    {

        return array_key_exists($name, $this->utils->getAllViews());

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The main handler that renders the template by name
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $name
     * @param array  $parameters
     *
     * @return mixed
     * @throws InvalidViewNameException
     */
    private function handlerRenderByName(string $name, array $parameters = []): mixed
    {

        if (false === $this->existsViewName($name)) {
            throw new InvalidViewNameException($name);
        }

        $parameters = array_merge($parameters, $this->utils->getViewParameters($name));

        $this->engine = $this->utils->getViewEngine($name);

        return $this->render($this->utils->getPathView($name), $parameters);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Render a template or multiple templates by name that
     * & exist in the views configuration view.yaml
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|array $name
     * @param array        $parameters
     *
     * @return $this
     * @throws InvalidViewNameException
     */
    public function renderByName(string|array $name, array $parameters = []): View
    {

        if (is_array($name)) {
            foreach ($name as $item) {
                $this->handlerRenderByName($item, $parameters);
            }
        } else {
            $this->handlerRenderByName($name, $parameters);
        }

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returning a factory view
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return ViewFactory
     */
    public function getFactory(): ViewFactory
    {

        return $this->factory;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & A method that converts a path to a specific one
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $path
     *
     * @return string
     */
    private function readablePath(string $path): string
    {

        return str_replace('.', '/', $path);

    }


}