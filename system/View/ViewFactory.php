<?php

namespace System\View;

use System\View\Engines\EngineInterface;

/**
 * Class ViewFactory
 * @package System\View
 *
 * @author  Codememory
 */
class ViewFactory
{

    /**
     * @var string|null
     */
    private ?string $engine;

    /**
     * @var string|null
     */
    private ?string $view = null;

    /**
     * @var array
     */
    private array $parameters;

    /**
     * ViewFactory constructor.
     *
     * @param string|null $engine
     * @param array       $parameters
     */
    public function __construct(?string $engine = null, array $parameters = [])
    {

        $this
            ->setEngine($engine)
            ->setParameters($parameters);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The main method that creates an object of a certain template
     * & engine class and passes the necessary data for rendering the template
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return EngineInterface
     */
    private function provideEngine(): EngineInterface
    {

        $engineNamespace = $this->engine;
        $engine = new $engineNamespace();

        return $engine
            ->setParameters($this->parameters)
            ->setView($this->view);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Install the template engine with which the template will be loaded
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|null $engine
     *
     * @return $this
     */
    public function setEngine(?string $engine): ViewFactory
    {

        $this->engine = $engine;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Set parameters to be passed to the template
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array $parameters
     *
     * @return $this
     */
    public function setParameters(array $parameters): ViewFactory
    {

        $this->parameters = $parameters;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Set the template to be rendered
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $view
     *
     * @return $this
     */
    public function setView(string $view): ViewFactory
    {

        $this->view = $view;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & A method that calls the main get method from the template
     * & engine class and returns the template itself
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return mixed
     */
    public function make(): mixed
    {

        return $this->provideEngine()->get();

    }

}