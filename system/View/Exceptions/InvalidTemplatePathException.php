<?php

namespace System\View\Exceptions;

use JetBrains\PhpStorm\Pure;

/**
 * Class InvalidTemplatePathException
 * @package System\View\Exceptions
 *
 * @author  Codememory
 */
class InvalidTemplatePathException extends ViewException
{

    /**
     * InvalidTemplatePathException constructor.
     *
     * @param string $path
     */
    #[Pure] public function __construct(string $path)
    {

        parent::__construct(sprintf(
            'Pattern in path <b>%s</b> not found',
            $path
        ));

    }

}