<?php

namespace System\View\Exceptions;

use JetBrains\PhpStorm\Pure;

/**
 * Class InvalidViewNameException
 * @package System\View\Exceptions
 *
 * @author  Codememory
 */
class InvalidViewNameException extends ViewException
{

    /**
     * InvalidViewNameException constructor.
     *
     * @param string $viewName
     */
    #[Pure] public function __construct(string $viewName)
    {
        parent::__construct(sprintf(
            'Template in <b>view.yaml</b> configuration named <b>%s</b> was not found',
            $viewName
        ));
    }

}