<?php

namespace System\View\Exceptions;

use ErrorException;

/**
 * Class ViewException
 * @package System\View\Exceptions
 *
 * @author  Codememory
 */
abstract class ViewException extends ErrorException
{

}