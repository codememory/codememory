<?php

namespace System\View;

use Config;

/**
 * Class Utils
 * @package System\View
 *
 * @author  Codememory
 */
class Utils
{

    /**
     * @var array
     */
    private array $config;

    /**
     * Utils constructor.
     */
    public function __construct()
    {

        $this->config = Config::open('configs.cdm')->get('view');

    }

    /**
     * @return array
     */
    public function getAllViews(): array
    {

        return $this->config;

    }

    /**
     * @param string $name
     *
     * @return array|mixed
     */
    public function getDataView(string $name): array
    {

        return $this->config[$name] ?? [];

    }

    /**
     * @param string $name
     *
     * @return mixed|null
     */
    public function getPathView(string $name): mixed
    {

        return $this->getDataView($name)['path'] ?? null;

    }

    /**
     * @param string $name
     *
     * @return array
     */
    public function getViewParameters(string $name): array
    {

        $parameters = $this->getDataView($name)['parameters'] ?? [];

        return false === is_array($parameters) ? [] : $parameters;

    }

    /**
     * @param string $name
     *
     * @return mixed
     */
    public function getViewEngine(string $name): mixed
    {

        $engine = $this->getDataView($name)['engine'] ?? null;

        return null === $engine || empty($engine) ?
            Config::open('configs.binds')->all()['default.templateEngine'] :
            $engine;

    }

}