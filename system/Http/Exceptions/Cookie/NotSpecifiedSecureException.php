<?php

namespace System\Http\Exceptions\Cookie;

use ErrorException;
use JetBrains\PhpStorm\Pure;

/**
 * Class NotSpecifiedSecureException
 * @package System\Support\Exceptions\Cookie
 *
 * @author  Codememory
 */
class NotSpecifiedSecureException extends ErrorException
{

    /**
     * NotSpecifiedSecureException constructor.
     *
     * @param string $sameSite
     */
    #[Pure] public function __construct(string $sameSite)
    {

        parent::__construct(
            sprintf(
                'When choosing SameSite <b>%s</b>, you need to add Secure', $sameSite
            )
        );

    }

}