<?php

namespace System\Http\Exceptions\Cookie;

use ErrorException;
use JetBrains\PhpStorm\Pure;

/**
 * Class InvalidSameSiteException
 * @package System\Support\Exceptions\Cookie
 *
 * @author  Codememory
 */
class InvalidSameSiteException extends ErrorException
{

    /**
     * InvalidSameSiteException constructor.
     *
     * @param string $sameSite
     * @param string $listSameSite
     */
    #[Pure] public function __construct(string $sameSite, string $listSameSite)
    {

        parent::__construct(
            sprintf(
                'SameSite <b>%s</b> is not reserved. Available list: <b>%s</b>',
                $sameSite, $listSameSite
            )
        );

    }

}