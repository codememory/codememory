<?php

namespace System\Http\Exceptions;

use ErrorException;
use JetBrains\PhpStorm\Pure;

/**
 * Class NoSessionHandlerSpecifiedException
 * @package System\Http\Exceptions
 *
 * @author  Codememory
 */
class NoSessionHandlerSpecifiedException extends ErrorException
{

    /**
     * NoSessionHandlerSpecifiedException constructor.
     *
     * @param string $type
     */
    #[Pure] public function __construct(string $type)
    {

        parent::__construct(
            sprintf('Session handler not specified in session configuration <b>config/packages/session.yaml</b> In type <b>%s</b>', $type)
        );

    }

}