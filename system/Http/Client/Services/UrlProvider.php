<?php

namespace System\Http\Client\Services;

use System\Http\Client\Url;

/**
 * Class UrlProvider
 * @package System\Http\Client\Services
 *
 * @author  Codememory
 */
class UrlProvider
{

    /**
     * UrlProvider constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param Url $httpUrl
     *
     * @return Url
     */
    public function executor(Url $httpUrl): Url
    {

        return $httpUrl;

    }

}