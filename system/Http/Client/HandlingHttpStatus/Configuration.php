<?php

namespace System\Http\Client\HandlingHttpStatus;

use Config;
use JetBrains\PhpStorm\Pure;

/**
 * Class Configuration
 * @package System\Http\Client\HandlingHttpStatus
 *
 * @author  Codememory
 */
class Configuration
{

    private const PRIORITY = 'view';

    /**
     * @var array|string[]
     */
    protected array $statusProcessingTypes = [
        'controller', 'view'
    ];

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Getting a complete configuration of setting up response responses
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return array
     */
    public function allConfig(): array
    {

        return Config::open('configs.cdm')->get('response') ?? [];

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returning an array response certain controller
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array $controller
     *
     * @return array
     */
    private function certainController(array $controller): array
    {

        return [
            'namespace' => str_replace('.', '\\', $controller['namespace'] ?? null),
            'method'    => $controller['method'] ?? null
        ];

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returning an array response certain view
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array $view
     *
     * @return array
     */
    private function certainView(array $view): array
    {

        return [
            'path'   => $view['path'] ?? null,
            'engine' => $view['engine'] ?? null
        ];

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns a ready-made array of response certain settings
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return array
     */
    public function certain(): array
    {

        $certain = $this->allConfig()['certain'] ?? [];
        $assembledCertain = [
            'statuses'   => [],
            'priority'   => null,
            'controller' => [],
            'view'       => []
        ];

        if ([] !== $certain) {
            $assembledCertain = [
                'statuses'   => $certain['statuses'] ?? [],
                'priority'   => $certain['priority'] ?? null,
                'controller' => $this->certainController($certain['controller'] ?? []),
                'view'       => $this->certainView($certain['view'] ?? [])
            ];
        }

        return $assembledCertain;


    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Setting the key priority and return the configured key priority
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array $info
     * @param array $certain
     *
     * @return mixed
     */
    #[Pure] private function priority(array $info, array $certain): mixed
    {

        $priority = null;

        if (array_key_exists('priority', $info)) {
            $priority = $info['priority'];
        } else {
            if (array_key_exists('priority', $certain)) {
                $priority = $certain['priority'];
            }
        }

        return (null === $priority || empty($priority)) ? self::PRIORITY : $priority;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Configuring the controller key in all statuses and returning
     * & the configured controller key
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array $info
     * @param array $certain
     *
     * @return array
     */
    private function controller(array $info, array $certain): array
    {

        $controller = [
            'namespace' => $certain['controller']['namespace'] ?? null,
            'method'    => $certain['controller']['method'] ?? null
        ];

        if (array_key_exists('controller', $info)) {
            $info = $info['controller'];

            $controller['namespace'] = $info['namespace'] ?? $controller['namespace'];
            $controller['method'] = $info['method'] ?? $controller['method'];
        }

        return $controller;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & This method performs the same actions as the controller
     * & method only with the view key
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array $info
     * @param array $certain
     *
     * @return array
     */
    private function view(array $info, array $certain): array
    {

        $view = [
            'path'   => $certain['view']['path'] ?? null,
            'engine' => $certain['view']['engine'] ?? null,
        ];

        if (array_key_exists('view', $info)) {
            $info = $info['view'];

            $view['path'] = $info['path'] ?? $view['path'];
            $view['engine'] = $info['engine'] ?? $view['engine'];
        }

        return $view;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Return an array of all configured statuses in the
     * & response configuration
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return array
     */
    public function statuses(): array
    {

        $certain = [];
        $statuses = [];

        foreach ($this->allConfig()['statuses'] ?? [] as $code => $info) {
            if (in_array($code, $this->certain()['statuses'])) {
                $certain = $this->certain();
            }

            $statuses[$code] = [
                'priority'   => $this->priority($info, $certain),
                'controller' => $this->controller($info, $certain),
                'view'       => $this->view($info, $certain)
            ];
        }

        return $statuses;

    }

}