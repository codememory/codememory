<?php

namespace System\Http\Client\HandlingHttpStatus;

use File;
use ReflectionClass;
use System\Http\Client\HandlingHttpStatus\Exceptions\IncorrectTemplatingEngineException;
use System\Http\Client\HandlingHttpStatus\TemplateEngineLoaders\BigTemplateLoader;

/**
 * Trait PrioritiesTrait
 * @package System\Http\Client\HandlingHttpStatus
 *
 * @author  Codememory
 */
trait PrioritiesTrait
{

    private array $templateEngines = [
        'big' => BigTemplateLoader::class
    ];

    /**
     * @param array $info
     *
     * @return mixed
     */
    private function priorityController(array $info): mixed
    {

        return $this->container
            ->setName($info['namespace'])
            ->setObject($info['namespace'])
            ->setMethod($info['method'])
            ->collect()
            ->execute()
            ->get($info['namespace']);

    }

    /**
     * @param array $info
     *
     * @throws IncorrectTemplatingEngineException
     * @throws \ReflectionException
     */
    private function priorityView(array $info): void
    {

        $pathInfo = explode('=', $info['path']);
        $path = str_replace('.', '/', $pathInfo[0] ?? null);
        $expansion = $pathInfo[1] ?? null;

        if (null !== $info['engine'] && false === empty($info['engine'])) {
            if (false === array_key_exists($info['engine'], $this->templateEngines)) {
                throw new IncorrectTemplatingEngineException($info['engine']);
            } else {
                $reflectionLoader = new ReflectionClass(
                    $this->templateEngines[$info['engine']]
                );
                $reflectionLoader->getMethod('call')->invoke($reflectionLoader->newInstance(), $path, $expansion);
            }

            return;
        }

        File::getImport($path . '.' . $expansion);

    }

}