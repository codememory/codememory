<?php

namespace System\Http\Client\HandlingHttpStatus;

use JetBrains\PhpStorm\NoReturn;
use Response;
use System\Components\DI\DIElements\ObjectDependency;
use System\Http\Client\HandlingHttpStatus\Exceptions\IncorrectPriorityException;

/**
 * Class Handling
 * @package System\Http\Client\HandlingHttpStatus
 *
 * @author  Codememory
 */
class Handling extends Configuration
{

    use PrioritiesTrait;

    /**
     * @var int
     */
    private int $currentStatus;

    /**
     * @var ObjectDependency
     */
    private ObjectDependency $container;

    /**
     * @var bool
     */
    private bool $existHandlerStatus = false;

    /**
     * Handling constructor.
     *
     * @param ObjectDependency $container
     */
    public function __construct(ObjectDependency $container)
    {

        $this->currentStatus = Response::getResponseStatus();
        $this->container = $container;

        $this->understandStatus();

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Checks for the existence of http code in the response configuration
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return Handling
     */
    private function understandStatus(): Handling
    {

        if (array_key_exists($this->currentStatus, $this->statuses())) {
            $this->existHandlerStatus = true;
        } else {
            $this->existHandlerStatus = false;
        }

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Check for the existence of a priority (processing type response status)
     * & if the priority value is not allowed in processing, an
     * & IncorrectPriorityException will be thrown
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|null $priority
     *
     * @return Handling
     * @throws IncorrectPriorityException
     */
    private function checkStatusProcessingType(?string $priority): Handling
    {

        if (false === in_array($priority, $this->statusProcessingTypes)) {
            throw new IncorrectPriorityException($priority);
        }

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Calling the desired processing method by priority
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $priority
     *
     * @return mixed
     */
    #[NoReturn] protected function invokeProcessingType(string $priority): mixed
    {

        $methodName = sprintf('priority%s', ucfirst($priority));
        $info = $this->statuses()[$this->currentStatus][$priority];

        return die(call_user_func_array([$this, $methodName], [$info]));

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The main method for handling http status that will be executed if
     * & the http code exists in the response configuration
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return mixed
     * @throws IncorrectPriorityException
     */
    private function handleResponseStatus(): mixed
    {

        if ($this->existHandlerStatus) {
            $statusHandlerInfo = $this->statuses()[$this->currentStatus];
            $priority = $statusHandlerInfo['priority'];

            return $this
                ->checkStatusProcessingType($priority)
                ->invokeProcessingType($priority);

        }

        return null;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method calls the main method for handling http status
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return mixed
     * @throws IncorrectPriorityException
     */
    public function call(): mixed
    {

        return $this->handleResponseStatus();

    }

}