<?php

namespace System\Http\Client\HandlingHttpStatus\Exceptions;

use ErrorException;
use JetBrains\PhpStorm\Pure;

/**
 * Class IncorrectTemplatingEngineException
 * @package System\Http\Client\HandlingHttpStatus\Exceptions
 *
 * @author  Codememory
 */
class IncorrectTemplatingEngineException extends ErrorException
{

    /**
     * IncorrectTemplatingEngineException constructor.
     *
     * @param string $engine
     */
    #[Pure] public function __construct(string $engine)
    {

        parent::__construct(
            sprintf('Incorrect template engine for handling Http code. <b>%s</b> template engine doesn\'t exist', ucfirst($engine))
        );

    }

}