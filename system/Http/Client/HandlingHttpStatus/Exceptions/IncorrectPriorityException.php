<?php

namespace System\Http\Client\HandlingHttpStatus\Exceptions;

use ErrorException;
use JetBrains\PhpStorm\Pure;

/**
 * Class IncorrectPriorityException
 * @package System\Http\Client\HandlingHttpStatus\Exceptions
 *
 * @author  Codememory
 */
class IncorrectPriorityException extends ErrorException
{

    /**
     * IncorrectPriorityException constructor.
     *
     * @param string|null $priority
     */
    #[Pure] public function __construct(?string $priority)
    {
        parent::__construct(
            sprintf(
                'An error occurred in the <b>configs/response.yaml</b> configuration while processing the response status. Incorrect priority (processing type). The processing type <b>%s</b> does not exist',
                $priority ?? 'null'
            )
        );
    }

}