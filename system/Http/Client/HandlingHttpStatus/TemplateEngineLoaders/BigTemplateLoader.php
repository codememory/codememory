<?php

namespace System\Http\Client\HandlingHttpStatus\TemplateEngineLoaders;

use System\Components\Big\BigSlab;
use System\Components\Big\Exceptions\InvalidErrorTypeException;
use System\Components\Big\Exceptions\TemplateDoesNotExistException;
use System\FileSystem\Different\Exceptions\IncorrectPathException;

/**
 * Class BigTemplateLoader
 * @package System\Http\Client\HandlingHttpStatus\TemplateEngineLoaders
 *
 * @author  Codememory
 */
final class BigTemplateLoader implements TemplateEngineLoaderInterface
{

    /**
     * @param string $template
     *
     * @return BigSlab
     * @throws TemplateDoesNotExistException
     */
    private function loader(string $template): BigSlab
    {

        $big = new BigSlab();

        return $big->openTemplate($template);

    }

    /**
     * @param string $path
     *
     * @return TemplateEngineLoaderInterface
     * @throws TemplateDoesNotExistException
     * @throws InvalidErrorTypeException
     * @throws IncorrectPathException
     */
    public function call(string $path, ?string $expansion): TemplateEngineLoaderInterface
    {

        $this->loader($path)->make();

        return $this;

    }

}