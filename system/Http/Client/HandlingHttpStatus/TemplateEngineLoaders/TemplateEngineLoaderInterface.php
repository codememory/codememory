<?php

namespace System\Http\Client\HandlingHttpStatus\TemplateEngineLoaders;

/**
 * Interface TemplateEngineLoaderInterface
 * @package System\Http\Client\HandlingHttpStatus\TemplateEngineLoaders
 *
 * @author  Codememory
 */
interface TemplateEngineLoaderInterface
{

    /**
     * @param string      $path
     * @param string|null $expansion
     *
     * @return TemplateEngineLoaderInterface
     */
    public function call(string $path, ?string $expansion): TemplateEngineLoaderInterface;

}