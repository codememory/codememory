<?php

namespace System\Http\Session\Handlers;

use File;
use Markup;
use System\FileSystem\Config\Exceptions\InvalidCacheConfigTypeException;
use System\FileSystem\Different\Exceptions\InvalidTypeParsingMarkupException;
use System\Http\Session\Session as AppSession;

/**
 * Class FileSystemHandler
 * @package System\Http\Session\Handlers
 *
 * @author  Codememory
 */
class FileSystemHandler
{

    /**
     * @var AppSession
     */
    private AppSession $session;

    /**
     * FileSystemHandler constructor.
     *
     * @param AppSession $session
     *
     * @throws InvalidCacheConfigTypeException
     * @throws InvalidTypeParsingMarkupException
     */
    public function __construct(AppSession $session)
    {

        $this->session = $session;

        $this->makeInit();

    }

    /**
     * @return string
     * @throws InvalidCacheConfigTypeException
     * @throws InvalidTypeParsingMarkupException
     */
    private function getPath(): string
    {

        return $this->session->saveToFile()['path'];

    }

    /**
     * @return FileSystemHandler
     * @throws InvalidCacheConfigTypeException
     * @throws InvalidTypeParsingMarkupException
     */
    private function changePHPSettings(): FileSystemHandler
    {

        Markup::ini()
            ->setIni(
                'session.save_path',
                File::getRealPath($this->session->saveToFile()['path'])
            );

        return $this;

    }

    /**
     * @return FileSystemHandler
     * @throws InvalidCacheConfigTypeException
     * @throws InvalidTypeParsingMarkupException
     */
    private function pathCreation(): FileSystemHandler
    {

        File::mkdir($this->getPath(), 0777, true);

        return $this;

    }

    /**
     * @throws InvalidCacheConfigTypeException
     * @throws InvalidTypeParsingMarkupException
     */
    private function makeInit(): void
    {

        $this->changePHPSettings()
            ->pathCreation();

    }

}