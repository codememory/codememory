<?php

namespace System\Http\Session\Handlers;

use Json;
use Redis;
use RedisManager as RedisFacade;
use SessionHandlerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use System\Databases\Redis\RedisManager;
use System\FileSystem\Config\Exceptions\InvalidCacheConfigTypeException;
use System\FileSystem\Different\Exceptions\InvalidTypeParsingMarkupException;
use System\Http\Session\Session as SessionCM;
use System\Support\Exceptions\JsonParser\JsonErrorException;
use Time;

/**
 * Class Handler
 * @package System\Http\Session
 *
 * @author  Codememory
 */
class RedisHandler implements SessionHandlerInterface
{

    /**
     * @var SessionCM
     */
    private SessionCM $sessionCM;

    /**
     * RedisHandler constructor.
     *
     * @param SessionCM $session
     */
    public function __construct(SessionCM $session)
    {

        $this->sessionCM = $session;

    }

    /**
     * @return bool
     */
    public function close(): bool
    {

        return true;

    }

    /**
     * @param string $id
     *
     * @return bool
     */
    public function destroy(string $id): bool
    {

        return true;

    }

    /**
     * @param int $life
     *
     * @return bool
     */
    public function gc(int $life): bool
    {

        return true;

    }

    /**
     * @param string $path
     * @param string $name
     *
     * @return bool
     */
    public function open(string $path, string $name): bool
    {

        return true;

    }

    /**
     * @return RedisManager|Redis
     * @throws InvalidCacheConfigTypeException
     * @throws InvalidTypeParsingMarkupException
     */
    private function selectDb(): RedisManager|Redis
    {

        return RedisFacade::selectDb(
            $this->sessionCM->saveToRedis()['dbIndex']
        );

    }

    /**
     * @param string $id
     *
     * @return string|null
     * @throws InvalidCacheConfigTypeException
     * @throws InvalidTypeParsingMarkupException
     * @throws JsonErrorException
     */
    public function read(string $id): ?string
    {

        $data = $this->selectDb()->get($this->generateName($id));

        return serialize(
            false !== $data ? Json::setData($data)->decode() : null
        );

    }

    /**
     * @return array
     */
    private function dataToWrite(): array
    {

        return [
            'created' => Time::now()
        ];

    }

    /**
     * @param string $id
     *
     * @return string
     * @throws InvalidCacheConfigTypeException
     * @throws InvalidTypeParsingMarkupException
     */
    private function generateName(string $id): string
    {

        return $this->sessionCM->saveToRedis()['prefixKey'] . $id;

    }

    /**
     * @param string $id
     * @param string $data
     *
     * @return bool
     * @throws InvalidCacheConfigTypeException
     * @throws InvalidTypeParsingMarkupException
     * @throws JsonErrorException
     */
    public function write(string $id, string $data): bool
    {

        $session = new Session();
        $iterator = $session->getIterator()->getArrayCopy();
        $data = unserialize($data);

        if ([] !== $data) {
            foreach ($iterator as $name => $value) {
                $data['_sf2_meta'][$name] = $this->dataToWrite();
            }

            $this->selectDb()->set(
                $this->generateName($id),
                Json::setData($data)->encode(),
                $this->sessionCM->gcMaxLifetime()
            );

        }

        return true;

    }

}