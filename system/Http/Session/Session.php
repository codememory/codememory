<?php

namespace System\Http\Session;

use Config;
use Markup;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBagInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\Session as SymfonySession;
use Symfony\Component\HttpFoundation\Session\Storage\SessionStorageInterface;
use System\FileSystem\Config\Exceptions\InvalidCacheConfigTypeException;
use System\FileSystem\Different\Exceptions\InvalidTypeParsingMarkupException;
use System\Http\Exceptions\NoSessionHandlerSpecifiedException;
use System\Http\Session\Interfaces\SessionInterface;

/**
 * Class Session
 * @package System\Http\Session
 *
 * @author  Codememory
 */
class Session extends SymfonySession implements SessionInterface
{

    private const WHERE_TO_SAVE = 'files';
    private const SESSION_NAME_IN_COOKIE = 'PHPSESSID';
    private const ADD_TO_LOCALSTORAGE = false;
    private const SERIALIZE_HANDLER = 'php';
    private const DB_INDEX_REDIS = 0;
    private const PREFIX_KEY = null;
    private const COOKIE_LIFETIME = 0;
    private const GC_MAX_LIFETIME = 1440;
    private const PATH_SAVE_SESSION = 'storage.session';
    private const INTERFACE_IMPLEMENTATION = true;

    /**
     * @var Config
     */
    private $config;

    /**
     * Session constructor.
     *
     * @param SessionStorageInterface|null $storage
     * @param AttributeBagInterface|null   $attributes
     * @param FlashBagInterface|null       $flashes
     * @param callable|null                $usageReporter
     *
     * @throws InvalidCacheConfigTypeException
     * @throws InvalidTypeParsingMarkupException
     */
    public function __construct(SessionStorageInterface $storage = null, AttributeBagInterface $attributes = null, FlashBagInterface $flashes = null, callable $usageReporter = null)
    {

        parent::__construct($storage, $attributes, $flashes, $usageReporter);

        $this->config = Config::open('configs.packages');
        $this->serializeHandler();

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get handler type from config
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return string
     * @throws InvalidCacheConfigTypeException
     * @throws InvalidTypeParsingMarkupException
     */
    public function whereToSave(): string
    {

        return $this->config->get('session.whereToSave') ?: self::WHERE_TO_SAVE;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get session name in cookies from configuration
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return string
     * @throws InvalidCacheConfigTypeException
     * @throws InvalidTypeParsingMarkupException
     */
    private function sessionNameInCookie(): string
    {

        return $this->config->get('session.sessionNameInCookie') ?: self::SESSION_NAME_IN_COOKIE;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get status: whether to add session identifier to localstorage
     * & to get the identifier in javascript
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool
     * @throws InvalidCacheConfigTypeException
     * @throws InvalidTypeParsingMarkupException
     */
    public function addToLocalStorage(): bool
    {

        return $this->config->get('session.addToLocalStorage') ?: self::ADD_TO_LOCALSTORAGE;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Defines the name of the handler that is used to
     * & serialize/deserialize data.
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return Session
     * @throws InvalidCacheConfigTypeException
     * @throws InvalidTypeParsingMarkupException
     */
    private function serializeHandler(): Session
    {

        $handler = $this->config->get('session.serializeHandler') ?: self::SERIALIZE_HANDLER;

        Markup::ini()->setIni('session.serialize_handler', $handler);

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get session cookie lifetime from configuration
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return int
     * @throws InvalidCacheConfigTypeException
     * @throws InvalidTypeParsingMarkupException
     */
    private function cookieLifetime(): int
    {

        return $this->config->get('session.cookieLifetime') ?: self::COOKIE_LIFETIME;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get the time from the configuration after which all created
     * & sessions will be considered garbage
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return int
     * @throws InvalidCacheConfigTypeException
     * @throws InvalidTypeParsingMarkupException
     */
    public function gcMaxLifetime(): int
    {

        return $this->config->get('session.gcMaxlifetime') ?: self::GC_MAX_LIFETIME;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Information from redis type configuration
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return array
     * @throws InvalidCacheConfigTypeException
     * @throws InvalidTypeParsingMarkupException
     */
    public function saveToRedis(): array
    {

        $prefix = $this->config->get('session.redis.prefixKey');
        $prefix = $prefix ? $prefix . ':' : self::PREFIX_KEY;

        return [
            'dbIndex'   => (int) $this->config->get('session.redis.db') ?: self::DB_INDEX_REDIS,
            'prefixKey' => $prefix
        ];

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & An automated handler that determines which type to load for processing
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return array
     * @throws InvalidCacheConfigTypeException
     * @throws InvalidTypeParsingMarkupException
     * @throws NoSessionHandlerSpecifiedException
     */
    private function handler(): array
    {

        $type = $this->config->get(sprintf('session.%s', $this->whereToSave()));
        $handler = str_replace('.', '\\', $type['handler'] ?: null);

        if (null === $handler) {
            throw new NoSessionHandlerSpecifiedException($type);
        }

        return [
            'implementation' => $type['interfaceImplementation'] ?? self::INTERFACE_IMPLEMENTATION,
            'handler'        => new $handler($this)
        ];

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Information from file type configuration
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return array
     * @throws InvalidCacheConfigTypeException
     * @throws InvalidTypeParsingMarkupException
     */
    public function saveToFile(): array
    {

        $path = $this->config->get('session.files.path') ?: self::PATH_SAVE_SESSION;

        return [
            'path' => trim(str_replace('.', '/', $path), '/') . '/',
        ];

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Create and call the desired session handler before creating the session
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return Session
     * @throws InvalidCacheConfigTypeException
     * @throws InvalidTypeParsingMarkupException
     * @throws NoSessionHandlerSpecifiedException
     */
    private function callHandler(): Session
    {

        $handler = $this->handler();

        if (true === $handler['implementation']) {
            session_set_save_handler($handler['handler']);
        } else {
            $handler['handler'];
        }

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & A simple method that adds a session identifier to localstorage,
     * & this method will be called if the [addToLocalStorage] option in the
     * & session configuration is true
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return $this
     * @throws InvalidCacheConfigTypeException
     * @throws InvalidTypeParsingMarkupException
     */
    public function toLocalStorage(): Session
    {

        if ($this->addToLocalStorage()) {
            $script = <<<'SCRIPT'
            <script type="text/javascript" async defer>
                window.sessionName = "%1$s";
                window.%1$s = "%2$s";
                
                localStorage.setItem(window.sessionName, window[window.sessionName]);
            </script>
            SCRIPT;

            echo sprintf($script, $this->getName(), $this->getId());
        }

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Session initialization, changing php ini settings and starting
     * & the required handler
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return $this
     * @throws InvalidCacheConfigTypeException
     * @throws InvalidTypeParsingMarkupException
     * @throws NoSessionHandlerSpecifiedException
     */
    public function init(): Session
    {

        Markup::ini()
            ->setIni('session.name', $this->sessionNameInCookie())
            ->setIni('session.gc_maxlifetime', $this->gcMaxLifetime())
            ->setIni('session.cookie_lifetime', $this->cookieLifetime());

        $this->callHandler();

        return $this;

    }

}