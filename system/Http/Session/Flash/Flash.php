<?php

namespace System\Http\Session\Flash;

use Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class Flash
 * @package System\Http\Session
 *
 * @author  Codememory
 */
class Flash implements FlashInterface
{

    private const FLASH_NAME = '__cdm_flushes';

    /**
     * @var array
     */
    private array $flushes = [];

    /**
     * @var Session
     */
    private Session $session;

    /**
     * Flash constructor.
     */
    public function __construct()
    {

        $this->session = Request::this()->session;

    }

    /**
     * {@inheritdoc}
     */
    public function add(string $type, string|array $messages): FlashInterface
    {

        $messages = is_array($messages) ? $messages : [$messages];

        if (false === $this->has($type)) {
            $this->set([$type], $messages);
        } else {
            foreach ($messages as $message) {
                $this->flushes[$type][] = $message;
            }

            $this->create();
        }

        return $this;

    }

    /**
     * {@inheritdoc}
     */
    public function set(array $types, array $messages): FlashInterface
    {
        foreach ($types as $index => $type) {
            $this->flushes[$type][] = $messages[$index] ?? null;
        }

        return $this->create();

    }

    /**
     * {@inheritdoc}
     */
    public function has(string $type): bool
    {

        return array_key_exists($type, $this->lists());

    }

    /**
     * {@inheritdoc}
     */
    public function get(string $type, array $default = []): array
    {

        $flash = [];

        if ($this->has($type)) {
            foreach ($this->all()[$type] as $index => $message) {
                if (null === $message) {
                    $message = $default[$index] ?? null;
                }

                $flash[$type][$index] = $message;
            }
        }

        return $flash;

    }

    /**
     * @return array|mixed
     */
    private function lists(): array
    {

        return $this->session->get(self::FLASH_NAME) ?? [];

    }

    /**
     * @return FlashInterface
     */
    private function create(): FlashInterface
    {

        $this->session->set(self::FLASH_NAME, $this->flushes);

        return $this;

    }

    /**
     * {@inheritdoc}
     */
    public function peak(): FlashInterface
    {

        $this->session->remove(self::FLASH_NAME);

        return $this;

    }

    /**
     * {@inheritdoc}
     */
    public function all(): array
    {

        return $this->lists();

    }

}