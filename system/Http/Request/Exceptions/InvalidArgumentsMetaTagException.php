<?php

namespace System\Http\Request\Exceptions;

use ErrorException;

/**
 * Class InvalidArgumentsMetaTagException
 * @package System\Http\Request\Exceptions
 *
 * @author  Codememory
 */
class InvalidArgumentsMetaTagException extends ErrorException
{

    public function __construct()
    {

        parent::__construct('
            To get the meta tag, arguments <b>2 or 3</b> in the <b>getMeta</b> method must be filled
        ');

    }

}