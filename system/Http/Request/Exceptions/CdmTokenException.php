<?php

namespace System\Http\Request\Exceptions;

use ErrorException;

/**
 * Class CdmTokenErrorException
 * @package System\Http\Request\Exceptions
 *
 * @author  Codememory
 */
abstract class CdmTokenException extends ErrorException
{

}