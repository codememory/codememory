<?php

namespace System\FileSystem\Env;

use Symfony\Component\Yaml\Yaml;

/**
 * Class Parser
 * @package System\FileSystem\Env
 *
 * @author  Codememory
 */
class Parser extends Variables
{

    private const CONFIG_PATH = 'configs/app.yaml';
    private const ENV_PATH = '.env';

    /**
     * @var array
     */
    protected array $groups = [];

    /**
     * @var array
     */
    protected array $envi = [];

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns the environment configuration from configs/app.yaml
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return array
     */
    protected function configuration(): array
    {

        $appPath = $this->file->getRealPath(self::CONFIG_PATH);

        return Yaml::parseFile($appPath)['ENV'];

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method picks up the environment string from the .env file
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return string
     */
    protected function env(): string
    {

        return $this->file->read(self::ENV_PATH);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Splits text from .env into lines and returns an array of lines
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return array
     */
    private function onLines(): array
    {

        return explode(PHP_EOL, $this->env());

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Delimiter - delimiters the environment into group_name and value
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $variable
     *
     * @return array
     */
    private function delimiter(string $variable): array
    {

        preg_match('/(?<varName>[^=]+)=(?<value>.*)/', $variable, $match);

        return array_intersect_key($match, array_flip(['varName', 'value']));

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method iterates over an array of groups and returns an array of unique groups
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array $groups
     *
     * @return array
     */
    protected function groups(array $groups): array
    {

        $reassembledGroup = [];
        $lastNameGroup = null;

        foreach ($groups as $group) {
            $lastNameGroup = $group;

            if (!in_array($lastNameGroup, $reassembledGroup)) {
                $reassembledGroup[] = $group;
            }
        }

        return $reassembledGroup;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method will write the finished parsed information about the environments to the property
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array  $variable
     * @param array  $conf
     * @param string $groupName
     */
    private function recordingEnvironments(array $variable, array $conf, string $groupName): void
    {

        $definedValue = $this->setValue($variable['value'])->defineType();

        if (array_key_exists($variable['name'], $conf[$groupName])) {
            $this->envi[$groupName][$variable['name']] =
                null === $definedValue ? $conf[$groupName][$variable['name']]['default'] : $definedValue;
        } else {
            $this->envi[$groupName][$variable['name']] = $definedValue;
        }

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Complete assembly of environments. Value handling and reassembly
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     */
    protected function assembly(): void
    {

        $conf = $this->configuration();
        $parsedEnv = $this->parseEnv();
        $envi = $parsedEnv['envi'];
        $this->groups = $parsedEnv['groups'];

        foreach ($envi as $groupName => $variables) {
            if (array_key_exists($groupName, $conf)) {
                foreach ($variables as $variable) {
                    $this->recordingEnvironments($variable, $conf, $groupName);
                }
            }
        }

        $this->reassembly();

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Rebuilding environments to find variables in environment values
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     */
    private function reassembly(): void
    {

        $envi = [];

        foreach ($this->envi as $group => $variables) {
            foreach ($variables as $variable => $value) {
                $envi[$group][$variable] = $this->setValue($value)->parseVars();
            }
        }

        $this->envi = $envi;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Basic method for parsing .env and returning an array with group and environments
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return array
     */
    private function parseEnv(): array
    {

        $groups = [];
        $envi = [];

        foreach ($this->onLines() as $variable) {

            if (!empty($variable)) {
                $variableInfo = $this->delimiter($variable);
                $name = $variableInfo['varName'];
                $value = $variableInfo['value'];

                [$group, $var] = explode('_', $name);

                $groups[] = $group;
                $envi[$group][] = [
                    'name'  => $var,
                    'value' => $value
                ];
            }

        }

        return [
            'envi'   => $envi,
            'groups' => $this->groups($groups)
        ];

    }

}