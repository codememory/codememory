<?php

namespace System\FileSystem\Env;

/**
 * Class EnvVar
 * @package System\FileSystem\Env
 *
 * @author  Codememory
 */
abstract class Variables
{

    /**
     * @var mixed
     */
    private mixed $value = null;

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Set a value from an environment variable for further processing
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param $value
     *
     * @return $this
     */
    protected function setValue($value): Variables
    {

        $this->value = $value;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Search for variables in environment values. If the value contains a variable from the
     * &  environment, then return its value.
     * --------------------------------------------------------------------------------------------
     * For example:
     * There are 2 environment variables
     * APP_IP = 6000
     * APP_TEST_IP = $ {APP_IP} - (6000)
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array $envi
     *
     * @return mixed
     */
    protected function variable(array $envi): mixed
    {

        preg_match_all('/\${(?<varName>[^}]+)}/', $this->value, $match);
        $value = $this->value;

        if (array_key_exists('varName', $match) && ($match['varName'] !== [])) {
            foreach ($match['varName'] as $var) {
                [$group, $name] = explode('_', $var);

                $value = $envi[$group][$name];
            }
        }

        return $value;

    }

    /**
     * @return mixed
     */
    protected function parseVars(): mixed
    {

        if (!str_starts_with($this->value, '`') && !str_ends_with($this->value, '`')) {
            return $this->variable($this->envi);
        }

        return substr($this->value, 1, -1);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Searching for keywords in values if the keyword matches the expression
     * returns a value of a certain type
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool|string|null
     */
    protected function defineType(): null|bool|string
    {

        return match ($this->value) {
            'false', 'no', 'NO'  => false,
            'true', 'yes', 'YES' => true,
            'null', ''           => null,
            default              => $this->value
        };

    }

}