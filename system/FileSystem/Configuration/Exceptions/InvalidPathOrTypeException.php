<?php

namespace System\FileSystem\Configuration\Exceptions;

use ErrorException;
use JetBrains\PhpStorm\Pure;

/**
 * Class InvalidPathOrTypeException
 * @package System\FileSystem\Configuration\Exceptions
 *
 * @author  Codememory
 */
class InvalidPathOrTypeException extends ErrorException
{

    /**
     * InvalidPathOrTypeException constructor.
     *
     * @param string $message
     */
    #[Pure] public function __construct(string $message)
    {

        parent::__construct($message);

    }

}