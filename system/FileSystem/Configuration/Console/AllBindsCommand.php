<?php

namespace System\FileSystem\Configuration\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Config;

/**
 * Class AllBindsCommand
 * @package System\FileSystem\Configuration\Console
 *
 * @author  Codememory
 */
class AllBindsCommand extends Command
{

    protected function configure()
    {

        $this
            ->setName('app:binds')
            ->setDescription('Получить список всех биндов');

    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $io = new SymfonyStyle($input, $output);
        $binds =  Config::open('configs.binds')->all();
        $readyBinds = [];

        foreach ($binds as $key => $value) {
            $nameKey = sprintf("<fg=blue>Имя ключа</>: <fg=red>%s</>\n\t", $key);
            $value = sprintf("<fg=cyan>Значение</> = <fg=yellow>%s</>\n\n", $value);

            $readyBinds[] = $nameKey.$value;
        }

        foreach ($readyBinds as $index => &$value) {
            if($index === array_key_last($readyBinds)) {
                $value = substr($value, 0, -2);
            }
        }

        $io->listing($readyBinds);

        return Command::FAILURE;

    }

}