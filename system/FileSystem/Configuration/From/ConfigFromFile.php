<?php

namespace System\FileSystem\Configuration\From;

use File;
use Markup;
use System\FileSystem\Configuration\AbstractConfigFrom;
use System\FileSystem\Configuration\Exceptions\InvalidPathOrTypeException;
use System\FileSystem\Different\Exceptions\InvalidTypeParsingMarkupException;

/**
 * Class ConfigFromFile
 * @package System\FileSystem\Configuration\From
 *
 * @author  Codememory
 */
class ConfigFromFile extends AbstractConfigFrom
{

    private const CONFIGURATION_PATH = 'configs/';
    private const FILE_EXPANSION = '.yaml';

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Getting full path to configuration with system configuration folder
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return string
     */
    private function getConfigPath(): string
    {

        return sprintf(
            '%s%s%s',
            self::CONFIGURATION_PATH,
            str_replace('.', '/', $this->path),
            self::FILE_EXPANSION
        );

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Checking the existence of a config in the system configuration folder
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool
     */
    private function configExist(): bool
    {

        return File::exists($this->getConfigPath());

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Save data from the configuration for later retrieval, if the
     * & configuration does not exist, an InvalidPathOrTypeException will be thrown
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return AbstractConfigFrom
     * @throws InvalidPathOrTypeException
     * @throws InvalidTypeParsingMarkupException
     */
    public function call(): AbstractConfigFrom
    {

        if ($this->configExist()) {
            $this->data = Markup::yaml()->open($this->getConfigPath())->get();
        } else {
            throw new InvalidPathOrTypeException(
                sprintf('Config <b>%s</b> in configuration system folder <b>%s</b> not found', $this->path, self::CONFIGURATION_PATH)
            );
        }

        return $this;

    }

}