<?php

namespace System\FileSystem\Configuration\From;

use File;
use JetBrains\PhpStorm\Pure;
use Json;
use Markup;
use System\Components\Caching\Configuration;
use System\FileSystem\Configuration\AbstractConfigFrom;
use System\FileSystem\Configuration\Exceptions\InvalidPathOrTypeException;
use System\FileSystem\Different\Exceptions\InvalidTypeParsingMarkupException;
use System\Support\Exceptions\JsonParser\JsonErrorException;

/**
 * Class ConfigFromCache
 * @package System\FileSystem\Configuration
 *
 * @author  Codememory
 */
class ConfigFromCache extends AbstractConfigFrom
{

    private const FILE_EXPANSION = '.meta';

    /**
     * @var Configuration
     */
    private Configuration $cacheConfig;

    /**
     * ConfigFromCache constructor.
     */
    #[Pure] public function __construct()
    {

        $this->cacheConfig = new Configuration();

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Retrieving the entire cache history
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @throws InvalidTypeParsingMarkupException
     */
    private function history(): array
    {

        $pathToCacheHistory = sprintf(
            '%s%s.%s',
            $this->cacheConfig->pathSaveHistory(),
            $this->cacheConfig->historyFilename(),
            $this->cacheConfig->expansionHistoryFile()
        );

        return Markup::yaml()->open($pathToCacheHistory)->get('history');

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Checking for the existence of a configuration in the cache history
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool
     * @throws InvalidTypeParsingMarkupException
     */
    private function typeExist(): bool
    {

        return array_key_exists($this->path, $this->history());

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Getting the full path to the cache
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return string
     * @throws InvalidTypeParsingMarkupException
     */
    private function getPathWithType(): string
    {

        return sprintf(
            '%s%s',
            $this->cacheConfig->pathCache(),
            str_replace('.', '/', $this->path)
        );

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Checking for the existence of a cache in the system cache storage folder
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool
     * @throws InvalidTypeParsingMarkupException
     */
    private function typeExistToCache(): bool
    {

        return File::is()->isDir($this->getPathWithType());

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Retrieving the latest cache information from history, if the retrieved
     * & configuration does not exist in the cache an exception will be thrown
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return array
     * @throws InvalidPathOrTypeException
     * @throws InvalidTypeParsingMarkupException
     * @throws JsonErrorException
     */
    private function getLastCache(): array
    {

        if ($this->typeExist() && $this->typeExistToCache()) {
            $hash = $this->history()[$this->path]['lastCaching']['hash'];
            $fullPathToHash = sprintf('%s/%s%s', $this->getPathWithType(), $hash, self::FILE_EXPANSION);

            return Json::setData(
                unserialize(File::read($fullPathToHash))
            )->decode();

        }

        throw new InvalidPathOrTypeException(
            sprintf('Config <b>%s</b> was not found in the cache system. Possibly not updated cache', $this->path)
        );

    }

    /**
     * @return AbstractConfigFrom
     * @throws InvalidPathOrTypeException
     * @throws InvalidTypeParsingMarkupException
     * @throws JsonErrorException
     */
    public function call(): AbstractConfigFrom
    {

        $this->data = $this->getLastCache();

        return $this;

    }

}