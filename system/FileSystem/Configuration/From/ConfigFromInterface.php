<?php

namespace System\FileSystem\Configuration\From;

/**
 * Interface ConfigFromInterface
 * @package System\FileSystem\Configuration
 *
 * @author  Codememory
 */
interface ConfigFromInterface
{

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Set the path or to the configuration or the name of the
     * & configuration cache to get their data
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return array
     */
    public function getData(): array;

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns an array from configuration or configuration cache
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $path
     *
     * @return ConfigFromInterface
     */
    public function setPath(string $path): ConfigFromInterface;

}