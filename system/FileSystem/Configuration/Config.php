<?php

namespace System\FileSystem\Configuration;

use System\FileSystem\Configuration\From\ConfigFromCache;
use System\FileSystem\Configuration\From\ConfigFromFile;

/**
 * Class Config
 * @package System\FileSystem\Configuration
 *
 * @author  Codememory
 */
class Config
{

    /**
     * @var bool
     */
    private bool $directlyFromConfig = false;

    /**
     * @var string|null
     */
    private ?string $path = null;

    /**
     * @var array
     */
    private array $data = [];

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Open a specific config
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $config
     *
     * @return Config
     */
    public function open(string $config): Config
    {

        $this->path = $config;

        if (false === $this->directlyFromConfig) {
            return $this->takeFrom(new ConfigFromCache());
        }

        return $this->takeFrom(new ConfigFromFile());

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The main method that determines from where to open the configuration
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param AbstractConfigFrom $configFrom
     *
     * @return Config
     */
    private function takeFrom(AbstractConfigFrom $configFrom): Config
    {

        $this->data = $configFrom
            ->setPath($this->path)
            ->call()
            ->getData();

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Set the status whether to take the configuration from the system
     * & configuration folder
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param bool $take
     *
     * @return $this
     */
    public function directlyFromConfig(bool $take = true): Config
    {

        $this->directlyFromConfig = $take;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Add dynamically data to the configuration and if you need to
     * & add data to a specific element, for this, use the second argument,
     * & in which you need to pass a string of keys separated by a dot
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array       $data
     * @param string|null $toKey
     *
     * @return $this
     */
    public function add(array $data, ?string $toKey = null): Config
    {

        if (null !== $toKey) {
            $keys = explode('.', $toKey);
            $array = &$this->data;

            foreach ($keys as $i => $key) {
                if (count($keys) === 1) {
                    break;
                }
                unset($keys[$i]);

                if (!isset($array[$key]) || !is_array($array[$key])) {
                    $array[$key] = [];
                }

                $array = &$array[$key];
            }

            $array[array_shift($keys)] += $data;
        } else {
            $this->data = array_merge($this->data, $data);
        }

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns an array of all open configuration
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return array
     */
    public function all(): array
    {

        return $this->data;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get specific data for a key from an open configuration
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $keys
     *
     * @return mixed
     */
    public function get(string $keys): mixed
    {

        $data = $this->data;

        if (null !== $data && [] !== $data) {
            foreach (explode('.', $keys) as $key) {
                if (array_key_exists($key, $data)) {
                    $data = $data[$key];
                } else {
                    $data = [];
                }
            }
        }

        return $data === [] ? null : $data;

    }

    /**
     * @param string|null $bind
     *
     * @return mixed
     */
    public function binds(?string $bind = null): mixed
    {

        $binds = $this->open('configs.binds')->all();

        if(null !== $bind) {
            return $binds[$bind] ?? null;
        }

        return $binds;

    }

}