<?php

namespace System\FileSystem\Configuration;

use System\FileSystem\Configuration\From\ConfigFromInterface;

/**
 * Class AbstractConfigFrom
 * @package System\FileSystem\Configuration
 *
 * @author  Codememory
 */
abstract class AbstractConfigFrom implements ConfigFromInterface
{

    /**
     * @var string|null
     */
    protected ?string $path = null;

    /**
     * @var array
     */
    protected array $data = [];

    /**
     * @param string $path
     *
     * @return AbstractConfigFrom
     */
    public function setPath(string $path): AbstractConfigFrom
    {

        $this->path = str_replace('.', '/', $path);

        return $this;

    }

    /**
     * @return array
     */
    public function getData(): array
    {

        return $this->data;

    }

    /**
     * @return AbstractConfigFrom
     */
    abstract public function call(): AbstractConfigFrom;

}