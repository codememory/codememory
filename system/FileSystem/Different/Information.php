<?php

namespace System\FileSystem\Different;

use System\FileSystem\Different\Traits\FileInformationHandlerTrait;
use System\Support\UnitConversion\Units\FromBytes;
use UnitConversion;

/**
 * Class Information
 * @package System\FileSystem\Different
 *
 * @author  Codememory
 */
class Information
{

    use FileInformationHandlerTrait;

    /**
     * @var File
     */
    private File $file;

    /**
     * @var int|float
     */
    private int|float $size = 0;

    /**
     * Information constructor.
     *
     * @param File $file
     */
    public function __construct(File $file)
    {

        $this->file = $file;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns the last divisible part of the element [/]
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string      $path
     * @param string|null $suffix
     *
     * @return string
     */
    public function getBasename(string $path, ?string $suffix = null): string
    {

        return basename($this->file->getRealPath($path), $suffix);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns the file extension
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $path
     *
     * @return string
     */
    public function getExtension(string $path): string
    {

        return pathinfo($this->file->getRealPath($path), PATHINFO_EXTENSION);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns before the last part of the element of the
     * & string that is split into a symbol [/]
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $path
     *
     * @return string
     */
    public function getDirname(string $path): string
    {

        return pathinfo($this->file->getRealPath($path), PATHINFO_DIRNAME);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns the type. Which will define a file or a
     * & directory or something else
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $path
     *
     * @return string
     */
    public function getType(string $path): string
    {

        return filetype($this->file->getRealPath($path));

    }

    /**
     * @param string $path
     * @param bool   $recursion
     *
     * @return int|float
     */
    private function getSizeHandler(string $path, bool $recursion = false): int|float
    {

        if ($this->file->exists($path)) {
            if (false === $recursion) {
                return filesize($this->file->getRealPath($path));
            }
            $this->sizeCalculation($path);

            return $this->size;
        }

        return 0;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Obtaining the file size in different units by default in bytes
     * & To retrieve on another system, use the UnitsInterface
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $path
     * @param string $unit
     * @param bool   $recursion
     *
     * @return int|float
     */
    public function getSize(string $path, string $unit = 'getConvertible', bool $recursion = false): int|float
    {

        $conversion = UnitConversion::setConvertibleNumber(
            $this->getSizeHandler($path, $recursion)
        )->from(new FromBytes());

        $this->size = 0;

        return call_user_func_array([$conversion, $unit], []);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns the time the file was last modified
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string      $path
     * @param string|null $format
     *
     * @return string
     */
    public function lastModified(string $path, ?string $format = 'Y-m-d H:i'): string
    {

        return date($format, filectime($this->file->getRealPath($path)));

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns full information about a file or directory
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param $path
     *
     * @return array
     */
    public function getAllInfo($path): array
    {

        $info = [];

        if (is_string($path)) {
            $info = $this->getArrAllInfo($path);

            if ($this->file->getIs()->isFile($path)) {
                $info['extension'] = $this->getExtension($path);
                $info['size'] = $this->getArrayReadyMeasurementsUnits($path);
            } elseif ($this->file->getIs()->isDir($path)) {
                $info['size'] = $this->getArrayReadyMeasurementsUnits($path, true);
            }
        }

        return $info;

    }

}