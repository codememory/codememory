<?php

namespace System\FileSystem\Different;

/**
 * Class Find
 * @package System\FileSystem\Different
 *
 * @author  Codememory
 */
class Find
{

    /**
     * @var File
     */
    private File $file;

    /**
     * @var array
     */
    private array $find = [];

    /**
     * @var array|string[]
     */
    private array $ignore = [];

    /**
     * Find constructor.
     *
     * @param File $file
     */
    public function __construct(File $file)
    {

        $this->file = $file;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Ignoring paths when searching for any files
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string ...$names
     *
     * @return $this
     */
    public function setIgnore(string ...$names): Find
    {

        $this->ignore = array_map(function ($path) {
            return trim($path, '/');
        }, $names);

        return $this;

    }

    /**
     * @param             $names
     * @param string|null $specificWay
     * @param callable    $callback
     *
     * @return array
     * @throws Exceptions\IncorrectPathException
     */
    private function handlerFind($names, callable $callback, ?string $specificWay = null): array
    {

        $path = $specificWay === null ? '/' : '/' . trim($specificWay, '/');

        if ($this->file->getIs()->isDir($path)) {
            $scanningPath = $this->file->scanning($path);

            foreach ($scanningPath as $k => $item) {
                $generatePath = $path . '/' . $item;

                foreach ($this->ignore as $ignore) {
                    if (preg_match(sprintf('/^%s/', preg_quote($ignore, '/')), trim($generatePath, '/'))) {
                        unset($scanningPath[$k]);
                    }
                }
            }

            foreach ($scanningPath as $k => $item) {
                $generatePath = $path . '/' . $item;

                $this->handlerFindIgnore($item, $generatePath, $names, $path, $callback, $specificWay);
            }
        }

        return $this->find;

    }

    /**
     * @param string      $item
     * @param string      $path
     * @param             $names
     * @param string|null $specificWay
     * @param             $superPath
     * @param             $callback
     */
    private function handlerFindIgnore(string $item, string $path, $names, $superPath, $callback, ?string $specificWay = null): void
    {

        call_user_func_array($callback, [$item, $path, $names, $specificWay, $superPath]);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Find file and file path
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array       $names
     * @param string|null $specificWay
     *
     * @return array
     * @throws Exceptions\IncorrectPathException
     */
    public function find(array $names, ?string $specificWay = null): array
    {

        return $this->handlerFind($names, function ($item, $path, $names) {
            if (in_array($item, $names)) {
                $this->find[] = [
                    'path'     => substr($path, 1),
                    'realpath' => $this->file->getRealPath($path),
                    'filename' => $item
                ];
            } else
                $this->file->getIs()->isDir($path) ? $this->find($names, $path) : false;
        }, $specificWay);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Search files/folders by regular expression
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string      $regex
     * @param string|null $specificWay
     *
     * @return array
     * @throws Exceptions\IncorrectPathException
     */
    public function findByRegex(string $regex, ?string $specificWay = null): array
    {

        return $this->handlerFind($regex, function ($item, $path, $names) {
            if (preg_match(sprintf('/%s/', $names), $item)) {
                $this->find[] = [
                    'path'     => substr($path, 1),
                    'realpath' => $this->file->getRealPath($path),
                    'filename' => $item
                ];
            } else {
                $this->file->getIs()->isDir($path) ? $this->findByRegex($names, $path) : false;
            }
        }, $specificWay);

    }

}