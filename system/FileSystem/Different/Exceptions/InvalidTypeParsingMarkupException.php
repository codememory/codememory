<?php

namespace System\FileSystem\Different\Exceptions;

use ErrorException;
use JetBrains\PhpStorm\Pure;

/**
 * Class IncorrectPathException
 * @package System\FileSystem\Different\Exceptions
 *
 * @author  Codememory
 */
class InvalidTypeParsingMarkupException extends ErrorException
{

    /**
     * InvalidTypeParsingMarkupException constructor.
     */
    #[Pure] public function __construct()
    {

        parent::__construct('In order to parse markup files, you need to set the type setType (AbstractParseFile $type)');

    }

}