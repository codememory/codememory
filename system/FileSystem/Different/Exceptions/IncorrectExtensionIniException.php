<?php

namespace System\FileSystem\Different\Exceptions;

use ErrorException;
use JetBrains\PhpStorm\Pure;

/**
 * Class IncorrectPathException
 * @package System\FileSystem\Different\Exceptions
 *
 * @author  Codememory
 */
class IncorrectExtensionIniException extends ErrorException
{

    /**
     * IncorrectExtensionIniException constructor.
     *
     * @param string $extension
     */
    #[Pure] public function __construct(string $extension)
    {

        parent::__construct(sprintf('<b>%s</b> extension in php ini config does not exist', $extension));

    }

}