<?php

namespace System\FileSystem\Different\Exceptions;

use ErrorException;
use JetBrains\PhpStorm\Pure;

/**
 * Class IncorrectPathException
 * @package System\FileSystem\Different\Exceptions
 *
 * @author  Codememory
 */
class UnsupportedFlagsException extends ErrorException
{

    /**
     * UnsupportedFlagsException constructor.
     *
     * @param string $type
     */
    #[Pure] public function __construct(string $type)
    {

        parent::__construct(sprintf('Flags are not supported when parsing or assembling a <b>%s</b> file.', $type));

    }

}