<?php

namespace System\FileSystem\Different\Exceptions;

use ErrorException;
use JetBrains\PhpStorm\Pure;

/**
 * Class IncorrectPathException
 * @package System\FileSystem\Different\Exceptions
 *
 * @author  Codememory
 */
class IncorrectPathException extends ErrorException
{

    /**
     * IncorrectPathException constructor.
     *
     * @param string $path
     */
    #[Pure] public function __construct(string $path)
    {

        parent::__construct(sprintf('Incorrect path. The file or directory "<b>%s</b>" does not exist.', $path));

    }

}