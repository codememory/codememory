<?php

namespace System\FileSystem\Different\ConfigFiles;

use File;
use System\FileSystem\Different\Exceptions\IncorrectPathException;

/**
 * Class AbstractParseFile
 * @package System\FileSystem\Different\ConfigFiles
 *
 * @author  Codememory
 */
abstract class AbstractParseFile
{

    /**
     * @var false|array|string|null
     */
    protected mixed $data;

    /**
     * @var string|array|null
     */
    protected null|string|array $dataParse = null;

    /**
     * @var bool
     */
    private bool $mode = true;

    /**
     * @param bool $read
     *
     * @return $this
     */
    final public function mode(bool $read): AbstractParseFile
    {

        $this->mode = $read;

        return $this;

    }

    /**
     * @param string $path
     *
     * @return string
     */
    private function autoRemoveExpansion(string $path): string
    {

        $regex = sprintf('/%s$/', preg_quote(static::EXPANSION));

        if (preg_match($regex, $path)) {
            $path = str_replace(static::EXPANSION, '', $path);
        }

        return $path;

    }

    /**
     * @param string $path
     * @param bool   $real
     *
     * @return string
     * @throws IncorrectPathException
     */
    final protected function path(string $path, bool $real = true): string
    {

        $path = rtrim($path, '/');

        $createdPath = sprintf(
            '%s%s',
            str_replace('.', '/', $this->autoRemoveExpansion($path)),
            static::EXPANSION
        );

        $this->exist($createdPath);

        return true === $real ? File::getRealPath($createdPath) : $createdPath;

    }

    /**
     * @param string $path
     *
     * @return bool
     * @throws IncorrectPathException
     */
    private function exist(string $path): bool
    {

        if (true === $this->mode && false === File::exists($path)) {
            throw new IncorrectPathException($path);
        }

        return true;

    }

    /**
     * @return array|false|string|null
     */
    final public function getParsingData(): array|false|string|null
    {

        return $this->dataParse;

    }

}