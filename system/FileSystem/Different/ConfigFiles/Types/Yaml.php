<?php

namespace System\FileSystem\Different\ConfigFiles\Types;

use File;
use Symfony\Component\Yaml\Yaml as SymfonyYaml;
use System\FileSystem\Different\ConfigFiles\AbstractParseFile;
use System\FileSystem\Different\Exceptions\IncorrectPathException;
use System\FileSystem\Different\Interfaces\ParseFileInterface;

/**
 * Class Yaml
 * @package System\FileSystem\Different\ConfigFiles\Types
 *
 * @author  Codememory
 */
class Yaml extends AbstractParseFile implements ParseFileInterface
{

    protected const EXPANSION = '.yaml';
    public const DUMP_OBJECT = 1;
    public const PARSE_EXCEPTION_ON_INVALID_TYPE = 2;
    public const PARSE_OBJECT = 4;
    public const PARSE_OBJECT_FOR_MAP = 8;
    public const DUMP_EXCEPTION_ON_INVALID_TYPE = 16;
    public const PARSE_DATETIME = 32;
    public const DUMP_OBJECT_AS_MAP = 64;
    public const DUMP_MULTI_LINE_LITERAL_BLOCK = 128;
    public const PARSE_CONSTANT = 256;
    public const PARSE_CUSTOM_TAGS = 512;
    public const DUMP_EMPTY_ARRAY_AS_SEQUENCE = 1024;
    public const DUMP_NULL_AS_TILDE = 2048;

    /**
     * @var int
     */
    private int $flags = 0;

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Adding flags when reading or parsing a specific file format
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param int $flags
     *
     * @return ParseFileInterface
     */
    public function setFlags(int $flags): ParseFileInterface
    {

        $this->flags += $flags;

        return $this;

    }

    /**
     * @param string $path
     *
     * @return mixed
     * @throws IncorrectPathException
     */
    public function open(string $path): mixed
    {

        $this->data = SymfonyYaml::parseFile($this->path($path), $this->flags);

        return $this->data;

    }

    /**
     * @param array|object $data
     *
     * @return ParseFileInterface
     */
    public function parse(object|array $data): ParseFileInterface
    {

        $arrayData = [];

        if (is_object($data)) {
            foreach ($data as $key => $value) {
                if (false === is_array($value) && false === is_object($value)) {
                    $arrayData[$key] = $value;
                } else {
                    $this->parse($value);
                }
            }
        } else {
            $arrayData = $data;
        }

        $this->dataParse = SymfonyYaml::dump($arrayData, 5, 2, $this->flags);

        return $this;

    }


    /**
     * @param string $path
     * @param mixed  $data
     *
     * @throws IncorrectPathException
     */
    public function write(string $path, mixed $data): void
    {

        File::editor()->put($this->path($path, false), $data);

    }

}