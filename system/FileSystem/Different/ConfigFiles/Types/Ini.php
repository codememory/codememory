<?php

namespace System\FileSystem\Different\ConfigFiles\Types;

use ConvertType;
use File;
use System\FileSystem\Different\ConfigFiles\AbstractParseFile;
use System\FileSystem\Different\Exceptions\IncorrectExtensionIniException;
use System\FileSystem\Different\Exceptions\IncorrectPathException;
use System\FileSystem\Different\Interfaces\ParseFileInterface;

/**
 * Class Ini
 * @package System\FileSystem\Different
 *
 * @author  Codememory
 */
class Ini extends AbstractParseFile implements ParseFileInterface
{

    protected const EXPANSION = '.ini';

    /**
     * @var int
     */
    private int $flags = INI_SCANNER_TYPED;

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Adding flags when reading or parsing a specific file format
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param int $flags
     *
     * @return ParseFileInterface
     */
    public function setFlags(int $flags): ParseFileInterface
    {

        $this->flags += $flags;

        return $this;

    }

    /**
     * @param string $path
     *
     * @return mixed
     * @throws IncorrectPathException
     */
    public function open(string $path): mixed
    {

        $this->data = parse_ini_string(File::read(
            $this->path($path, false)
        ), true, $this->flags);

        return $this->data;

    }

    /**
     * @param array|object $data
     *
     * @return $this
     */
    public function parse(array|object $data): ParseFileInterface
    {

        foreach ($data as $key => $value) {
            if (is_object($value)) {
                $value = (array) $value;
            }

            if (false === is_array($value)) {
                $string = "%s = %s\n";

                if (true === is_bool($value)) {
                    $value = sprintf("%s", false === $value ? 'false' : 'true');
                } else {
                    $value = is_string($value) ? sprintf("\"%s\"", $value) : $value;
                }

                $this->dataParse .= sprintf($string, $key, preg_quote($value, '/'));
            } else {
                $this->dataParse .= sprintf("[%s]\n", $key);
                $this->parse($value);
            }
        }

        return $this;

    }

    /**
     * @param string $path
     * @param mixed  $data
     *
     * @throws IncorrectPathException
     */
    public function write(string $path, mixed $data): void
    {

        File::editor()->put($this->path($path, false), $data);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Set the value of a configuration setting by passing an array or
     * & string of configuration keys and their new value
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|array          $keys
     * @param string|int|array|null $value
     *
     * @return ParseFileInterface
     */
    public function setIni(string|array $keys, null|string|int|array $value = null): ParseFileInterface
    {

        if (is_array($keys)) {
            foreach ($keys as $index => $key) {
                ini_set($key, is_array($value) ? $value[$index] : $value);
            }
        } else {
            ini_set($keys, $value);
        }

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get configuration value by passing 2 argument means whether to show
     * & detailed information about the configuration key
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|array $keys
     * @param bool         $more
     *
     * @return array
     * @throws IncorrectExtensionIniException
     */
    public function getIni(string|array $keys, bool $more = true): array
    {

        $settings = [];
        $all = $this->getAllIni(more: $more);

        if (is_array($keys)) {
            foreach ($keys as $key) {
                if (array_key_exists($key, $all)) {
                    $settings[$key] = $all[$key];
                }
            }
        } else {
            if (array_key_exists($keys, $all)) {
                $settings[$keys] = $all[$keys];
            }
        }

        return $settings;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get a list of all configuration. By passing 1 as the extension argument,
     * & this is the part of the key name up to a period. Example: allow.name
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|null $extension
     * @param bool        $more
     *
     * @return array
     * @throws IncorrectExtensionIniException
     */
    public function getAllIni(?string $extension = null, bool $more = true): array
    {

        $allSettings = ini_get_all(details: $more);

        if (null !== $extension) {
            $allSettings = array_filter($allSettings, function (mixed $value, mixed $key) use ($extension) {
                $regex = sprintf('/^%s\./', $extension);

                if (preg_match($regex, $key)) {
                    return $value;
                }
            }, ARRAY_FILTER_USE_BOTH);

            if ([] === $allSettings) {
                throw new IncorrectExtensionIniException($extension);
            }
        }

        if ([] !== $allSettings) {
            foreach ($allSettings as $key => $value) {
                if (false === is_array($value)) {
                    $allSettings[$key] = ConvertType::ofString($value);
                }
            }
        }

        return $allSettings;


    }

}