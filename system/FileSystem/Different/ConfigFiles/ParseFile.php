<?php

namespace System\FileSystem\Different\ConfigFiles;

use ReflectionClass;
use System\FileSystem\Different\Exceptions\InvalidTypeParsingMarkupException;
use System\FileSystem\Different\Exceptions\UnsupportedFlagsException;

/**
 * Class ParseFile
 * @package System\FileSystem\Different
 *
 * @author  Codememory
 */
class ParseFile
{

    private const NAME_METHOD_ADD_FLAGS = 'setFlags';

    /**
     * @var AbstractParseFile|null
     */
    private ?AbstractParseFile $type = null;

    /**
     * @var mixed|null
     */
    private mixed $data = null;

    /**
     * @var string|null
     */
    private ?string $path = null;

    /**
     * ParseFile constructor.
     *
     * @param AbstractParseFile|null $type
     */
    public function __construct(?AbstractParseFile $type = null)
    {

        if (null !== $type) {
            $this->setType($type);
        }

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Add an object (file type) to work with. This class must
     * & implement the ParseFileInterface interface and inherit the
     * & abstract class AbstractParseFile
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param AbstractParseFile $type
     *
     * @return $this
     */
    public function setType(AbstractParseFile $type): ParseFile
    {

        $this->type = $type;

        return $this;

    }

    /**
     * @param int $flags
     *
     * @return $this
     * @throws UnsupportedFlagsException
     * @throws \ReflectionException
     */
    public function flags(int $flags): ParseFile
    {

        if (false === method_exists($this->type, self::NAME_METHOD_ADD_FLAGS)) {
            $type = new ReflectionClass($this->type);

            throw new UnsupportedFlagsException($type->getShortName());
        }

        $this->type->setFlags($flags);

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Open the file with which you want to perform some action. The first parameter
     * & is the path to the file without its extension, the second parameter determines
     * & what to open the file for true - for reading, false - for writing and reading
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $path
     * @param bool   $read
     *
     * @return $this
     * @throws InvalidTypeParsingMarkupException
     */
    public function open(string $path, bool $read = true): ParseFile
    {

        if (null === $this->type) {
            throw new InvalidTypeParsingMarkupException();
        }

        $this->path = $path;
        $this->data = $this->type
            ->mode($read)
            ->open($this->path);

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Json to reserved file format
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $data
     *
     * @return $this
     */
    public function ofJson(string $data): ParseFile
    {

        $this->data = $this->type->parse(json_decode($data))->getParsingData();

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Arr to reserved file format
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array $data
     *
     * @return $this
     */
    public function ofArray(array $data): ParseFile
    {

        $this->data = $this->type->parse($data)->getParsingData();

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Object to reserved file format
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param object $data
     *
     * @return $this
     */
    public function ofObject(object $data): ParseFile
    {

        $this->data = $this->type->parse($data)->getParsingData();

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get data from file in json format
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return $this
     */
    public function inJson(): ParseFile
    {

        $this->data = json_encode($this->data);

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get data from file in array format
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return $this
     * @throws InvalidTypeParsingMarkupException
     */
    public function inArray(): ParseFile
    {

        $this->open($this->path);

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get data from file in object format
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return $this
     */
    public function inObject(): ParseFile
    {

        $this->inJson()->data = json_decode($this->data);

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns ready parsed data, etc.
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|null $keys
     *
     * @return mixed
     */
    public function get(?string $keys = null): mixed
    {

        $data = $this->data;

        if (is_array($this->data) && null !== $keys) {
            foreach (explode('.', $keys) as $key) {
                if (array_key_exists($key, $data)) {
                    $data = $data[$key];
                }
            }
        }

        return $data;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Make changes to the file via callback. Callback accepts a link of
     * & the $data parameter, which contains an array of parsed data
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param callable $handle
     *
     * @return bool
     */
    public function change(callable $handle): bool
    {

        $data = $this->get();

        call_user_func_array($handle, [&$data]);
        $this->data = $this->ofArray($data)->get();

        $this->write();

        return true;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Write data to a file, if the file does not exist, the method will
     * & try to create it, but for this, the $read argument must be false
     * & in the call to the open method
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool
     */
    public function write(): bool
    {

        $this->type->write($this->path, $this->get());

        return true;

    }

    /**
     * @param string $method
     * @param array  $arguments
     *
     * @return mixed
     */
    public function __call(string $method, array $arguments): mixed
    {

        return call_user_func_array([$this->type, $method], $arguments);

    }

}