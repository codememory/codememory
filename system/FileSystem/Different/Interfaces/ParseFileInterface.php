<?php

namespace System\FileSystem\Different\Interfaces;

/**
 * Interface ParseFileInterface
 * @package System\FileSystem\Different\Interfaces
 *
 * @author  Codememory
 */
interface ParseFileInterface
{

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Open a file of a specific extension, and return the result of the
     * & read file as an array or object
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $path
     *
     * @return mixed
     */
    public function open(string $path): mixed;

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & A parsing method that transforms data from an array or object into
     * & the desired format yaml, ini, etc.
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array|object $data
     *
     * @return ParseFileInterface
     */
    public function parse(array|object $data): ParseFileInterface;

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Write data to a file in the form of a certain format, for
     * & example, in yaml, ini, etc.
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $path
     * @param mixed  $data
     */
    public function write(string $path, mixed $data): void;

}