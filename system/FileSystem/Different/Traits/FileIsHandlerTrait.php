<?php

namespace System\FileSystem\Different\Traits;

/**
 * Trait FileIsHandlerTrait
 * @package System\FileSystem\Different\Traits
 *
 * @author  Codememory
 */
trait FileIsHandlerTrait
{

    /**
     * @param string|array $path
     * @param callable     $callback
     *
     * @return mixed
     */
    private function handlerIs(string|array $path, callable $callback): mixed
    {

        $is = false;

        if (is_string($path)) {
            $is = call_user_func($callback, $this->file->getRealPath($path));
        } else if (is_array($path)) {
            foreach ($path as $name) {
                $is = $this->handlerIs($name, $callback);
            }
        }

        return $is;

    }

}