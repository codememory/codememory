<?php

namespace System\FileSystem\Different\Traits;

use File;
use UnitConversion;

/**
 * Trait FileInformationHandlerTrait
 * @package System\FileSystem\Different\Traits
 *
 * @author  Codememory
 */
trait FileInformationHandlerTrait
{

    /**
     * @param string $dir
     */
    private function sizeCalculation(string $dir): void
    {

        $dir = rtrim($dir, '/') . '/';
        $attached = $this->file->scanning($dir);
        if ([] !== $attached) {
            foreach ($attached as $path) {
                $path = $dir . $path;

                if (File::is()->isFile($path)) {
                    $this->size += $this->getSizeHandler($path);
                } else {
                    $this->size += $this->getSizeHandler($path);
                    $this->sizeCalculation($path);
                }
            }
        }

    }

    /**
     * @param $path
     *
     * @return array
     */
    private function getArrAllInfo($path): array
    {

        $realPath = $this->file->getRealPath($path);

        return [
            'type'         => $this->getType($path),
            'lastModified' => $this->lastModified($path),
            'path'         => $this->getDirname($path),
            'group'        => posix_getpwuid(filegroup($realPath))['name'],
            'owner'        => posix_getpwuid(fileowner($realPath))['name'],
            'permissions'  => substr(sprintf('%o', fileperms($realPath)), -4)
        ];

    }

    /**
     * @param      $path
     * @param bool $recursion
     *
     * @return array
     */
    private function getArrayReadyMeasurementsUnits($path, bool $recursion = false): array
    {

        return [
            'b'  => $this->getSize($path, UnitConversion::CURRENT, $recursion),
            'kb' => $this->getSize($path, UnitConversion::UNIT_KB, $recursion),
            'mb' => $this->getSize($path, UnitConversion::UNIT_MB, $recursion),
            'gb' => $this->getSize($path, UnitConversion::UNIT_GB, $recursion),
        ];

    }

}