<?php

namespace System\FileSystem\Different;

use JetBrains\PhpStorm\Pure;
use System\FileSystem\Different\Exceptions\IncorrectPathException;

/**
 * Class File
 * @package System\FileSystem\Different
 *
 * @author  Codememory
 */
class File
{

    /**
     * @var Is
     */
    private Is $is;

    /**
     * @var Editor
     */
    private Editor $editor;

    /**
     * File constructor.
     */
    #[Pure] public function __construct()
    {

        $this->is = new Is($this);
        $this->editor = new Editor($this);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns the full path to the root and with the
     * & option to add the path
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|null $join
     *
     * @return string
     */
    public function getRealPath(?string $join = null): string
    {

        if (str_starts_with($join, '*')) {
            return substr(trim($join, '/'), 1);
        } else {
            return sprintf('%s/%s', ROOT, trim($join, '/'));
        }

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns boolean checks whether such file or directory
     * & exists in the specified path
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param $path
     *
     * @return bool
     */
    public function exists($path): bool
    {

        $isExists = false;

        if (is_string($path)) {
            $isExists = file_exists($this->getRealPath($path));
        } elseif (is_array($path)) {
            foreach ($path as $value) {
                $isExists = $this->exists($value);
            }
        }

        return $isExists;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Handler ignore files|directories when scanning a path
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array $data
     * @param array $ignore
     *
     * @return array
     */
    private function ignoring(array $data, array $ignore): array
    {

        foreach ($data as $keyArr => $item) {
            if (is_array($item)) {
                foreach ($item as $key => $value) {
                    if (in_array($value, $ignore)) unset($data[$keyArr][$key]);
                }
            } else unset($data[$keyArr]);
        }

        return $data;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns an array with the result of scanning paths.
     * & You can specify an array of paths to scan, and also ignore
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param            $path
     * @param array|null $ignoring
     *
     * @return array
     * @throws IncorrectPathException
     */
    public function scanning($path, ?array $ignoring = []): array
    {

        $scanResult = [];

        if ($this->exists($path) === false && (is_string($path))) {
            throw new IncorrectPathException($path);
        }

        if (is_string($path)) {
            $scan = scandir($this->getRealPath($path));
            $scanResult = array_diff($scan, ['.', '..']);
        } elseif (is_array($path)) {
            foreach ($path as $value) {
                $scanResult[$value] = $this->scanning($value);
            }
        }

        if ($ignoring !== []) {
            $scanResult = $this->ignoring($scanResult, $ignoring);
        }

        return $scanResult;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method creates a folder or creates folders recursively i.e. sub folders
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param          $dirname
     * @param int|null $permission
     * @param bool     $recursion
     *
     * @throws IncorrectPathException
     */
    public function mkdir($dirname, ?int $permission = 0777, bool $recursion = false): void
    {

        if (is_string($dirname)) {
            if (!$this->is->isDir($dirname)) {
                mkdir($this->getRealPath($dirname), $permission, $recursion);
                $this->setPermission($dirname, $permission);
            }
        } elseif (is_array($dirname)) {
            foreach ($dirname as $name) {
                $this->mkdir($name, $permission, $recursion);
            }
        }

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Rights or owner update handler
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $path
     * @param        $data
     * @param string $function
     *
     * @return bool
     * @throws IncorrectPathException
     */
    private function updatePermissionsOrOwnerRecursively(string $path, $data, string $function): bool
    {

        $this->setPermissionOrOwner($path, $data, $function);
        $scanningPath = $this->scanning($path);

        if ($scanningPath !== []) {
            foreach ($scanningPath as $pathname) {
                $pathName = rtrim($path, '/') . '/' . $pathname;

                if ($this->getIs()->isFile($pathName)) {
                    $this->setPermissionOrOwner($pathName, $data, $function);
                } else {
                    $this->updatePermissionsOrOwnerRecursively($pathName, $data, $function);
                }
            }
        }

        return true;

    }

    /**
     * @param string $path
     * @param        $data
     * @param bool   $recursion
     * @param string $function
     *
     * @return bool
     * @throws IncorrectPathException
     */
    private function setPermissionOrOwner(string $path, $data, string $function, bool $recursion = false): bool
    {

        if ($this->exists($path) === false) throw new IncorrectPathException($path);

        if ($recursion === false || ($this->getIs()->isFile($path)))
            return $function($this->getRealPath($path), $data);
        else {
            $this->setPermissionOrOwner($path, $data, $function);

            return $this->updatePermissionsOrOwnerRecursively($path, $data, $function);
        }

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Set | Change file or directory permissions and specifying 3 will recurse
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $path
     * @param int    $permission
     * @param bool   $recursion
     *
     * @return bool
     * @throws IncorrectPathException
     */
    public function setPermission(string $path, int $permission = 0777, bool $recursion = false): bool
    {

        return $this->setPermissionOrOwner($path, $permission, 'chmod', $recursion);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Set | Change owner of file or folder 3 parameter recursive change
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $path
     * @param string $ownerName
     * @param bool   $recursion
     *
     * @return bool
     * @throws IncorrectPathException
     */
    public function setOwner(string $path, string $ownerName, bool $recursion = false): bool
    {

        return $this->setPermissionOrOwner($path, $ownerName, 'chown', $recursion);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Method renames a folder or file
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $path
     * @param string $newName
     *
     * @return bool
     */
    public function rename(string $path, string $newName): bool
    {

        if ($this->exists($path)) {
            $cutPath = explode('/', $path);
            unset($cutPath[array_key_last($cutPath)]);

            $generateName = $this->getRealPath(implode('/', $cutPath) . '/' . $newName);

            rename($this->getRealPath($path), $generateName);
        }

        return false;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Deleting a file or folder 2 argument recursive deletion, i.e. if there are
     * & sub folders and files in the folder and you need to delete the entire folder then 2
     * & argument must be true
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param      $path
     * @param bool $recursion
     * @param bool $removeCurrentDir
     *
     * @return bool
     * @throws IncorrectPathException
     */
    public function remove($path, bool $recursion = false, bool $removeCurrentDir = false): bool
    {

        $isRemove = false;

        if ($recursion === false) {
            if (is_string($path)) {
                $isRemove = $this->getIs()->isFile($path) ? unlink($this->getRealPath($path)) : rmdir($this->getRealPath($path));

            } elseif (is_array($path)) {
                foreach ($path as $name) {
                    $isRemove = $this->remove($name);
                }
            }
        } else {
            $isRemove = $this->removeAll($path);
        }

        if ($isRemove && $removeCurrentDir) {
            $this->remove($path);
        }

        return $isRemove;

    }

    /**
     * @param $path
     *
     * @return bool
     * @throws IncorrectPathException
     */
    private function removeAll($path): bool
    {

        $isRemove = false;
        $items = $this->scanning($path);

        if ($items !== []) {
            foreach ($items as $item) {
                $pathName = rtrim($path, '/') . '/' . $item;

                if ($this->getIs()->isDir($pathName)) {
                    $isRemove = $this->removeAll($pathName);
                } else {
                    $isRemove = $this->remove($pathName);
                }
            }
        } else {
            $isRemove = $this->remove($path);
        }

        return $isRemove;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Method moves a file or folder to a new location
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $path
     * @param string $moveTo
     *
     * @return bool
     */
    public function move(string $path, string $moveTo): bool
    {

        if ($this->exists($path)) {
            $cutPath = explode('/', $path);
            $movePath = $cutPath[array_key_last($cutPath)];

            rename($this->getRealPath($path), $this->getRealPath($moveTo . '/' . $movePath));
        }

        return false;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method adds or changes a group of a file or folder
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $path
     * @param        $group
     *
     * @return bool
     */
    public function setGroup(string $path, $group): bool
    {

        if ($this->exists($path)) {
            return chgrp($this->getRealPath($path), $group);
        }

        return false;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method reads the file and returns the result
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $path
     *
     * @return string|null
     */
    public function read(string $path): ?string
    {

        if ($this->getIs()->isRead($path)) {
            return file_get_contents($this->getRealPath($path));
        }

        return null;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns data from a file that is returned
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param       $path
     * @param array $parameters
     *
     * @return mixed
     */
    public function getImport($path, array $parameters = []): mixed
    {

        $data = null;

        if (is_string($path)) {
            $data = require $this->getRealPath($path);
        } elseif (is_array($path)) {
            foreach ($path as $name) {
                $data[$name] = $this->getImport($name);
            }
        }

        extract($parameters);

        return $data;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method connects the file only once, no more
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $path
     * @param array  $parameters
     */
    public function oneImport(string $path, array $parameters = []): void
    {

        if ($this->exists($path)) {
            extract($parameters);

            require_once $this->getRealPath($path);
        }

    }

    /**
     * @return Is
     */
    public function getIs(): Is
    {

        return $this->is;

    }

    /**
     * @return Editor
     */
    public function getEditor(): Editor
    {

        return $this->editor;

    }


}