<?php

namespace System\FileSystem\Different\Services;

use System\FileSystem\Different\Editor;

/**
 * @method bool put(string $path, mixed $contents, int $permission = 0777, int $length = null)
 * @method Editor append(string $path, mixed $data)
 * @method Editor prepend(string $path, mixed $data)
 * @method bool appendToLine(string $path, mixed $data, int $line)
 *
 * Class FileEditorProvider
 * @package System\FileSystem\Different\Services
 *
 * @author  Codememory
 */
class FileEditorProvider
{

    public function __construct()
    {
    }

    /**
     * @param Editor $editor
     *
     * @return Editor
     */
    public function executor(Editor $editor): Editor
    {

        return $editor;

    }
}