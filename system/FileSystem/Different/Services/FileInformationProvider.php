<?php

namespace System\FileSystem\Different\Services;

use System\FileSystem\Different\Information;

/**
 * @method bool isFile(string|array $filename)
 * @method bool isDir(string|array $dirname)
 * @method bool isWrite(string|array $filename)
 * @method bool isRead(string|array $filename)
 * @method bool isLink(string|array $filename)
 *
 * Class FileInformationProvider
 * @package System\FileSystem\Different\Services
 *
 * @author  Codememory
 */
class FileInformationProvider
{

    public function __construct()
    {
    }

    /**
     * @param Information $file
     *
     * @return Information
     */
    public function executor(Information $file): Information
    {

        return $file;

    }

}