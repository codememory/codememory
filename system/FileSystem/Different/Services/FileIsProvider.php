<?php

namespace System\FileSystem\Different\Services;

use System\FileSystem\Different\Is;

/**
 * @method bool isFile(string|array $filename)
 * @method bool isDir(string|array $dirname)
 * @method bool isWrite(string|array $filename)
 * @method bool isRead(string|array $filename)
 * @method bool isLink(string|array $filename)
 *
 * Class FileIsProvider
 * @package System\FileSystem\Different\Services
 *
 * @author  Codememory
 */
class FileIsProvider
{

    public function __construct()
    {
    }

    /**
     * @param Is $fileIs
     *
     * @return Is
     */
    public function executor(Is $fileIs): Is
    {

        return $fileIs;

    }

}