<?php

namespace System\FileSystem\Different\Services;

use JetBrains\PhpStorm\Pure;
use System\FileSystem\Different\Editor as FileEditor;
use System\FileSystem\Different\File;
use System\FileSystem\Different\Find;
use System\FileSystem\Different\Information as FileInformation;
use System\FileSystem\Different\Is as FileIs;

/**
 * @method bool exists(string $path)
 * @method File setRealPath(string $path)
 * @method string getRealPath(?string $join = null)
 * @method array scanning($path, ?array $ignoring = [])
 * @method void mkdir($dirname, ?int $permission = 0777, bool $recursion = false)
 * @method bool setPermission(string $path, int $permission = 0777, bool $recursion = false)
 * @method bool setOwner(string $path, string $ownerName, bool $recursion = false)
 * @method bool rename(string $path, string $newName)
 * @method bool remove($path, bool $recursion = false, bool $removeCurrentDir = false)
 * @method bool move(string $path, string $moveTo)
 * @method bool setGroup(string $path, $group)
 * @method string read(string $path)
 * @method mixed getImport($path)
 * @method void oneImport(string $path)
 *
 * Class FileProvider
 * @package System\FileSystem\Different\Services
 *
 * @author  Codememory
 */
class FileProvider
{

    public function __construct()
    {
    }

    /**
     * @param File $file
     *
     * @return FileIs
     */
    #[Pure] public function is(File $file): FileIs
    {

        return $file->getIs();

    }

    /**
     * @param File $file
     *
     * @return FileEditor
     */
    #[Pure] public function editor(File $file): FileEditor
    {

        return $file->getEditor();

    }

    /**
     * @param FileInformation $fileInformation
     *
     * @return FileInformation
     */
    public function info(FileInformation $fileInformation): FileInformation
    {

        return $fileInformation;

    }

    /**
     * @param Find $findFile
     *
     * @return Find
     */
    public function find(Find $findFile): Find
    {

        return $findFile;

    }

    /**
     * @param File $file
     *
     * @return File
     */
    public function executor(File $file): File
    {

        return $file;

    }

}