<?php

namespace System\Components\Caching\Commands;

use Config;
use File;
use Markup;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use System\Components\Caching\Caching;
use System\FileSystem\Different\Exceptions\IncorrectPathException;
use System\FileSystem\Different\Exceptions\InvalidTypeParsingMarkupException;

/**
 * Class ConfigCommand
 * @package System\Components\Caching\Commands
 *
 * @author  Codememory
 */
class ConfigCommand extends Command
{

    private const PACKAGE_PATH = 'configs/packages/';
    private const CONFIG_PATH = 'configs/';
    private const NAME_CONFIG = '/^[a-z0-9]+\.yaml$/i';

    /**
     * @var array
     */
    private array $packageData = [];

    /**
     * @var array
     */
    private array $configData = [];

    /**
     * @var array
     */
    private array $scanningFile = [];

    /**
     * @var array
     */
    private array $binds = [];

    /**
     * @var InputInterface|null
     */
    private ?InputInterface $input = null;

    protected function configure(): void
    {

        $this->setName('cache:config')
            ->addOption('all', null, InputOption::VALUE_NONE, 'Обновить весь кэш конфигурации')
            ->addOption('package', null, InputOption::VALUE_NONE, 'Обновить кэш конфигурации пакетов')
            ->addOption('no-binds', null, InputOption::VALUE_NONE, 'Не обновлять бинды конфигураций')
            ->setDescription('Обновление кэша конфигурации');

    }

    /**
     * @param Caching $cache
     *
     * @return ConfigCommand
     * @throws IncorrectPathException|InvalidTypeParsingMarkupException
     */
    private function all(Caching $cache): ConfigCommand
    {

        $this->config(self::CONFIG_PATH)
            ->package(self::PACKAGE_PATH)
            ->createConfigCache($cache)
            ->createPackageConfigCache($cache);

        return $this;

    }

    /**
     * @param string $path
     *
     * @return ConfigCommand
     * @throws InvalidTypeParsingMarkupException
     */
    private function package(string $path): ConfigCommand
    {

        if (File::is()->isFile($path)) {
            $splitPath = explode('/', substr($path, 0, -1));

            if (preg_match(self::NAME_CONFIG, $splitPath[array_key_last($splitPath)])) {
                $this->packageData = array_merge($this->packageData, Markup::yaml()->open($path)->get());

                $this->scanningFile[] = substr($path, 0, -1);
            }
        } else {
            $scan = File::scanning($path);

            if ([] !== $scan) {
                foreach ($scan as $dirOrFile) {
                    $this->package($path . $dirOrFile . '/');
                }
            }
        }

        return $this;

    }

    /**
     * @param string $path
     *
     * @return ConfigCommand
     * @throws InvalidTypeParsingMarkupException
     */
    private function config(string $path): ConfigCommand
    {

        $scan = File::scanning($path);

        if ([] !== $scan) {
            foreach ($scan as $filename) {
                if (File::is()->isFile($path . $filename)) {
                    if (preg_match(self::NAME_CONFIG, $filename)) {
                        $this->configData = array_merge($this->configData, Markup::yaml()->open($path . $filename)->get());

                        $this->scanningFile[] = $path . $filename;
                    }
                }
            }
        }

        return $this;

    }

    /**
     * @param Caching $cache
     * @param array   $data
     *
     * @return ConfigCommand
     * @throws IncorrectPathException
     * @throws InvalidTypeParsingMarkupException
     */
    private function binds(Caching $cache, array $data): ConfigCommand
    {

        $binds = [];

        foreach ($data as $nameConfig => $value) {
            if (is_array($value)) {
                if (array_key_exists('binds', $value)) {
                    $binds = array_merge($binds, $value['binds']);
                }
            }
        }

        $this->binds = array_merge($this->binds, $binds);

        if (false === $this->input->getOption('no-binds')) {
            $cache
                ->type('configs/binds')
                ->content(json_encode($this->binds))
                ->createOnlyMeta()
                ->make();
        }

        return $this;

    }

    /**
     * @return array
     */
    private function allBinds(): array
    {

        return Config::open('configs.binds')->all();

    }

    /**
     * @param mixed $data
     */
    private function replaceBinds(mixed &$data)
    {

        foreach ($data as $config => &$value) {
            if (is_array($value)) {
                $this->replaceBinds($value);
            } else {
                $value = preg_replace_callback('/%(?<bind>[^%]+)%/', fn (array $match) => $this->allBinds()[$match['bind']], $value);
            }
        }

    }

    /**
     * @param Caching $cache
     * @param string  $type
     * @param string  $perkType
     * @param array   $data
     *
     * @return ConfigCommand
     * @throws IncorrectPathException|InvalidTypeParsingMarkupException
     */
    private function handlerCreateCache(Caching $cache, string $type, string $perkType, array $data): ConfigCommand
    {

        $this->binds($cache, $data)->replaceBinds($data);

        $cache
            ->type(sprintf('%s/%s', $type, $perkType))
            ->content(json_encode($data))
            ->createOnlyMeta()
            ->make();

        return $this;

    }

    /**
     * @param Caching $cache
     *
     * @return ConfigCommand
     * @throws IncorrectPathException|InvalidTypeParsingMarkupException
     */
    private function createConfigCache(Caching $cache): ConfigCommand
    {

        $this->handlerCreateCache($cache, 'configs', 'cdm', $this->configData);

        return $this;

    }

    /**
     * @param Caching $cache
     *
     * @return ConfigCommand
     * @throws IncorrectPathException|InvalidTypeParsingMarkupException
     */
    private function createPackageConfigCache(Caching $cache): ConfigCommand
    {

        $this->handlerCreateCache($cache, 'configs', 'packages', $this->packageData);

        return $this;

    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws IncorrectPathException|InvalidTypeParsingMarkupException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $this->input = $input;
        $io = new SymfonyStyle($input, $output);
        $cache = new Caching();

        if ($input->getOption('package')) {
            $this
                ->package(self::PACKAGE_PATH)
                ->createPackageConfigCache($cache);

        } elseif ($input->getOption('all')) {
            $this
                ->all($cache);
        } else {

            $this
                ->config(self::CONFIG_PATH)
                ->createConfigCache($cache);
        }

        $io
            ->success(
                [
                    sprintf('Кэш обновлен для %s файлов', count($this->scanningFile)),
                    sprintf(
                        'Файлы для которых обновлен кэш: %s%s',
                        PHP_EOL,
                        implode(PHP_EOL, $this->scanningFile)
                    )
                ]
            );

        return Command::FAILURE;

    }

}