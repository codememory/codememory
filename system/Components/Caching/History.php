<?php

namespace System\Components\Caching;

use Date;
use File;
use JetBrains\PhpStorm\Pure;
use Markup;
use System\FileSystem\Different\Exceptions\IncorrectPathException;
use System\FileSystem\Different\Exceptions\InvalidTypeParsingMarkupException;

/**
 * Class History
 * @package System\Components\Caching
 *
 * @author  Codememory
 */
class History
{

    /**
     * @var string|null
     */
    private ?string $type = null;

    /**
     * History constructor.
     *
     * @param string $path
     * @param string $filename
     * @param string $expansion
     * @param string $formatDate
     * @param string $fileCacheName
     * @param string $normalPathCacheFile
     */
    public function __construct(
        private string $path,
        private string $filename,
        private string $expansion,
        private string $formatDate,
        private string $fileCacheName,
        private string $normalPathCacheFile
    )
    {
    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get the path to the cache file with the name of the cache file
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return string
     */
    #[Pure] private function getPath(): string
    {

        return sprintf('%s%s.%s', $this->path, $this->filename, $this->expansion);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Set the type (cache name) for which the history will be created
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $type
     *
     * @return $this
     */
    public function setType(string $type): History
    {

        $this->type = $type;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Method creates history path if it doesn't exist
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return History
     * @throws IncorrectPathException
     */
    public function createHistoryFile(): History
    {

        if (!File::is()->isDir($this->path) || !File::exists($this->getPath())) {
            File::mkdir($this->path, 0777, true);

            File::editor()->put($this->getPath(), null);
        }

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method clears the entire cache history
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return History
     */
    public function clearHistory(): History
    {

        if (File::is()->isDir($this->path)) {
            File::remove($this->path, true, true);
        }

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & n array of information that should be recorded in the history of the
     * last change in the cache by type
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $date
     * @param string $hash
     * @param int    $size
     *
     * @return array
     */
    private function lastData(string $date, string $hash, int $size): array
    {

        return [
            'date' => $date,
            'hash' => $hash,
            'size' => sprintf('%s byte cache', $size)
        ];

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Arr of last added cache by type
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $date
     * @param string $hash
     * @param int    $size
     *
     * @return \array[][]
     */
    private function newTypeOfHistory(string $date, string $hash, int $size): array
    {

        return [
            $this->type => [
                'lastCaching' => $this->lastData($date, $hash, $size)
            ]
        ];

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get history array if history file is just created
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $date
     * @param string $hash
     * @param int    $size
     *
     * @return \array[][][]
     */
    private function emptyHistory(string $date, string $hash, int $size): array
    {

        return [
            'history' => $this->newTypeOfHistory($date, $hash, $size)
        ];

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get array of last cached data by type
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $type
     *
     * @return array
     * @throws InvalidTypeParsingMarkupException
     */
    public function getLastCacheInfo(string $type): array
    {

        $data = [];

        if (File::exists($this->getPath())) {
            $history = Markup::yaml()->open($this->getPath())->get();

            if (array_key_exists($type, $history['history'] ?? [])) {
                $data = $history['history'][$type]['lastCaching'];
            }
        }

        return $data;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get the size of the added cache in bytes
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return int
     * @throws IncorrectPathException
     */
    private function cacheSize(): int
    {

        return File::info()->getSize($this->normalPathCacheFile);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The main method that modifies the history file is add new information and added cache
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return $this
     * @throws InvalidTypeParsingMarkupException
     */
    public function recordHistory(): History
    {

        Markup::yaml()->open($this->getPath())
            ->change(function (&$data) {
                $date = Date::timezone('UTC')->format('Y.m-d H:i:s');

                if (null === $data) {
                    $data = $this->emptyHistory(
                        $date, $this->fileCacheName, $this->cacheSize()
                    );
                } else {
                    if (!array_key_exists($this->type, $data['history'])) {
                        $data['history'] += $this->newTypeOfHistory(
                            $date, $this->fileCacheName, $this->cacheSize()
                        );
                    } else {
                        $lastInfo = $this->getLastCacheInfo($this->type);

                        if ($lastInfo['hash'] !== $this->fileCacheName) {
                            $data['history'][$this->type][$lastInfo['hash']] = [
                                'date' => $lastInfo['date'],
                                'size' => $lastInfo['size']
                            ];
                        }

                        $data['history'][$this->type]['lastCaching'] = $this->lastData(
                            $date, $this->fileCacheName, $this->cacheSize()
                        );
                    }
                }
            });

        return $this;

    }

}