<?php

namespace System\Components\Logging;

/**
 * Interface LoggingInterface
 * @package System\Components\Logging
 *
 * @author  Codememory
 */
interface LoggingInterface
{

    /**
     * @param string $message
     *
     * @return Logging
     */
    public function error(string $message): Logging;

    /**
     * @param string $message
     *
     * @return Logging
     */
    public function warning(string $message): Logging;

    /**
     * @param string $message
     *
     * @return Logging
     */
    public function info(string $message): Logging;


}