<?php

namespace System\Components\DI\Exceptions;

use ErrorException;
use JetBrains\PhpStorm\Pure;

/**
 * Class MethodТotExistsException
 * @package System\Components\DI\Exceptions
 *
 * @author  Codememory
 */
class MethodNotExistsException extends ErrorException
{

    /**
     * MethodТotExistsException constructor.
     *
     * @param string $method
     * @param string $namespace
     */
    #[Pure] public function __construct(string $method = '?', string $namespace = '?')
    {

        parent::__construct(
            sprintf(
                'The <b>%s</b> method does not exist in the <b>%s</b> class, or the method name is incorrect.',
                $method,
                $namespace
            )
        );

    }

}