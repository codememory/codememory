<?php

namespace System\Components\DI;

use ReflectionClass;
use ReflectionException;
use ReflectionFunction;

/**
 * Class Container
 * @package System\Components\DI
 *
 * @author  Codememory
 */
class Container extends CollectArguments
{

    /**
     * @var string
     */
    protected string $name;

    /**
     * @var callable|string
     */
    private $registered;

    /**
     * @var array[]
     */
    protected $argumentsConstructor = [];

    /**
     * @var array
     */
    public array $assembled = [];

    /**
     * @var array
     */
    protected array $arguments = [];

    /**
     * @var array
     */
    protected array $parameters = [];

    /**
     * @var string|null
     */
    private ?string $type = null;

    /**
     * @var array
     */
    protected array $additionalCollectKeys = [];

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method sets the type so that DI understands what it needs to work with objects or functions
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $type
     *
     * @return $this
     */
    protected function setType(string $type): object
    {

        $this->type = $type;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & This method sets the name for the object or function to track. This method is needed to get a
     * & ready-made object or function.
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): object
    {

        $this->name = $name;
        $this->clears();

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method for registering a function or object, or, to be more precise, tell it information
     * & about where to track arguments so that DI can automatically pass dependencies
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param $objectOrCallback
     *
     * @return $this
     */
    protected function register($objectOrCallback): object
    {

        $this->registered = $objectOrCallback;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & This method is needed to pass parameters as an array. Example using an argument: ["varName" => value]
     * & Works with methods and functions
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array $arguments
     *
     * @return $this
     */
    public function addArguments(array $arguments): object
    {

        $this->arguments = $arguments;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & This method is an alias of the "addArguments" method only as 1 argument is used 2
     * & [1 - varName]
     * & [2 - value]
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $argumentName
     * @param        $value
     *
     * @return $this
     */
    public function addArgument(string $argumentName, $value): object
    {

        $this->arguments[$argumentName] = $value;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Method for adding arguments to a constructor
     * & An array with ["varName" => value] is passed as an argument
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array $arguments
     *
     * @return $this
     */
    public function addArgumentsInConstructor(array $arguments): object
    {

        $this->argumentsConstructor = $arguments;

        return $this;

    }

    /**
     * @param $objectOrCallback
     *
     * @return ReflectionClass|ReflectionFunction
     * @throws ReflectionException
     */
    protected function getReflection($objectOrCallback): ReflectionClass|ReflectionFunction
    {

        if ($this->type === 'object') {
            return new ReflectionClass($objectOrCallback);
        }

        return new ReflectionFunction($objectOrCallback);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & This method is called at the end of the method call chain. Builds tracking
     * & information into a single array
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return object
     */
    public function collect(): object
    {

        $data = [
            'name'                 => $this->name,
            'registered'           => $this->registered,
            'arguments'            => $this->arguments,
            'argumentsConstructor' => $this->argumentsConstructor
        ];

        if ($this->additionalCollectKeys !== []) {
            foreach ($this->additionalCollectKeys as $key => $value) {
                $data[$key] = $value;
            }
        }

        $this->assembled[$this->type][$this->name] = $data;
        $this->additionalCollectKeys = [];

        return $this;

    }

    /**
     * METHOD CLEARS
     */
    public function clears(): void
    {

        $this->arguments = [];
        $this->argumentsConstructor = [];

    }

}