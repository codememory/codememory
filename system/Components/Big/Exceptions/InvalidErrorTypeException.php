<?php

namespace System\Components\Big\Exceptions;

use ErrorException;
use JetBrains\PhpStorm\Pure;

/**
 * Class InvalidErrorTypeException
 * @package System\Components\Big\Exceptions
 *
 * @author  Codememory
 */
class InvalidErrorTypeException extends ErrorException
{

    /**
     * InvalidErrorTypeException constructor.
     *
     * @param string|null $type
     */
    #[Pure] public function __construct(?string $type)
    {

        parent::__construct(
            sprintf('The Big templating engine configuration threw an error due to an incorrect type of errors. Error type <b>%s</b> does not exist', $type ?? 'null')

        );
    }

}