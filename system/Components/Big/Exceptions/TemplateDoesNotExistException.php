<?php

namespace System\Components\Big\Exceptions;

use ErrorException;
use JetBrains\PhpStorm\Pure;

/**
 * Class TemplateDoesNotExistException
 * @package System\Components\Big\Exceptions
 *
 * @author  Codememory
 */
class TemplateDoesNotExistException extends ErrorException
{

    /**
     * TemplateDoesNotExistException constructor.
     *
     * @param string $path
     */
    #[Pure] public function __construct(string $path)
    {

        parent::__construct(
            sprintf(
                'Big\'s template engine tried to load template <b>%s</b> But there is no such pattern at the specified path.', $path
            )
        );

    }

}