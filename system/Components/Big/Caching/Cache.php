<?php

namespace System\Components\Big\Caching;

use File;
use System\Components\Big\Util;
use System\FileSystem\Different\Exceptions\IncorrectPathException;

/**
 * Class Cache
 * @package System\Components\Big\Caching
 *
 * @author  Codememory
 */
class Cache
{

    /**
     * @var Util
     */
    private Util $util;

    /**
     * @var string|null
     */
    private ?string $content = null;

    /**
     * @var string
     */
    private string $template;

    /**
     * Cache constructor.
     *
     * @param Util $util
     */
    public function __construct(Util $util, string $path)
    {

        $this->util = $util;
        $this->template = $path;

    }

    /**
     * @param string|null $content
     *
     * @return $this
     */
    public function setContent(?string $content): Cache
    {

        $this->content = $content;

        return $this;

    }

    /**
     * @throws IncorrectPathException
     */
    public function performCaching(): string
    {

        $filename = hash($this->util->cacheFillEncryption(), $this->template);
        $fullPath = $this->util->pathSaveCache();
        $pathWithFilename = sprintf('%s/%s.php', $fullPath, $filename);

        if (false === File::exists($fullPath)) {
            File::mkdir($fullPath, 0777, true);
        }

        File::editor()->put($pathWithFilename, $this->content);


        return File::read($pathWithFilename);

    }

}