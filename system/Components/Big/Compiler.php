<?php

namespace System\Components\Big;

use Env;
use JetBrains\PhpStorm\Pure;
use System\Components\Big\{
    Debugger\Debug,
    Debugger\DebugUtils,
    Interfaces\CompilersInterface};
use System\Components\Big\Compilers\{
    CompilerConstruction,
    CompilerExists,
    CompilerOutput};

/**
 * Class Compiler
 * @package System\Components\Big
 *
 * @author  Codememory
 */
class Compiler
{

    public const SYMBOL_STARTS_OUTPUT = '[[';
    public const SYMBOL_ENDS_OUTPUT = ']]';
    public const CONSTRUCTION_START_SYMBOL = '[@';
    public const CONSTRUCTION_END_SYMBOL = ']';
    public const START_AS_TEXT = '#!';
    public const END_AS_TEXT = '!#';

    /**
     * @var array|string[]
     */
    private array $compilersClasses = [
        CompilerOutput::class,
        CompilerExists::class,
        CompilerConstruction::class
    ];

    /**
     * @var Util
     */
    public Util $util;

    /**
     * @var Debug
     */
    private Debug $debug;

    /**
     * @var array
     */
    private array $parameters;

    /**
     * Compiler constructor.
     *
     * @param Util  $util
     * @param Debug $debug
     */
    public function __construct(Util $util, Debug $debug, array $params)
    {

        $this->util = $util;
        $this->debug = $debug;
        $this->parameters = $params;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns a regular expression with a BIG comment wrapper
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $regex
     *
     * @return string
     */
    #[Pure] public function findWithoutComment(string $regex): string
    {

        return sprintf('(?!%s)%s(?!%s)', self::START_AS_TEXT, $regex, self::END_AS_TEXT);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Create and return a regular expression wrapped with output "[[" "]]"
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $regex
     *
     * @return string
     */
    #[Pure] public function createOutputRegex(string $regex): string
    {

        return sprintf('/%s%s%s/', self::SYMBOL_STARTS_OUTPUT, $this->findWithoutComment($regex), self::SYMBOL_ENDS_OUTPUT);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Create and return a regular expression wrapped with
     * & construction "[@" "]"
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $regex
     *
     * @return string
     */
    #[Pure] public function createConstructionRegex(string $regex): string
    {

        return sprintf('/%s%s%s/', self::CONSTRUCTION_START_SYMBOL, $this->findWithoutComment($regex), self::CONSTRUCTION_END_SYMBOL);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns a list of all compilers from the parsed compiler class
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param CompilersInterface $compiler
     *
     * @return array
     */
    private function callCompiler(CompilersInterface $compiler): array
    {

        return $compiler->getCompilers();

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The main method that creates a comment with a specific style
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string      $styleComment
     * @param string      $comment
     * @param string|null $toInsert
     * @param string      $position
     *
     * @return string
     */
    private function handlerAddComment(string $styleComment, string $comment, ?string &$toInsert, string $position = 'top'): string
    {

        $comment = sprintf($styleComment, $comment);

        if ('top' === $position) {
            $toInsert = $comment . $toInsert;
        } elseif ('bottom' === $position) {
            $toInsert .= PHP_EOL . $comment;
        }

        return $comment;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Creates a php comment and returns it for a
     * & bet in the template
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $comment
     *
     * @return string
     */
    public function addPhpComment(string $comment, ?string &$toInsert, string $position = 'top'): string
    {

        return $this->handlerAddComment("<?php /** %s */ ?>\n", $comment, $toInsert, $position);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Adds a comment at a specific position with html style
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $comment
     *
     * @return string
     */
    public function addHtmlComment(string $comment, ?string &$toInsert, string $position = 'top'): string
    {


        return $this->handlerAddComment("<!-- %s -->\n", $comment, $toInsert, $position);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns an array of variables that should be available
     * & in the template
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return array
     */
    public function variables(): array
    {

        return array_merge(
            $this->parameters,
            [
                '__cdmB_all_variables' => $this->parameters,
                '__cdmB_cache'         => [
                    'filename' => null,
                    'path'     => $this->util->pathSaveCache()
                ],
                '__cdm_env'            => Env::getAll()
            ]
        );

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The main method that loops through the array with compiler
     * & classes and then iterates over all compiler constructs;
     * & if an error occurs, debug returns, otherwise it returns
     * & with the compiled template
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $template
     *
     * @return string
     */
    public function compilersExecution(string $template): string
    {

        $arrayTemplate = DebugUtils::getArrayTemplate($template);

        foreach ($this->compilersClasses as $compilersClass) {
            $compilersClass = new $compilersClass($this, $template, $this->debug);
            $compilers = $this->callCompiler($compilersClass);

            foreach ($compilers as $compilerName) {
                $fullNameCompilerMethod = sprintf('compiler%s', $compilerName);

                foreach ($arrayTemplate as $numberLine => &$line) {
                    $line = call_user_func_array([$compilersClass, $fullNameCompilerMethod], [$line, $numberLine]);
                }

            }

        }

        return implode(PHP_EOL, $arrayTemplate);

    }

}