<?php

namespace System\Components\Big\Compilers;

use JetBrains\PhpStorm\Pure;
use System\Components\Big\AbstractCompilers;
use System\Components\Big\Compiler;
use System\Components\Big\Compilers\DebugMessages\OutputTrait;
use System\Components\Big\Exceptions\InvalidErrorTypeException;
use System\Components\Big\Util;

/**
 * Class CompilerOutput
 * @package System\Components\Big\Compilers
 *
 * @author  Codememory
 */
class CompilerOutput extends AbstractCompilers
{

    use OutputTrait;

    /**
     * @var array|string[]
     */
    private array $compilers = [
        'Echo'
    ];

    /**
     * @param string $perkRegex
     *
     * @return string
     */
    #[Pure] private function echoRegex(string $perkRegex): string
    {

        return sprintf(
            '/%s%s%s/',
            $this->quote(Compiler::SYMBOL_STARTS_OUTPUT),
            $perkRegex,
            $this->quote(Compiler::SYMBOL_ENDS_OUTPUT)
        );

    }

    /**
     * @param array $match
     *
     * @return array
     */
    private function echoSyntax(array $match): array
    {

        $syntax = explode(Compiler::SYMBOL_STARTS_OUTPUT, $match[0][0]);
        $syntax = array_filter($syntax, fn ($value) => !empty($value));

        return array_map(fn ($value) => Compiler::SYMBOL_STARTS_OUTPUT . $value, $syntax);

    }

    /**
     * @param string|null $line
     * @param int         $numberLine
     *
     * @return bool|string
     * @throws InvalidErrorTypeException
     */
    public function compilerEcho(?string $line, int $numberLine): bool|string
    {

        $startSyntax = $this->quote(Compiler::SYMBOL_STARTS_OUTPUT);
        $regex = $this->compiler->findWithoutComment($startSyntax . '.*');
        $this->debug->setLineWithError($numberLine);

        preg_match_all('/' . $regex . '/', $line, $match);

        if ([] !== $match[0]) {
            foreach ($this->echoSyntax($match) as $str) {
                preg_match($this->echoRegex('(?<data>.*)'), $str, $match);

                if ([] === $match) {
                    return $this->debug(Util::E_BIG_SYNTAX, $this->echoDebugMessage());
                } else {
                    $line = preg_replace_callback(
                        $this->echoRegex('(.*)'),
                        fn () => sprintf('<?php echo %s; ?>', trim($match['data'])),
                        $line
                    );
                }
            }
        }

        return $line;

    }

    /**
     * @return array
     */
    public function getCompilers(): array
    {

        return $this->compilers;

    }
}