<?php

namespace System\Components\Big\Compilers;

use System\Components\Big\AbstractCompilers;
use System\Components\Big\Compilers\DebugMessages\ExistsTrait;
use System\Components\Big\Exceptions\InvalidErrorTypeException;
use System\Components\Big\Util;

/**
 * Class CompilerExists
 * @package System\Components\Big\Compilers
 *
 * @author  Codememory
 */
class CompilerExists extends AbstractCompilers
{

    use ExistsTrait;

    /**
     * @var array
     */
    private array $compilers = [
        'Func'
    ];

    /**
     * @param string|null $line
     * @param int         $lineNumber
     *
     * @return array|bool|string|null
     * @throws InvalidErrorTypeException
     */
    public function compilerFunc(?string $line, int $lineNumber): array|bool|string|null
    {

        $this->debug->setLineWithError($lineNumber);
        $regex = '/@(?<func>\w+)\((?<arguments>.*)(?=\))/i';

        preg_match($regex, $line, $match);

        if ([] !== $match) {
            if (false === $this->funcExist($match['func'])) {
                return $this->debug(Util::E_BIG_TRAGIC, $this->funcDebugMessage($match['func']));
            } else {

                $line = preg_replace_callback($regex, fn () => sprintf('%s(%s', $match['func'], $match['arguments']), $line);
            }
        }

        return $line;

    }

    /**
     * @return array
     */
    public function getCompilers(): array
    {

        return $this->compilers;

    }

}