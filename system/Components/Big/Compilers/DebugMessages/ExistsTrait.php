<?php

namespace System\Components\Big\Compilers\DebugMessages;

/**
 * Class ExistsTrait
 * @package System\Components\Big\Compilers\DebugMessages
 *
 * @author  Codememory
 */
trait ExistsTrait
{

    /**
     * @param string $name
     *
     * @return string
     */
    private function funcDebugMessage(string $name): string
    {

        return $this->createDebugMessage(
            'The %s function does not exist, perhaps the name is incorrect', $name
        );

    }

}