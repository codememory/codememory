<?php

namespace System\Components\Big\Compilers\DebugMessages;

/**
 * Trait OutputTrait
 * @package System\Components\Big\Compilers\DebugMessages
 *
 * @author  Codememory
 */
trait OutputTrait
{

    /**
     * @return string
     */
    private function echoDebugMessage(): string
    {

        return $this->createDebugMessage(
            $this->syntaxError('expected to close %s or specify that this is text, put before %s %s'), '"]]"', '"[["', '#!'
        );

    }

}