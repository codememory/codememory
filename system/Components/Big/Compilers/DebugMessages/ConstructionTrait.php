<?php

namespace System\Components\Big\Compilers\DebugMessages;

use System\Components\Big\Compiler;

/**
 * Trait ConstructionTrait
 * @package System\Components\Big\Compilers\DebugMessages
 *
 * @author  Codememory
 */
trait ConstructionTrait
{

    /**
     * @return mixed
     */
    private function closing(): string
    {

        return $this->createDebugMessage(
            $this->syntaxError('construction awaiting closure %s'), Compiler::CONSTRUCTION_END_SYMBOL
        );

    }

    /**
     * @param string $constructionName
     * @param array  $compilers
     *
     * @return string
     */
    private function notFoundConstruction(string $constructionName, array $compilers): string
    {

        return $this->createDebugMessage('The %s construct does not exist. List of all available %s constructs', $constructionName, implode(strtolower(','), $compilers));

    }

    /**
     * @param string $constructionName
     *
     * @return string
     */
    private function emptyValue(string $constructionName): string
    {

        return $this->createDebugMessage('The %s construct must take at least one value, and this value it must not be empty', $constructionName);

    }

    /**
     * @param string $constructionName
     *
     * @return string
     */
    private function takesNoArguments(string $constructionName): string
    {

        return $this->createDebugMessage('The %s construction takes no value. This construction can be used to close another structure', $constructionName);

    }

    /**
     * @return string
     */
    private function emptyArgumentsForeach(): string
    {

        return $this->createDebugMessage('Foreach must contain at least %s %s', '4 values', '{varWithArray} as {varName}');

    }

    /**
     * @return string
     */
    private function foreachArgumentsOverflowed(): string
    {

        return $this->createDebugMessage('Foreach must contain no more than %s %s', '6 values', '{varWithArray} as {varName} => {varName}');

    }

    /**
     * @param string      $constructionName
     * @param string|null $operator
     * @param array       $operators
     *
     * @return string
     */
    private function invalidOperator(string $constructionName, ?string $operator, array $operators): string
    {

        return $this->createDebugMessage('There is no %s operator in %s. List of all operators %s', $operator, $constructionName, implode(',', $operators));

    }

    /**
     * @return string
     */
    private function invalidArgumentsFor(): string
    {

        return $this->createDebugMessage('For must contain at least one value. Minimum value %s', '";;"');

    }

    /**
     * @param string $message
     * @param mixed  ...$params
     *
     * @return string
     */
    private function createMessage(string $message, ...$params): string
    {

        return $this->createDebugMessage($message, ...$params);

    }

}