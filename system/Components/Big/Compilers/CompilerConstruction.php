<?php

namespace System\Components\Big\Compilers;

use File;
use JetBrains\PhpStorm\Pure;
use ReflectionException;
use ReflectionFunction;
use System\Components\Big\AbstractCompilers;
use System\Components\Big\BigSlab;
use System\Components\Big\Compiler;
use System\Components\Big\Compilers\DebugMessages\ConstructionTrait;
use System\Components\Big\Exceptions\InvalidErrorTypeException;
use System\Components\Big\Util;
use System\Http\Request\CdmToken;

/**
 * Class CompilerConstruction
 * @package System\Components\Big\Compilers\DebugMessages
 *
 * @author  Codememory
 */
class CompilerConstruction extends AbstractCompilers
{

    use ConstructionTrait;

    /**
     * @var array
     */
    private array $compilers = [
        'If', 'ElseIf', 'Else', 'EndIf', 'Foreach',
        'EndForeach', 'For', 'endFor', 'formToken',
        'Break', 'Continue', 'Insert'
    ];

    /**
     * @var array|string[]
     */
    private array $operatorsForeach = [
        'as'
    ];

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Splits the string into tokens and returns an array of tokens
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|null $line
     *
     * @return string|null
     */
    private function breakdownIntoTokens(?string $line): ?string
    {

        $tokens = explode(Compiler::CONSTRUCTION_START_SYMBOL, $line);
        $tokens = array_filter($tokens, fn (mixed $value) => false === empty($value));

        return $tokens[array_key_first($tokens)];

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Method that checks the closure of the structure
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|null $line
     *
     * @return bool
     * @throws InvalidErrorTypeException
     */
    private function closingVerification(?string $line): bool
    {

        if (false === str_ends_with($line, Compiler::CONSTRUCTION_END_SYMBOL)) {
            return $this->debug(Util::E_BIG_SYNTAX, $this->closing());
        }

        return true;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The main method to start compiling a string. The method immediately
     * & automatically checks the closure of the structure
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|null $line
     * @param int         $lineNumber
     * @param callable    $handler
     *
     * @return mixed
     * @throws InvalidErrorTypeException
     */
    private function executeStart(?string $line, int $lineNumber, callable $handler): mixed
    {

        $line = trim($line);

        if (str_starts_with($line, Compiler::CONSTRUCTION_START_SYMBOL)) {
            $this->closingVerification($line);

            $line = call_user_func($handler, $line, $lineNumber);

            return $line;
        }

        return $line;

    }

    /**
     * @param string|null $tokenSeparator
     * @param string      $compilerName
     * @param string|null $line
     * @param callable    $handler
     *
     * @return false|mixed|string|null
     * @throws InvalidErrorTypeException|ReflectionException
     */
    private function handler(?string $tokenSeparator, string $compilerName, ?string $line, callable $handler): mixed
    {

        $reflectionHandler = new ReflectionFunction($handler);

        $tokens = explode($tokenSeparator, $this->breakdownIntoTokens($line));
        $constructionName = $this->getNameConstruction($tokens);

        $this->compilerExist($constructionName);

        if ($compilerName === $constructionName) {

            unset($tokens[0]);

            $value = substr(implode(' ', $tokens), 0, -1);
            $allArguments = [
                'line'             => $line,
                'tokens'           => $tokens,
                'constructionName' => $constructionName,
                'value'            => $value
            ];
            $passArguments = [];

            foreach ($reflectionHandler->getParameters() as $arguments) {
                $passArguments[$arguments->name] = $allArguments[$arguments->name];
            }

            return call_user_func_array($handler, $passArguments);
        }

        return $line;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Checking for the existence of a compiler in the entire
     * & list of constructs
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|null $compiler
     *
     * @return CompilerConstruction
     * @throws InvalidErrorTypeException
     */
    private function compilerExist(?string $compiler): CompilerConstruction
    {

        $compilers = array_map(fn ($value) => lcfirst($value), $this->compilers);

        if (false === in_array(lcfirst($compiler), $compilers)) {
            $this->debug(Util::E_BIG_TRAGIC, $this->notFoundConstruction($compiler, $compilers));
        }

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Getting the name of the structure
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array $tokens
     *
     * @return string
     */
    #[Pure] private function getNameConstruction(array $tokens): string
    {

        $name = lcfirst($tokens[0] ?? null);

        if (count($tokens) === 1) {
            $name = substr($name, 0, -1);
        }

        return $name;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & A construct handler that has the same parameters as a condition construct
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string      $handlerConstructionName
     * @param string      $replace
     * @param string|null $line
     * @param int         $lineNumber
     *
     * @return mixed
     * @throws InvalidErrorTypeException
     */
    private function handlerWithArgumentsAsCondition(string $handlerConstructionName, string $replace, ?string $line, int $lineNumber): mixed
    {

        $this->debug->setLineWithError($lineNumber);

        return $this->executeStart($line, $lineNumber, function (string $line) use ($handlerConstructionName, $replace) {
            return $this->handler(' ', $handlerConstructionName, $line, function (?string $line, array $tokens, ?string $constructionName, ?string $value) use ($handlerConstructionName, $replace) {
                if (false === array_key_exists(1, $tokens) || empty($tokens[1])) {
                    return $this->debug(Util::E_BIG_TRAGIC, $this->emptyValue($constructionName));
                }

                $line = str_replace($line, sprintf('<?php %s(%s): ?>', $replace, $value), $line);

                return $line;
            });
        });

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Condition construction
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|null $line
     * @param int         $lineNumber
     *
     * @return mixed
     * @throws InvalidErrorTypeException
     */
    public function compilerIf(?string $line, int $lineNumber): mixed
    {

        return $this->handlerWithArgumentsAsCondition('if', 'if', $line, $lineNumber);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Construction of otherwise if conditions "elseif"
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|null $line
     * @param int         $lineNumber
     *
     * @return mixed
     * @throws InvalidErrorTypeException
     */
    public function compilerElseIf(?string $line, int $lineNumber): mixed
    {

        return $this->handlerWithArgumentsAsCondition('elseIf', 'elseif', $line, $lineNumber);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The handler that processes the closure construct of another construct
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string      $handlerConstructionName
     * @param string      $replace
     * @param string|null $line
     * @param int         $lineNumber
     *
     * @return mixed
     * @throws InvalidErrorTypeException
     */
    private function handlerEnd(string $handlerConstructionName, string $replace, ?string $line, int $lineNumber): mixed
    {

        $this->debug->setLineWithError($lineNumber);

        return $this->executeStart($line, $lineNumber, function (string $line) use ($handlerConstructionName, $replace) {
            $tokens = explode(' ', $this->breakdownIntoTokens($line));
            $constructionName = $this->getNameConstruction($tokens);

            $this->compilerExist($constructionName);

            if ($handlerConstructionName === $constructionName) {

                if (count($tokens) > 1) {
                    $this->debug(Util::E_BIG_WARNING, $this->takesNoArguments($constructionName));
                }

                $line = str_replace($line, sprintf('<?php %s ?>', $replace), $line);
            }

            return $line;
        });

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Constructing conditions otherwise "else"
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|null $line
     * @param int         $lineNumber
     *
     * @return mixed
     * @throws InvalidErrorTypeException
     */
    public function compilerElse(?string $line, int $lineNumber): mixed
    {

        return $this->handlerEnd('else', 'else:', $line, $lineNumber);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Closing construct of the "endif" condition
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|null $line
     * @param int         $lineNumber
     *
     * @return mixed
     * @throws InvalidErrorTypeException
     */
    public function compilerEndIf(?string $line, int $lineNumber): mixed
    {

        return $this->handlerEnd('endIf', 'endif;', $line, $lineNumber);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The foreach loop construct. It cannot accept an array as 1 argument,
     * & it can only accept a variable in which the array
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|null $line
     * @param int         $lineNumber
     *
     * @return mixed
     * @throws InvalidErrorTypeException
     */
    public function compilerForeach(?string $line, int $lineNumber): mixed
    {

        $this->debug->setLineWithError($lineNumber);

        return $this->executeStart($line, $lineNumber, function (string $line) {
            return $this->handler(' ', 'foreach', $line, function (?string $line, array $tokens, ?string $constructionName, ?string $value) {
                if (count($tokens) < 3) {
                    return $this->debug(Util::E_BIG_TRAGIC, $this->emptyArgumentsForeach());
                } elseif (count($tokens) > 3 && count($tokens) < 5 || count($tokens) > 5) {
                    return $this->debug(Util::E_BIG_TRAGIC, $this->foreachArgumentsOverflowed());
                }

                $operator = $tokens[2];

                if (false === in_array($operator, $this->operatorsForeach)) {
                    return $this->debug(Util::E_BIG_TRAGIC, $this->invalidOperator($constructionName, $operator, $this->operatorsForeach));
                }

                if (count($tokens) === 5) {
                    $operators = ['=>'];

                    if (false === in_array($tokens[4], $operators)) {
                        return $this->debug(Util::E_BIG_TRAGIC, $this->invalidOperator($constructionName, $tokens[4], $operators));
                    }
                }

                $line = str_replace($line, sprintf('<?php foreach(%s): ?>', $value), $line);

                return $line;
            });
        });

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Closing the foreach loop
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|null $line
     * @param int         $lineNumber
     *
     * @return mixed
     * @throws InvalidErrorTypeException
     */
    public function compilerEndForeach(?string $line, int $lineNumber): mixed
    {

        return $this->handlerEnd('endForeach', 'endforeach;', $line, $lineNumber);

    }

    /**
     * @param string|null $line
     * @param int         $lineNumber
     *
     * @return string|null
     * @throws InvalidErrorTypeException
     */
    public function compilerFor(?string $line, int $lineNumber): ?string
    {

        $this->debug->setLineWithError($lineNumber);

        return $this->executeStart($line, $lineNumber, function (string $line) {
            return $this->handler(' ', 'for', $line, function (?string $line, array $tokens, ?string $value) {

                if ([] === $tokens || (count($tokens) === 1 && $value !== ';;')) {
                    return $this->debug(Util::E_BIG_TRAGIC, $this->invalidArgumentsFor());
                }

                $line = str_replace($line, sprintf('<?php for(%s): ?>', $value), $line);

                return $line;
            });
        });

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Closing the for loop
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|null $line
     * @param int         $lineNumber
     *
     * @return mixed
     * @throws InvalidErrorTypeException
     */
    public function compilerEndFor(?string $line, int $lineNumber): mixed
    {

        return $this->handlerEnd('endFor', 'endfor;', $line, $lineNumber);

    }

    /**
     * @param string|null $line
     * @param int         $lineNumber
     *
     * @return mixed
     * @throws InvalidErrorTypeException
     */
    public function compilerFormToken(?string $line, int $lineNumber): mixed
    {

        $this->debug->setLineWithError($lineNumber);

        return $this->executeStart($line, $lineNumber, function (string $line) {
            return $this->handler(' ', 'formToken', $line, function (?string $line, array $tokens, ?string $constructionName, ?string $value) {
                if (false === array_key_exists(1, $tokens) || empty($tokens[1])) {
                    $this->debug(Util::E_BIG_TRAGIC, $this->emptyValue($constructionName));
                }

                $inputName = sprintf(CdmToken::INPUT_NAME, $value);
                $line = str_replace($line, sprintf('<input type="hidden" name="%s" value="<?php echo cdmToken(\'%s\'); ?>">', $inputName, $value), $line);

                return $line;
            });
        });

    }

    /**
     * @param string|null $line
     * @param int         $lineNumber
     *
     * @return mixed
     * @throws InvalidErrorTypeException
     */
    public function compilerBreak(?string $line, int $lineNumber): mixed
    {

        return $this->handlerEnd('break', 'break;', $line, $lineNumber);

    }

    /**
     * @param string|null $line
     * @param int         $lineNumber
     *
     * @return mixed
     * @throws InvalidErrorTypeException
     */
    public function compilerContinue(?string $line, int $lineNumber): mixed
    {

        return $this->handlerEnd('continue', 'continue;', $line, $lineNumber);

    }

    /**
     * @param string|null $line
     * @param int         $lineNumber
     *
     * @return mixed
     * @throws InvalidErrorTypeException
     */
    public function compilerInsert(?string $line, int $lineNumber): mixed
    {

        $this->debug->setLineWithError($lineNumber);

        return $this->executeStart($line, $lineNumber, function (?string $line) {
            return $this->handler(' ', 'insert', $line, function (?string $line, ?string $value) {
                $pathToTemplate = $this->compiler->util->pathToTemplates();
                $pathWithTemplateName = str_replace('.', '/', $pathToTemplate . '.' . $value) . BigSlab::EXPANSION;

                if (empty($value)) {
                    $this->debug(Util::E_BIG_TRAGIC, $this->createMessage('The %s construct must take at least one parameter - the path to the template', 'Insert'));
                } elseif (false === File::exists($pathWithTemplateName)) {
                    $this->debug(Util::E_BIG_TRAGIC, $this->createMessage('The path to the template is incorrect. The %s pattern was not found. There may be errors in the following: "an extension is specified", "an absolute path to a template is specified", "an attempt to insert a template that does not have a %s extension"', $pathWithTemplateName, BigSlab::EXPANSION));
                } else {
                    $template = File::read($pathWithTemplateName);

                    $line = str_replace($line, $this->compiler->compilersExecution($template), $line);

                    if (isDevelopment()) {
                        $this->compiler->addHtmlComment(sprintf('Template output from file %s starts', $pathWithTemplateName), $line);
                        $this->compiler->addHtmlComment(sprintf('Ends with output from template %s', $pathWithTemplateName), $line, 'bottom');
                    }
                }

                return $line;
            });
        });

    }

    /**
     * @return array
     */
    public function getCompilers(): array
    {

        return $this->compilers;

    }
}