<?php

namespace System\Components\Big;

use File;
use System\Components\Big\Caching\Cache;
use System\Components\Big\Debugger\Debug;
use System\Components\Big\Debugger\DebugUtils;
use System\Components\Big\Exceptions\TemplateDoesNotExistException;
use System\FileSystem\Different\Exceptions\IncorrectPathException;

/**
 * Class BigSlab
 * @package System\Components\Big
 *
 * @author  Codememory
 */
class BigSlab
{

    public const EXPANSION = '.big';

    /**
     * @var string|null
     */
    private ?string $pathToTemplate = null;

    /**
     * @var DebugUtils|null
     */
    protected ?DebugUtils $debugUtils = null;

    /**
     * @var Util
     */
    private Util $util;

    /**
     * @var Debug
     */
    private Debug $debug;

    /**
     * @var array
     */
    private array $parameters = [];

    /**
     * BigSlab constructor.
     */
    public function __construct()
    {

        $this->util = new Util();
        $this->debug = new Debug($this->util);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Getting the full path to a template
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $template
     * @param bool   $withExpansion
     *
     * @return string
     */
    private function getFullPath(string $template, bool $withExpansion = true): string
    {

        $expansion = $withExpansion ? self::EXPANSION : null;
        $path = $this->util->pathToTemplates() . '.' . $template;

        return str_replace('.', '/', $path) . $expansion;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Open a specific template
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $template
     *
     * @return $this
     * @throws TemplateDoesNotExistException
     */
    public function openTemplate(string $template): BigSlab
    {

        $this->pathToTemplate = $this->getFullPath($template);

        if (false === $this->templateExist($template)) {
            throw new TemplateDoesNotExistException($this->pathToTemplate);
        }

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Pass some variables to the template, as a key to mass this is
     * & the name of the variable
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param array $params
     *
     * @return $this
     */
    public function setParameters(array $params): BigSlab
    {

        $this->parameters = $params;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Checking for the existence of a template
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $path
     *
     * @return bool
     */
    public function templateExist(string $path): bool
    {

        return File::exists($this->getFullPath($path));

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns a debug object with installed tracking files
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return Debug
     */
    private function createDebug(): Debug
    {

        $this->debug->setFileWithError($this->pathToTemplate);

        return $this->debug;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Translating an array into variables and returning a template
     * & to eval to work out php code
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param Compiler    $compiler
     * @param string|null $template
     *
     * @return mixed
     */
    private function e(Compiler $compiler, ?string $template): mixed
    {


        extract($compiler->variables());

        return eval(sprintf('?>%s', $template));

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Runs all necessary functions and returns the template
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @throws IncorrectPathException
     */
    public function make()
    {

        $cache = new Cache($this->util, $this->pathToTemplate);
        $compiler = new Compiler($this->util, $this->createDebug(), $this->parameters);
        $template = $compiler->compilersExecution(
            File::read($this->pathToTemplate)
        );

        if (false === $this->util->templateCaching()) {
            return $this->e($compiler, $template);
        }

        return $this->e($compiler, $cache->setContent($template)->performCaching());

    }

}