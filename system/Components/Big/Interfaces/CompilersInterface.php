<?php

namespace System\Components\Big\Interfaces;

/**
 * Interface CompilersInterface
 * @package System\Components\Big\Interfaces
 *
 * @author  Codememory
 */
interface CompilersInterface
{

    /**
     * @return array
     */
    public function getCompilers(): array;

}