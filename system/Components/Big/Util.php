<?php

namespace System\Components\Big;

use Config;
use System\Components\Big\Exceptions\InvalidErrorTypeException;

/**
 * Class Configuration
 * @package System\Components\Big
 *
 * @author  Codememory
 */
class Util
{

    public const DEFAULT_ERROR_TYPE = 'E_BIG_ALL';

    private const INTERRUPT_FOLLOWING_CODE = true;
    private const TEMPLATE_MAX_NUMBER_LINES = 20;
    private const CACHE_TEMPLATE = true;
    private const CACHE_FILE_ENCRYPTION = 'sha256';
    private const PATH_SAVE_CACHE = 'storage.cache.templates.big';

    public const E_BIG_WARNING = 0;
    public const E_BIG_TRAGIC = 1;
    public const E_BIG_ALL = 2;
    public const E_BIG_SYNTAX = 3;

    /**
     * List of all available error types
     *
     * @var array|string[]
     */
    public array $allErrorTypes = [
        0 => 'E_BIG_WARNING',
        1 => 'E_BIG_TRAGIC',
        2 => 'E_BIG_ALL',
        3 => 'E_BIG_SYNTAX'
    ];

    /**
     * @var array|mixed
     */
    public array $config;

    /**
     * Configuration constructor.
     */
    public function __construct()
    {

        $this->config = Config::open('configs.packages')->get('big');

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Whether to show debug when an error occurs
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool
     */
    public function displayErrors(): bool
    {

        return $this->config['displayErrors'] ?? envi('app.debug');

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns an array of error types to show when an error occurs
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return array
     * @throws InvalidErrorTypeException
     */
    public function errorTypes(): array
    {

        $types = $this->config['errors']['types'] ?? [];
        $types = empty($types) ? [] : $types;

        if ([] !== $types) {
            foreach ($types as $type) {
                if (false === in_array($type, $this->allErrorTypes)) {
                    throw new InvalidErrorTypeException($type);
                }
            }
        } else {
            $types[] = self::DEFAULT_ERROR_TYPE;
        }

        return $types;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Whether to interrupt the following executable code if a
     * & compilation error occurs
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool
     */
    public function interruptFollowingCode(): bool
    {

        return $this->config['errors']['interruptFollowingCode'] ?? self::INTERRUPT_FOLLOWING_CODE;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns the number of lines of code to display when a
     * & compilation error occurs
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return int
     */
    public function templateMaxNumberLines(): int
    {

        return $this->config['errors']['template']['maxNumberLines'] ?? self::TEMPLATE_MAX_NUMBER_LINES;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Whether to create a cache and load the called template
     * & from the cache
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool
     */
    public function templateCaching(): bool
    {

        return $this->config['caching']['cache'] ?? self::CACHE_TEMPLATE;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns the path where the cache of the called template
     * & should be saved
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return mixed|string|string[]
     */
    public function pathSaveCache(): mixed
    {

        $path = $this->config['caching']['path'] ?? self::PATH_SAVE_CACHE;

        return str_replace('.', '/', $path);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Cache file name encryption algorithm
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return string
     */
    public function cacheFillEncryption(): string
    {

        return $this->config['caching']['encryption'] ?? self::CACHE_FILE_ENCRYPTION;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Restores the path where all the templates lay
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return string|null
     */
    public function pathToTemplates(): ?string
    {

        return $this->config['pathToTemplates'] ?? null;

    }

}