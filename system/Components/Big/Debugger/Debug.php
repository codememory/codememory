<?php

namespace System\Components\Big\Debugger;

use File;
use RuntimeException;
use System\Components\Big\Exceptions\InvalidErrorTypeException;
use System\Components\Big\Util;

/**
 * Class Debug
 * @package System\Components\Big\Debugger
 *
 * @author  Codememory
 */
class Debug
{

    /**
     * @var string|null
     */
    private ?string $type = null;

    /**
     * @var string|null
     */
    private ?string $pathWithError = null;

    /**
     * @var int
     */
    private int $lineWithError = 0;

    /**
     * @var string|null
     */
    private ?string $errorMessage = null;

    /**
     * @var Util
     */
    protected Util $util;

    /**
     * Debug constructor.
     *
     * @param Util $util
     */
    public function __construct(Util $util)
    {

        $this->util = $util;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Set the type of error that will be displayed in the debug template
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param int $type
     *
     * @return $this
     */
    public function setErrorType(int $type): Debug
    {

        $this->type = $this->util->allErrorTypes[$type];

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Set the path to the template where the error occurred
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $path
     *
     * @return $this
     */
    public function setFileWithError(string $path): Debug
    {

        $this->pathWithError = $path;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Set the line number of the compilation error
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param int $line
     *
     * @return $this
     */
    public function setLineWithError(int $line): Debug
    {

        $this->lineWithError = $line;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Set compile error message
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $message
     *
     * @return $this
     */
    public function setErrorMessage(string $message): Debug
    {

        $this->errorMessage = $message;

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Checking all the specified information to create a debug in
     * & case of false, exceptions will be thrown
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return Debug
     */
    private function checkBeforeReceiving(): Debug
    {

        if (null === $this->type) {
            $this->type = Util::DEFAULT_ERROR_TYPE;
        }

        if (null === $this->pathWithError || false === File::exists($this->pathWithError)) {
            throw new RuntimeException('An error occurred while asking to create a debug for the big templating engine. The path to the file where the error occurred is not specified, or the path to the file is incorrect.');
        } elseif (0 === $this->lineWithError) {
            throw new RuntimeException('Debug of the Big template engine cannot understand which line the error is on, perhaps when creating debug, the line number where the error occurred was not specified.');
        }

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Creates an array of all the tender information to be passed
     * & to the debug processing class and finally returns the debug itself
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool
     * @throws InvalidErrorTypeException
     */
    public function makeDebug(): bool
    {

        $templateByLines = explode(PHP_EOL, File::read($this->pathWithError));

        $debugUtils = new DebugUtils($templateByLines, $this->util, $this->lineWithError);
        $handler = new Handler($this->util, $debugUtils, [
            'type'    => $this->type,
            'message' => $this->errorMessage,
            'file'    => $this->pathWithError,
            'line'    => $this->lineWithError
        ]);

        $this->checkBeforeReceiving();

        return $handler->getDebug();

    }

}