<?php

namespace System\Components\Big\Debugger;

use JetBrains\PhpStorm\Pure;
use System\Components\Big\Util as BigConfigUtils;

/**
 * Class Calculations
 * @package System\Components\Big\Debugger
 *
 * @author  Codememory
 */
class DebugUtils
{

    private const ERROR_TEMPLATE = 'system.Components.Big.Debugger.templates.errorTemplate';
    private const EXTENDING_ERROR_TEMPLATE = '.php';

    private array $template;

    /**
     * @var BigConfigUtils
     */
    private BigConfigUtils $utils;

    /**
     * @var int
     */
    private int $lineWithError;

    /**
     * DebugUtils constructor.
     *
     * @param array          $templateInLines
     * @param BigConfigUtils $utils
     * @param int            $lineWithError
     */
    public function __construct(array $templateInLines, BigConfigUtils $utils, int $lineWithError)
    {

        $this->template = $templateInLines;
        $this->utils = $utils;
        $this->lineWithError = $lineWithError;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>
     * & Get error number
     * <=<=<=<=<=<=<=<=<=<=
     *
     * @return int
     */
    public function getLineWithError(): int
    {

        return $this->lineWithError;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns half the number from the configuration
     * & maximum number of lines to display
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return int
     */
    public function getHalfLines(): int
    {

        return ceil($this->utils->templateMaxNumberLines() / 2);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get the number of lines in a template
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return int
     */
    #[Pure] public function getNumberLinesInTemplate(): int
    {

        return count($this->template);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get the number from which to start the cycle of selecting rows
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return int
     */
    public function getTopHalfByErrorCode(): int
    {

        $lineWithError = $this->lineWithError;
        $top = $lineWithError - $this->getHalfLines();

        if (0 === $top || $this->getNumberLinesInTemplate() <= $this->utils->templateMaxNumberLines()) {
            return 1;
        }

        return $top < 0 ? 1 : $top;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Get the number at which to end the loop for selecting rows
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return int
     */
    public function getBottomHalfByErrorCode(): int
    {

        $lineWithError = $this->lineWithError;
        $bottom = ($lineWithError + $this->getHalfLines());

        if ($bottom < ($this->getHalfLines() * 2)) {
            $bottom += ($this->getHalfLines() * 2) - $bottom;
        }

        return $bottom > $this->getNumberLinesInTemplate() ? $this->getNumberLinesInTemplate() : $bottom;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns an array pattern
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string|array $template
     *
     * @return array
     */
    #[Pure] public static function getArrayTemplate(string|array $template): array
    {

        $codeByLine = $template;
        $code = [];

        if (is_string($template)) {
            $codeByLine = explode(PHP_EOL, $template);
        }

        foreach ($codeByLine as $index => $value) {
            $code[++$index] = $value;
        }

        return $code;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & A handler that is called in the debug template to display
     * & all the information of the generated debug
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param callable $handler
     *
     * @return $this
     */
    public function displayTemplateWithError(callable $handler): DebugUtils
    {

        if ($this->utils->displayErrors()) {
            $code = self::getArrayTemplate($this->template);

            for ($i = $this->getTopHalfByErrorCode(); $i <= $this->getBottomHalfByErrorCode(); $i++) {
                call_user_func($handler, $i, htmlspecialchars($code[$i]), $this->lineWithError);
            }
        }

        return $this;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns the path to the template where the
     * & compilation error occurred
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return string
     */
    public function getPathErrorTemplate(): string
    {

        $path = str_replace('.', '/', self::ERROR_TEMPLATE);

        return $path . self::EXTENDING_ERROR_TEMPLATE;

    }

}