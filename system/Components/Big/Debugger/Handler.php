<?php

namespace System\Components\Big\Debugger;

use File;
use System\Components\Big\Exceptions\InvalidErrorTypeException;
use System\Components\Big\Util;

/**
 * Class Handler
 * @package System\Components\Big\Debugger
 *
 * @author  Codememory
 */
class Handler
{

    /**
     * @var Util
     */
    private Util $util;

    /**
     * @var DebugUtils
     */
    private DebugUtils $debugUtils;

    /**
     * @var array
     */
    private array $errorInfo;

    /**
     * Handler constructor.
     *
     * @param Util       $util
     * @param DebugUtils $debugUtils
     * @param array      $errorInfo
     */
    public function __construct(Util $util, DebugUtils $debugUtils, array $errorInfo)
    {

        $this->util = $util;
        $this->debugUtils = $debugUtils;
        $this->errorInfo = $errorInfo;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns a boolean value by checking the error types with
     * & the configuration and the error type of the debug being generated
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool
     * @throws InvalidErrorTypeException
     */
    private function typeMatch(): bool
    {

        $permitted = $this->util->errorTypes();

        return in_array($this->errorInfo['type'], $permitted)
            || $this->errorInfo['type'] === Util::DEFAULT_ERROR_TYPE
            || in_array(Util::DEFAULT_ERROR_TYPE, $permitted);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns the generated debug including the debug template
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return bool
     * @throws InvalidErrorTypeException
     */
    public function getDebug(): bool
    {

        if (true === $this->util->displayErrors() && $this->typeMatch()) {
            ob_start();
            extract([
                'errorInfo'  => $this->errorInfo,
                'debugUtils' => $this->debugUtils
            ]);
            require_once File::getRealPath($this->debugUtils->getPathErrorTemplate());

            echo ob_get_clean();
        }

        return true;

    }

}