<?php

namespace System\Components\Big;

use JetBrains\PhpStorm\Pure;
use Response;
use System\Components\Big\Debugger\Debug;
use System\Components\Big\Debugger\DebugUtils;
use System\Components\Big\Interfaces\CompilersInterface;

/**
 * Class AbstractCompilers
 * @package System\Components\Big
 *
 * @author  Codememory
 */
abstract class AbstractCompilers implements CompilersInterface
{

    /**
     * @var Compiler
     */
    protected Compiler $compiler;

    /**
     * @var string
     */
    protected string $template;

    /**
     * @var Debug
     */
    protected Debug $debug;

    /**
     * AbstractCompilers constructor.
     *
     * @param Compiler $compiler
     * @param string   $template
     * @param Debug    $debug
     */
    public function __construct(Compiler $compiler, string $template, Debug $debug)
    {

        $this->compiler = $compiler;
        $this->template = $template;
        $this->debug = $debug;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns the template in the form of an array, i.e. each element
     * & in the mass is a new line, as a key it is a line number
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return array
     */
    #[Pure] protected function getArrayTemplate(): array
    {

        return DebugUtils::getArrayTemplate($this->template);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Returns small text to generate debug for syntax error
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $add
     *
     * @return string
     */
    protected function syntaxError(string $add): string
    {

        return 'Syntax error, ' . $add;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Automatically bold specific words for debug messages
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $message
     * @param mixed  ...$params
     *
     * @return string
     */
    protected function createDebugMessage(string $message, ...$params): string
    {

        $message = str_replace('%s', '<b class="feature">%s</b>', $message);

        return sprintf($message, ...$params);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Escapes strings for regular expression
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $syntax
     *
     * @return string
     */
    #[Pure] protected function quote(string $syntax): string
    {

        return preg_quote($syntax);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Creates debug and invokes the generated debug. You need an
     * & error type and an error message as arguments
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param int    $type
     * @param string $message
     *
     * @return bool
     * @throws Exceptions\InvalidErrorTypeException
     */
    protected function debug(int $type, string $message): bool
    {

        Response::responseStatus(500)->concatSendHeaders();

        $this->debug
            ->setErrorType($type)
            ->setErrorMessage($message)
            ->makeDebug();

        if ($this->compiler->util->interruptFollowingCode()) {
            die;
        }

        return false;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Method that checks the existence of a function
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $func
     *
     * @return bool
     */
    #[Pure] protected function funcExist(string $func): bool
    {

        $customFunctions = get_defined_functions()['user'];

        return in_array($func, $customFunctions);

    }

}