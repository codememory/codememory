<?php

namespace Kernel\Facades;

use Kernel\Facades\Handler\Facade;
use System\FileSystem\Configuration\Config as AppConfig;

/**
 * @method static AppConfig open(string $config)
 * @method static AppConfig package()
 * @method static array|string get(?string $keys = null)
 * @method static mixed binds(?string $bind = null)
 *
 * Class Config
 * @package Kernel\Facades
 *
 * @author  Codememory
 */
final class Config extends Facade
{

    /**
     * @return string
     */
    protected static function init(): string
    {

        return self::$service = 'config';

    }

}
