<?php

namespace Kernel\Facades;

use JetBrains\PhpStorm\Pure;
use Kernel\Facades\Handler\Facade;
use System\Support\UnitConversion\Conversion;
use System\Support\UnitConversion\Units\AbstractUnit;

/**
 * @method static Conversion setConvertibleNumber(int|float $number)
 * @method static AbstractUnit from(AbstractUnit $unit)
 *
 * Class UnitConversion
 * @package Kernel\Facades
 *
 * @author  Codememory
 */
final class UnitConversion extends Facade
{

    public const CURRENT = 'getConvertible';
    public const UNIT_KB = 'getKb';
    public const UNIT_MB = 'getMB';
    public const UNIT_GB = 'getGb';
    public const UNIT_TB = 'getTb';
    public const UNIT_PB = 'getPb';
    public const UNIT_EB = 'getEb';
    public const UNIT_ZB = 'getZb';
    public const UNIT_YB = 'getYb';

    /**
     * @return Conversion
     */
    #[Pure] protected static function init(): Conversion
    {

        return new Conversion();

    }

}
