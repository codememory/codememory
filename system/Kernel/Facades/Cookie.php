<?php

namespace Kernel\Facades;

use Kernel\Facades\Handler\Facade;
use System\Http\Client\Cookie as AppCookie;

/**
 * @method static AppCookie create(string $name, string|int|float|null $value = null, ?string $domain = null, ?string $path = null, int $expires = 0, bool $httpOnly = false, bool $secure = false, ?string $sameSite = AppCookie::SAME_SITE_LAX)
 * @method static array all()
 * @method static bool missing(string $name)
 * @method static AppCookie whenMissing(string $name, callable $callback)
 * @method static null|int|string get(string $name)
 * @method static bool remove(string $name)
 * @method static AppCookie send()
 * @method static string|null getName()
 * @method static AppCookie setName(?string $name)
 * @method static float|int|string|null getValue()
 * @method static AppCookie setValue(float|int|string|null $value)
 * @method static string|null getDomain()
 * @method static AppCookie setDomain(?string $domain)
 * @method static string|null getPath()
 * @method static AppCookie setPath(?string $path)
 * @method static int getExpires()
 * @method static AppCookie setExpires(int $expires)
 * @method static bool isHttpOnly()
 * @method static AppCookie setHttpOnly(bool $httpOnly)
 * @method static bool isSecure()
 * @method static AppCookie setSecure(bool $secure)
 * @method static string|null getSameSite()
 * @method static AppCookie setSameSite(?string $sameSite)
 *
 * Class Cookie
 * @package Kernel\Facades
 */
final class Cookie extends Facade
{

    public const SAME_SITE_LAX = 'Lax';
    public const SAME_SITE_STRICT = 'Strict';
    public const SAME_SITE_NONE = 'None';

    /**
     * @return string
     */
    public static function init(): string
    {

        return self::$service = 'cookie';        

    }

}
