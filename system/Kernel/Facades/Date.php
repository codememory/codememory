<?php

namespace Kernel\Facades;

use Kernel\Facades\Handler\Facade;
use System\Support\DateTime\DateTime;
use System\Support\DateTime\Time;

/**
 * @method static DateTime|Time setDate(?string $date = null, int $day = 1, int $month = 0, int $year = 0)
 * @method static DateTime|Time setTime(int $hour = 0, int $minute = 0, int $second = 0, bool $timestamp = false)
 * @method static DateTime|Time modify(string $modify)
 * @method static DateTime|Time addTime()
 * @method static DateTime|Time years(int $number)
 * @method static DateTime|Time months(int $number)
 * @method static DateTime|Time weeks(int $number)
 * @method static DateTime|Time days(int $number)
 * @method static DateTime|Time hours(int $number)
 * @method static DateTime|Time minutes(int $number)
 * @method static DateTime|Time seconds(int $number)
 * @method static int|string now()
 * @method static DateTime|Time sub()
 * @method static DateTime|Time splitByFormat(string $format, string $date)
 * @method static DateTime|Time diff(string $compare, string $target)
 * @method static string format(string $format)
 * @method static Timezone|DateTime|Time timezone(string $timezone)
 * @method static DateTimeZone|Timezone getTimezone()
 *
 * Class Date
 * @package Kernel\Facades
 *
 * @author  Codememory
 */
final class Date extends Facade
{

    /**
     * @return string
     */
    public static function init(): string
    {

        return self::$service = 'date';

    }

}
