<?php

namespace Kernel\Facades\Traits;

/**
 * Trait DefaultMethodsTrait
 * @package Kernel\Facades\Traits
 *
 * @author Codememory
 */
trait DefaultMethodsTrait
{

    /**
     * @return object|null
     */
    public static function this(): object|null
    {

        static::init();

        if(!is_null(static::$service)) {
            return static::getService(static::$service);
        }

        return null;

    }

}