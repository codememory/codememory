<?php

namespace Kernel\Facades;

use Kernel\Facades\Handler\Facade;
use Redis;
use System\Databases\Redis\RedisManager as ProcessedRedisManager;

/**
 * @method static bool tableExist(string $name)
 * @method static RedisManager|Redis selectDb(int $db)
 * @method static RedisManager createTable(string $name, bool $ifMissing = true, ?int $life = null)
 * @method static RedisManager selectTable(string $name)
 * @method static RedisManager addRecord(string $column, mixed $value)
 * @method static array getRecords()
 * @method static null|bool|string getRecord(string $column)
 * @method static bool updateRecord(string $column, mixed $value)
 * @method static bool updateRecordWithAdd(string $column, callable $callback)
 * @method static bool removeRecord(...$columns)
 *
 * Class RedisManager
 * @package Kernel\Facades
 *
 * @author  Codememory
 */
final class RedisManager extends Facade
{

    /**
     * @return ProcessedRedisManager
     */
    public static function init(): ProcessedRedisManager
    {

        return new ProcessedRedisManager();

    }

    /**
     * @param string $method
     * @param array  $arguments
     *
     * @return ProcessedRedisManager|Redis
     * @throws Exceptions\InitException
     */
    public static function __callStatic(string $method, array $arguments): ProcessedRedisManager|Redis
    {

        return parent::__callStatic($method, $arguments);

    }

}
