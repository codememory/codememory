<?php

namespace Kernel\Facades;

use Kernel\Facades\Handler\Facade;
use System\FileSystem\Different\ConfigFiles\ParseFile;
use System\FileSystem\Different\ConfigFiles\Types\Ini;
use System\FileSystem\Different\ConfigFiles\Types\Yaml;

/**
 * Class Markup
 * @package Kernel\Facades
 *
 * @author  Codememory
 */
final class Markup extends Facade
{

    /**
     * @return ParseFile
     */
    public static function yaml(): ParseFile
    {

        return clone new ParseFile(new Yaml());

    }

    /**
     * @return ParseFile|Ini
     */
    public static function ini(): ParseFile|Ini
    {

        return clone new ParseFile(new Ini());

    }

    /**
     * @return mixed
     */
    public static function init(): mixed
    {

        return null;

    }

}
