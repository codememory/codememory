<?php

namespace Kernel\Facades;

use JetBrains\PhpStorm\Pure;
use Kernel\Facades\Handler\Facade;
use System\Support\ConvertType as ConvertToType;

/**
 * @method static float|bool|int|string ofString(string $data)
 *
 * Class ConvertType
 * @package Kernel\Facades
 */
final class ConvertType extends Facade
{

    /**
     * @return ConvertToType
     */
    #[Pure] public static function init(): ConvertToType
    {

        return new ConvertToType();

    }

}
