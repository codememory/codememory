<?php

namespace Kernel\Facades;

use Kernel\Facades\Handler\Facade;
use System\Http\Session\Flash\Flash as FlashBag;
use System\Http\Session\Flash\FlashInterface;

/**
 * @method static FlashInterface add(string $type, string $messages)
 * @method static FlashInterface set(array $types, array $messages)
 * @method static bool has(string $type)
 * @method static array get(string $type, array $default)
 * @method static FlashInterface peak()
 * @method static array all()
 *
 * Class Flash
 * @package Kernel\Facades
 */
final class Flash extends Facade
{

    /**
     * @return mixed
     */
    protected static function init(): mixed
    {

        return new FlashBag();

    }

}
