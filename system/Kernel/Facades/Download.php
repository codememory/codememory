<?php

namespace Kernel\Facades;

use Kernel\Facades\Handler\Facade;
use System\Http\Client\Response\Download as ResponseDownload;

/**
 * @method static ResponseDownload accept(string $file)
 * @method static ResponseDownload rename(string $newName)
 * @method static ResponseDownload cacheControl(string $value)
 * @method static bool make()
 *
 * Class Download
 * @package Kernel\Facades
 *
 * @author  Codememory
 */
final class Download extends Facade
{

    /**
     * @return ResponseDownload
     */
    public static function init(): ResponseDownload
    {

        return new ResponseDownload();

    }

}
