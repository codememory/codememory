<?php

namespace Kernel\Facades;

use Kernel\Facades\Handler\Facade;
use System\FileSystem\Env\Environment;

/**
 * @method static Environment set(string $group, string $varName, bool|null|string $value)
 * @method static bool|string|null get(string $group, string $varName, bool|null|string $default = null)
 * @method static array getGroups()
 * @method static array|string|null getAll(string $type = self::TYPE_ARRAY)
 *
 * Class Env
 * @package Kernel\Facades
 *
 * @author  Codememory
 */
final class Env extends Facade
{

    public const TYPE_ARRAY = 'array';
    public const TYPE_STRING = 'string';
    public const TYPE_JSON = 'json';

    /**
     * @return string
     */
    public static function init(): string
    {

        return self::$service = 'env';

    }

}
