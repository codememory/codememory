<?php

namespace Kernel\Facades\Handler;

use JetBrains\PhpStorm\Pure;

/**
 * Class Handler
 * @package Kernel\Facades\Handler
 *
 * @author  Codememory
 */
abstract class Handler
{

    /**
     * @param mixed $init
     *
     * @return mixed
     */
    #[Pure] protected static function defineInit(mixed $init): mixed
    {

        if (is_string($init) && !is_null(static::$service)) {
            return static::getService(static::$service);
        } else {
            return $init;
        }

    }

}