<?php

namespace Kernel\Facades\Handler;

use JetBrains\PhpStorm\Pure;
use Kernel\Facades\Exceptions\InitException;
use Kernel\Facades\Traits\DefaultMethodsTrait;
use Kernel\Service\ServiceProvider;

/**
 * Class Facade
 * @package Kernel\Facades
 *
 * @author  Codememory
 */
abstract class Facade extends Handler
{

    use DefaultMethodsTrait;

    /**
     * @var string|null
     */
    protected static ?string $service = null;

    /**
     * @var ServiceProvider|null
     */
    protected static ?ServiceProvider $provider = null;

    /**
     * @param ServiceProvider $provider
     *
     * @return void
     */
    final static function setProviders(ServiceProvider $provider): void
    {

        self::$provider = $provider;

    }

    /**
     * @return static
     */
    private static function cloneFacade(): static
    {

        return new static();

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The service provider returns by name
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $serviceName
     *
     * @return mixed
     */
    #[Pure] final static function getService(string $serviceName): mixed
    {

        return self::$provider->getServiceAll($serviceName);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns a list of all facades that are in the facades.yaml configuration
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return array
     */
    final static function facades(): array
    {

        return self::getService('yaml')
                   ->open('configs/facades.yaml')
                   ->parse()['facades'];

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns the namespace of the facade by name
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $facade
     *
     * @return string
     */
    final static function facadeNamespace(string $facade): string
    {

        return self::facades()[$facade];

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method to be overridden in facades. Triggered when accessing the facade and method
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @throws InitException
     */
    protected static function init(): mixed
    {

        throw new InitException('Some facades are not initialized');

    }

    /**
     * @param string $method
     * @param array  $arguments
     *
     * @return mixed
     * @throws InitException
     */
    public static function __callStatic(string $method, array $arguments): mixed
    {

        $facade = clone self::cloneFacade();
        $init = $facade->init();

        return self::defineInit($init)->$method(...$arguments);

    }

}