<?php

namespace Kernel\Facades;

use JetBrains\PhpStorm\Pure;
use Kernel\Facades\Handler\Facade;
use PHPHtmlParser\Dom;
use System\Http\Request\Files;
use System\Http\Request\Parser;
use System\Http\Request\Request as AppRequest;
use System\Http\Request\Upload\Uploader;

/**
 * @method static array|string getBody(...$args)
 * @method static mixed post(?string $key = null, mixed $default = null)
 * @method static mixed query(?string $key = null, mixed $default = null)
 * @method static mixed all(string|null|array $key = null, mixed $default = null)
 * @method static Files file(string $input)
 * @method static bool inBoolean(string $key)
 * @method static bool isFilled(string $key)
 * @method static bool anyFilled(array $keys)
 * @method static bool anyNotFilled(array $keys)
 * @method static mixed whenFiled(string $key, callable $callback)
 * @method static bool missing(string $key)
 * @method static bool anyMissing(array $keys)
 * @method static mixed whenMissing(string $key, callable $callback)
 * @method static string path()
 * @method static bool isPathString(string $path)
 * @method static bool isPath(string|array $path)
 * @method static string method()
 * @method static bool isMethod(string $method)
 * @method static AppRequest setOldData(?string $key = null, mixed $supplement = null)
 * @method static mixed public function old(string|array $key, mixed $default = null, string $method =
 *         AppRequest::DEFAULT_OLD_METHOD)
 *
 * Class Request
 * @package Kernel\Facades
 *
 * @author  Codememory
 */
final class Request extends Facade
{

    /**
     * @return Dom
     */
    #[Pure] public static function dom(): Dom
    {

        return self::getService('request')->dom;

    }

    /**
     * @return Uploader
     */
    #[Pure] public static function upload(): Uploader
    {

        return self::getService('request')->upload;

    }

    /**
     * @return Parser
     */
    #[Pure] public static function parser(): Parser
    {

        return self::getService('request')->parser;

    }

    /**
     * @return string
     */
    public static function init(): string
    {

        return self::$service = 'request';

    }

}
