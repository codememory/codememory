<?php

namespace Kernel\Facades;

use Kernel\Facades\Handler\Facade;
use System\Http\Client\Headers\Header as AppHeader;
use System\Http\Client\Headers\Parser;

/**
 * @method static Parser getParser()
 * @method static AppHeader setProtocolVersion(int|float $version)
 * @method static AppHeader replaceHeaders(bool $replace)
 * @method static AppHeader set(array $headers, bool $replace = true)
 * @method static AppHeader setContentType(string $type)
 * @method static AppHeader setCharset(string $charset)
 * @method static AppHeader setResponseCode(int $code)
 * @method static string|null addEnum(array ...$data)
 * @method static int getHttpStatus()
 * @method static string|null|array getHeader(string ...$headers)
 * @method static array getAll()
 * @method static bool hasHeader(string $key)
 * @method static bool removeHeaders(string ...$headers)
 * @method static AppHeader send()
 *
 * Class Header
 * @package Kernel\Facades
 *
 * @author  Codememory
 */
class Header extends Facade
{

    /**
     * @return string
     */
    public static function init(): string
    {

        return self::$service = 'header';

    }

}