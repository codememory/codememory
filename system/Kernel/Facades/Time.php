<?php

namespace Kernel\Facades;

use Kernel\Facades\Handler\Facade;
use System\Support\DateTime\DateTimeInterface;

/**
 * @method static int get()
 * @method static int|string now()
 * @method static DateTimeInterface setDate(?string $date = null, int $day = 1, int $month = 0, int $year = 0)
 * @method static DateTimeInterface setTime(int $hour = 0, int $minute = 0, int $second = 0, bool $timestamp = false)
 * @method static DateTimeInterface modify(string $modify)
 * @method static DateTimeInterface addTime()
 * @method static DateTimeInterface years(int $number)
 * @method static DateTimeInterface months(int $number)
 * @method static DateTimeInterface weeks(int $number)
 * @method static DateTimeInterface days(int $number)
 * @method static DateTimeInterface hours(int $number)
 * @method static DateTimeInterface minutes(int $number)
 * @method static DateTimeInterface seconds(int $number)
 * @method static DateTimeInterface sub()
 * @method static DateTimeInterface splitByFormat(string $format, string $date)
 * @method static DateTimeInterface diff(string $compare, string $target)
 * @method static string format(string $format)
 * @method static Timezone timezone(string $timezone)
 * @method static DateTimeZone|Timezone getTimezone()
 * @method static DateTimeInterface setTimestamp(int $second)
 * @method static int durationToSeconds()
 *
 * Class Time
 * @package Kernel\Facades
 *
 * @author  Codememory
 */
final class Time extends Facade
{

    /**
     * @return string
     */
    public static function init(): string
    {

        return self::$service = 'time';

    }

}
