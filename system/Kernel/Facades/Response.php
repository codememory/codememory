<?php

namespace Kernel\Facades;

use Kernel\Facades\Handler\Facade;
use System\Http\Client\Response\Response as AppResponse;

/**
 * @method static AppResponse create(?string $content = null, int $statusCode = 200, array $headers = []): Response
 * @method static AppResponse setContent(string|int|null $content)
 * @method static string|int|null getContent()
 * @method static AppResponse sendContent()
 * @method static AppResponse responseStatus(int $code)
 * @method static int getResponseStatus()
 * @method static AppResponse sendHeaders()
 * @method static AppResponse concatSendHeaders()
 * @method static string json(array|string|int|object $data, int $status = 200, array $headers = [])
 * @method static AppResponse setContentType(string $type)
 * @method static AppResponse setCharset(string $charset)
 * @method static bool isContinue()
 * @method static bool isOk()
 * @method static bool isRedirect()
 * @method static bool isForbidden()
 * @method static bool isNotFound()
 * @method static bool isInformational()
 * @method static bool isSuccess()
 * @method static bool isRedirection()
 * @method static bool isClientError()
 * @method static bool isServerError()
 *
 * Class Response
 * @package Kernel\Facades
 *
 * @author  Codememory
 */
class Response extends Facade
{

    /**
     * @return string
     */
    protected static function init(): string
    {

        return self::$service = 'response';

    }

}