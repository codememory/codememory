<?php

namespace Kernel\Facades;

use JetBrains\PhpStorm\Pure;
use Kernel\Facades\Handler\Facade;
use System\FileSystem\Different\Editor;
use System\FileSystem\Different\File as AppFile;
use System\FileSystem\Different\Find;
use System\FileSystem\Different\Information;
use System\FileSystem\Different\Is;

/**
 * @method static bool exists(string $path)
 * @method static AppFile setRealPath(string $path)
 * @method static string getRealPath(?string $join = null)
 * @method static array scanning($path, ?array $ignoring = [])
 * @method static void mkdir($dirname, ?int $permission = 0777, bool $recursion = false)
 * @method static bool setPermission(string $path, int $permission = 0777, bool $recursion = false)
 * @method static bool setOwner(string $path, string $ownerName, bool $recursion = false)
 * @method static bool rename(string $path, string $newName)
 * @method static bool remove($path, bool $recursion = false, bool $removeCurrentDir = false)
 * @method static bool move(string $path, string $moveTo)
 * @method static bool setGroup(string $path, $group)
 * @method static string read(string $path)
 * @method static mixed getImport($path, array $parameters = [])
 * @method static void oneImport(string $path, array $parameters = [])
 *
 * Class File
 * @package Kernel\Facades
 *
 * @author  Codememory
 */
final class File extends Facade
{

    /**
     * @return Is
     */
    #[Pure] public static function is(): Is
    {

        return self::getService('file_is');

    }

    /**
     * @return Editor
     */
    #[Pure] public static function editor(): Editor
    {

        return self::getService('file_editor');

    }

    /**
     * @return Find
     */
    #[Pure] public static function find(): Find
    {

        return self::getService('file_find');

    }

    /**
     * @return Information
     */
    #[Pure] public static function info(): Information
    {

        return self::getService('file_info');

    }

    /**
     * @return string
     */
    protected static function init(): string
    {

        return self::$service = 'file';

    }

}