<?php

namespace Kernel\Facades;

use Kernel\Facades\Handler\Facade;
use System\Support\JsonParser;
use System\Support\Traits\JsonParser\DecoderTrait;

/**
 * @method static JsonParser setData(mixed $data)
 * @method static JsonParser removeRecursions(mixed $replacement = null)
 * @method static JsonParser removeNanOrInf()
 * @method static DecoderTrait decodeAmp()
 * @method static DecoderTrait decodeQuotes()
 * @method static DecoderTrait decodeTag()
 * @method static DecoderTrait slashAdaptation()
 * @method static JsonParser ofJson(int $inType = JsonParser::JSON_TO_ARRAY)
 * @method static mixed encode(int $flags = 0)
 * @method static mixed decode(int $flags = 0)
 *
 * Class Json
 * @package Kernel\Facades
 *
 * @author  Codememory
 */
final class Json extends Facade
{

    public const JSON_TO_ARRAY = 4;
    public const JSON_TO_OBJECT = 6;
    public const JSON_TO_SERIALIZE = 9;

    /**
     * @return string
     */
    public static function init(): string
    {

        return self::$service = 'json';

    }

}
