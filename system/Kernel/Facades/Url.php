<?php

namespace Kernel\Facades;

use Kernel\Facades\Handler\Facade;

/**
 * @method static string|null current()
 * @method static string getMethod(?string $case = 'up')
 * @method static array getQuery(?string $key = null)
 * @method static string|null getPrevUrl()
 * @method static string|null mergeUrl(string $url, array $params = [], bool $saveCurrentQuery = true)
 *
 * Class Url
 * @package Kernel\Facades
 *
 * @author  Codememory
 */
class Url extends Facade
{

    /**
     * @return string
     */
    public static function init(): string
    {

        return self::$service = 'url';

    }

}