<?php

namespace Kernel\Service\Exceptions;

use ErrorException;

/**
 * Class ServiceNotImplemented
 * @package System\Kernel\Service\Exceptions
 *
 * @author  Codememory
 */
class ServiceNotImplementedException extends ErrorException
{

    /**
     * @var string|null
     */
    private ?string $serviceName;

    /**
     * ServiceNotImplemented constructor.
     *
     * @param string $serviceName
     */
    public function __construct(string $serviceName)
    {

        $this->serviceName = $serviceName;

        parent::__construct(
            sprintf(
                'The <b>%s</b> service could not be called. Since he does not return anything',
                $serviceName
            )
        );

    }

    /**
     * @return string|null
     */
    public function getServiceName(): ?string
    {

        return $this->serviceName;

    }

}