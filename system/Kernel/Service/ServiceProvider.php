<?php

namespace Kernel\Service;

use Kernel\Service\Traits\ServiceInfoGetterTrait;
use Symfony\Component\Yaml\Yaml;
use System\FileSystem\Different\File;

/**
 * Class ServiceProvider
 * @package Kernel\Service
 *
 * @author  Codememory
 */
class ServiceProvider extends ServiceAssembly
{

    use ServiceInfoGetterTrait;

    const PATH_SERVICES = 'configs/providers.yaml';

    protected const DEFAULT = [
        'executed' => [
            'method' => 'executor',
            'as'     => 'service'
        ]
    ];

    /**
     * @var array
     */
    protected array $assembledServices = [];

    /**
     * @var array
     */
    private array $services;

    /**
     * @var File
     */
    private File $file;

    /**
     * ServiceProvider constructor.
     */
    public function __construct()
    {

        $this->file = new File();

        $pathProviders = $this->file->getRealPath(self::PATH_SERVICES);
        $this->services = Yaml::parseFile($pathProviders);

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Getting array lists ready services
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return array
     */
    protected function getReady(): array
    {

        return $this->assembledServices;

    }

}