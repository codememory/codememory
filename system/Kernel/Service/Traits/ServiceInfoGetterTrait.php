<?php

namespace Kernel\Service\Traits;

/**
 * Trait ServiceInfoGetterTrait
 * @package System\Kernel\Service\Traits
 *
 * @author  Codememory
 */
trait ServiceInfoGetterTrait
{

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns an array of all services that can be accessed in the context of $ this
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return array
     */
    public function getServices(): array
    {

        return $this->services['services'];

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method revives all information about the service
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $serviceName
     *
     * @return mixed
     */
    public function getServiceInfo(string $serviceName): mixed
    {

        return $this->getServices()[$serviceName] ?? [];

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns the namespace of the service specified in configs/providers.yaml
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $serviceName
     *
     * @return string
     */
    public function getServiceNamespace(string $serviceName): string
    {

        return $this->getServiceInfo($serviceName)['namespace'];

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method increases the array of arguments for the service; the arguments
     * & are specified in configs / providers.yaml under the [arguments] key
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $serviceName
     *
     * @return array
     */
    public function getServiceArguments(string $serviceName): array
    {

        return $this->getServiceInfo($serviceName)['arguments'] ?? [];

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns an array of service dependencies that are passed to the startup method. Dependencies
     * & are written in configs / providers.yaml under the [dependencies] -> [object] key. The arguments and
     * & dependencies in services are different. [arguments] are binds that are passed to the service constructor
     * & and the dependencies are what was written earlier
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string      $serviceName
     * @param string|null $type
     *
     * @return array
     */
    public function getDependencies(string $serviceName, ?string $type = null): array
    {

        $info = $this->getServiceInfo($serviceName);

        if ($type === null) {
            return $info['dependencies']['objects'] ?? [];
        } else {
            return $info['dependencies'][$type] ?? [];
        }

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & This method returns an array of information of the executed service, in this key there are other
     * & keys that are needed to work with the service. But the key is optional if the system is not
     * & specified, the system automatically substituted information on default - this may be the name of
     * & the method that should be launched
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string      $serviceName
     * @param string|null $key
     *
     * @return mixed
     */
    public function getServiceExecuted(string $serviceName, ?string $key = null): mixed
    {

        $executed = $this->getServiceInfo($serviceName)['executed'] ?? self::DEFAULT['executed'];

        if ($key !== null && $executed !== []) {
            if (!array_key_exists($key, $executed)) {
                return self::DEFAULT['executed'][$key];
            } else {
                return $executed[$key];
            }
        }

        return $executed;

    }

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & The method returns an array of extensions for the service provider
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @param string $serviceName
     *
     * @return array
     */
    public function getServiceExtender(string $serviceName): array
    {

        return $this->getServiceInfo($serviceName)['expand'] ?? [];

    }

}