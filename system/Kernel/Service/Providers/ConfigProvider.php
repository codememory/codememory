<?php

namespace Kernel\Service\Providers;

use System\FileSystem\Configuration\Config;

/**
 * @method Config open(string $config)
 * @method Config directlyFromConfig(bool $take = true)
 * @method Config add(array $data, ?string $toKey = null)
 * @method array all()
 * @method mixed get(string $keys)
 * @method mixed binds(?string $bind = null)
 *
 * Class ConfigProvider
 * @package Kernel\Service\Providers
 *
 * @author  Codememory
 */
class ConfigProvider
{

    public function __construct()
    {
    }

    /**
     * @param Config $config
     *
     * @return Config
     */
    public function executor(Config $config): Config
    {

        return $config;

    }

}
