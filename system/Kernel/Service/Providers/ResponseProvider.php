<?php

namespace Kernel\Service\Providers;

use System\Http\Client\Headers\Header;
use System\Http\Client\Response\Download;
use System\Http\Client\Response\Response;
use System\Support\JsonParser;

/**
 * @package Kernel\Service\Providers
 *
 * @property Header     $headers
 * @property JsonParser $json
 *
 * @method Response create(?string $content = null, int $statusCode = 200, array $headers = [])
 * @method Response setContent(string|int|null $content)
 * @method string|int|null getContent()
 * @method Response sendContent()
 * @method Response responseStatus(int $code)
 * @method int getResponseStatus()
 * @method Response sendHeaders()
 * @method Response concatSendHeaders()
 * @method string json(array|string|int|object $data, int $status = 200, array $headers = [])
 * @method Response setContentType(string $type)
 * @method Response setCharset(string $charset)
 * @method bool isContinue()
 * @method bool isOk()
 * @method bool isRedirect()
 * @method bool isForbidden()
 * @method bool isNotFound()
 * @method bool isInformational()
 * @method bool isSuccess()
 * @method bool isRedirection()
 * @method bool isClientError()
 * @method bool isServerError()
 *
 * Class ResponseProvider
 * @property Download   $download
 * @author  Codememory
 */
class ResponseProvider
{

    public function __construct()
    {
    }

    /**
     * @param Response $response
     *
     * @return Response
     */
    public function executor(Response $response): Response
    {

        return $response;

    }

}