<?php

namespace Kernel\Service\Providers;

use PHPHtmlParser\Dom;
use Symfony\Component\HttpFoundation\Session\Session;
use System\Http\Request\CdmToken;
use System\Http\Request\Files;
use System\Http\Request\Parser;
use System\Http\Request\Request;
use System\Http\Request\Upload\Uploader;

/**
 * @method array|string getBody(...$args)
 * @method mixed post(?string $key = null, mixed $default = null)
 * @method mixed query(?string $key = null, mixed $default = null)
 * @method mixed all(string|null|array $key = null, mixed $default = null)
 * @method Files file(string $input)
 * @method bool inBoolean(string $key)
 * @method bool isFilled(string $key)
 * @method bool anyFilled(array $keys)
 * @method bool anyNotFilled(array $keys)
 * @method mixed whenFiled(string $key, callable $callback)
 * @method bool missing(string $key)
 * @method bool anyMissing(array $keys)
 * @method mixed whenMissing(string $key, callable $callback)
 * @method string path()
 * @method bool isPathString(string $path)
 * @method bool isPath(string|array $path)
 * @method string method()
 * @method bool isMethod(string $method)
 * @method Request setOldData(?string $key = null, mixed $supplement = null)
 * @method mixed public function old(string|array $key, mixed $default = null, string $method =
 *         Request::DEFAULT_OLD_METHOD)
 *
 * Class requestProvider
 * @package Kernel\Service\Providers
 *
 * @author  Codememory
 */
class RequestProvider
{

    /**
     * @var Dom|null
     */
    public ?Dom $dom = null;

    /**
     * @var Parser|null
     */
    public ?Parser $parser = null;

    /**
     * @var Uploader|null
     */
    public ?Uploader $upload = null;

    /**
     * @var mixed|Session
     */
    public ?Session $session = null;

    /**
     * @var CdmToken|null
     */
    public ?CdmToken $cdmToken = null;

    public function __construct()
    {
    }

    /**
     * @param Request $request
     *
     * @return Request
     */
    public function executor(Request $request): Request
    {

        return $request;

    }

}
