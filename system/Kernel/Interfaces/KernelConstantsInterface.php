<?php

namespace Kernel\Interfaces;

/**
 * Interface KernelConstantsInterface
 * @package Kernel\Interfaces
 *
 * @author  Codememory
 */
interface KernelConstantsInterface
{

    public const REGEX_ROUTE_FILES = 'Routes.[a-z0-9_]+.php';

    public const PATH_TO_ROUTES = 'app/Routes/';

}