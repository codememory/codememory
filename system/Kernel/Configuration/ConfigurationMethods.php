<?php

namespace Kernel\Configuration;

use Kernel\Exceptions\PHPVersionIsIncorrectException;

/**
 * Trait ConfigurationMethods
 * @package Kernel\Configuration
 *
 * @author  Codememory
 */
trait ConfigurationMethods
{

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Method checks PHP version
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
     *
     * @return $this
     * @throws PHPVersionIsIncorrectException
     */
    private function versionCheck()
    {

        if (version_compare(PHP_VERSION, '8.0') === -1) {
            throw new PHPVersionIsIncorrectException();
        }

        return $this;

    }

}