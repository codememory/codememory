<?php

namespace Kernel\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use File;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class CompleteSkeletonCommand
 * @package Kernel\Console
 *
 * @author  Codememory
 */
class CompleteSkeletonCommand extends Command
{

    /**
     * @var array|string[]
     */
    private array $folders = [
        'avails', 'app', 'app.Controllers', 'app.Routes', 'app.Validations', 'storage', 'storage.cache', 'resources', 'app.ORM', 'app.ORM.Entity', 'app.Repository'
    ];

    protected function configure(): void
    {

        $this->setName('app:complete-skeleton')
            ->addOption('full', null, InputOption::VALUE_NONE, 'Проверить весь ли скилет создан')
            ->setDescription('Создать полный скилет проекта');

    }

    /**
     * @param SymfonyStyle $io
     * @param callable     $handler
     *
     * @return CompleteSkeletonCommand
     */
    private function handlerFolders(SymfonyStyle $io, callable $handler): CompleteSkeletonCommand
    {

        foreach ($this->folders as $path) {
            $path = str_replace('.', '/', $path);

            call_user_func($handler, $path, $io);
        }

        return $this;

    }

    /**
     * @param SymfonyStyle $io
     *
     * @return CompleteSkeletonCommand
     */
    private function full(SymfonyStyle $io): CompleteSkeletonCommand
    {

        $notCreated = [];

        $this->handlerFolders($io, function (string $path, SymfonyStyle $io) use (&$notCreated) {
            if(false === File::exists($path)) {
                $notCreated[] = $path;
            }
        });

        if([] === $notCreated) {
            $io->success('Весь скилет уже создан!');
        } else {
            $message = "Необходимо выполнить команду app:complete-skeleton. \n\nВсе не созданные ресурсы\n";
            $folders = implode(
                PHP_EOL,
                array_map(
                    fn (string $value) => str_repeat(' ', 3).$value,
                    $notCreated
                )
            );

            $io->warning($message.$folders);
        }

        return $this;

    }

    /**
     * @param SymfonyStyle $io
     *
     * @return CompleteSkeletonCommand
     */
    private function completeFolders(SymfonyStyle $io): CompleteSkeletonCommand
    {

        $createdPaths = [];

        $this->handlerFolders($io, function (string $path, SymfonyStyle $io) use (&$createdPaths) {
            if(false === File::exists($path)) {
                File::mkdir($path, 0777, true);

                $createdPaths[] = $path;
            }
        });

        if([] === $createdPaths) {
            $this->full($io);
        } else {
            $io->success(array_merge(
                ['Созданные папки:'],
                $createdPaths
            ));
        }

        return $this;

    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $io = new SymfonyStyle($input, $output);
        $full = $input->getOption('full');

        if(true === $full) {
            $this->full($io);
        } else {
            $this->completeFolders($io);
        }

        return Command::FAILURE;

    }

}