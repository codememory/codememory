<?php

use System\Kernel\KernelLaunch;
use System\Routing\Router;
use System\Http\Session\Session;

/**
 * =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>
 * & Handling all exceptions and running the framework
 * <=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=<=
 */
try {
    $kernel = new KernelLaunch();
    $kernel
        ->assembly()
        ->initSession(new Session());

    /**
     * =>=>=>=>=>=>=>=>=>=>=>=>=>=>
     * & Assembling the router
     * <=<=<=<=<=<=<=<=<=<=<=<=<=<=
     */
    Router::callRouting();
} catch (ErrorException | ReflectionException | Exception $e) {
    echo $e->getMessage();

    die;
}
