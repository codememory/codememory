<?php


namespace App\Controllers;

use System\Routing\Controller\AbstractController;
use System\View\Exceptions\InvalidViewNameException;
use System\View\View;

/**
 * Class MainController
 * @package App\Controllers
 *
 * @author Codememory
 */
class MainController extends AbstractController
{

    /**
     * @return View
     * @throws InvalidViewNameException
     */
    public function codememory(): View
    {

        return $this->view->renderByName('codememory', [
            'controller' => __CLASS__,
            'method'     => explode('::', __METHOD__)[1],
            'route'      => '/'
        ]);

    }

}

