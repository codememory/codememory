<?php

/**
 * Initialization of all codememory commands
 */
return [
    System\Routing\Console\MakeControllerCommand::class,
    System\Routing\Console\ListCommand::class,
    Kernel\Console\MakeServiceCommand::class,
    Kernel\Console\MakeFacadeCommand::class,
    Kernel\Console\UpdateFacadeAliasesCommand::class,
    Kernel\Console\CompleteSkeletonCommand::class,
    System\Components\Caching\Commands\EnvCachingCommand::class,
    System\Components\Caching\Commands\ConfigCommand::class,
    System\FileSystem\Configuration\Console\AllBindsCommand::class,
    System\Validation\Console\CreateValidationCommand::class,
    System\Databases\Doctrine\Commands\InfoCommand::class,
    System\Databases\Doctrine\Commands\MakeEntityCommand::class
];