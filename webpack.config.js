const path = require('path');

module.exports = {
    mode: 'development',
    entry: path.resolve(__dirname, 'src/assets/js/app.js'),
    output: {
        path: path.resolve(__dirname, 'src/build/js'),
        filename: 'app.js'
    }
};